// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { shallow, render, mount } from 'enzyme';
const Adapter = require('enzyme-adapter-react-16');
const enzyme = require('enzyme');
// React 16 Enzyme adapter
enzyme.configure({ adapter: new Adapter() });
export { shallow, mount, render };
export default enzyme;
