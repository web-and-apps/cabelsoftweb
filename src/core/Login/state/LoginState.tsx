
/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

export default interface LoginState {
    readonly isFetching: Boolean
    readonly tokenValue: String
    readonly roleType: String
    readonly getEmpDetail: any
    readonly payload: any
    readonly showLoading: any
    readonly isSuccess: any
}