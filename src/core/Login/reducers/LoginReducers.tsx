/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import LoginState from '../state/LoginState';
import LoginActionTypes from '../actions/LoginActionTypes';
import LoginActionTypeKeys from '../actions/LoginActionTypeKeys';

const initialState: LoginState = {
    isFetching: false,
    tokenValue: "",
    roleType: "",
    getEmpDetail: "",
    showLoading: false,
    isSuccess: false,
    payload:''
};

const LoginReducer: Reducer<LoginState, LoginActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case LoginActionTypeKeys.SIGNIN_SUCCESS: {
            return {
                ...state,
                tokenValue:action.tokenValue,
                isFetching: true

            };
        }
        case LoginActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoading: action.showLoading

            };
        }
        case LoginActionTypeKeys.GET_EMP_DETAIL_SUCCESS: {
            return {
                ...state,
                getEmpDetail: action.value,
                isFetching: action.isFetching,
                showLoading: action.showLoading,
                isSuccess: action.isSuccess
            };
        }
        case LoginActionTypeKeys.GET_EMP_DETAIL_FAILURE: {
            return {
                ...state,
                payload: '',
                showLoading: action.showLoading,
                isSuccess: action.isSuccess
            };
        }
        default:
            return state;
    }
};

export default LoginReducer;