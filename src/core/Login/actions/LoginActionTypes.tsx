/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import {
    ISignInFailAction,
    ISignInInProgressAction,
    ISignInSuccessAction,
    IGetEmpFailureInfo,
    IGetEmpSuccessInfo,
    IShowLoader
} from "./LoginActionInterface";
import {
    ISignOutFailAction,
    ISignOutInProgressAction,
    ISignOutSuccessAction
} from "./LoginActionInterface";

type LoginActionTypes =
    | ISignInFailAction
    | ISignInInProgressAction
    | ISignInSuccessAction
    | ISignOutFailAction
    | ISignOutInProgressAction
    | IGetEmpSuccessInfo
    | IGetEmpFailureInfo
    | ISignOutSuccessAction
    | IShowLoader;

export default LoginActionTypes;