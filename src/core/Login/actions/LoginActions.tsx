/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { ActionCreator, Dispatch } from 'redux';
import { APIUserManagement } from '../../api/APIUsersManagement';
import { ThunkAction } from 'redux-thunk';
import LoginActionTypes from './LoginActionTypes'
import LoginStore from '../state/LoginState';
import LoginActionTypeKeys from './LoginActionTypeKeys';

export const loginSuccessCall = (data:any): LoginActionTypes => {
    return {
        type: LoginActionTypeKeys.SIGNIN_SUCCESS,
        tokenValue: data,
        isFetching: true
    };
}

export const loginFailed = (error: Error): LoginActionTypes => {
    return {
        type: LoginActionTypeKeys.SIGNIN_FAIL,
        payload: { error }
    };
}

export const getEmpDetailSuccess = (data: any): LoginActionTypes => {
    return {
      type: LoginActionTypeKeys.GET_EMP_DETAIL_SUCCESS,
      value: data,
      isFetching: false,
      showLoading: false,
      isSuccess: true
    }
  }
export const showLoader = (): LoginActionTypes => {
    return {
      type: LoginActionTypeKeys.SHOW_LOADER,
      showLoading: true
    }
  }
  export const getEmpDetailFailure = (error: Error): LoginActionTypes => {
    return {
      type: LoginActionTypeKeys.GET_EMP_DETAIL_FAILURE,
      showLoading: false,
      isSuccess: false,
      payload: { error },
    }
  }

export const actionDoLogin: ActionCreator<
    ThunkAction<
        Promise<any>,
        LoginStore,
        null,
        LoginActionTypes
    >
> = (data: any) => {
    return (dispatch: Dispatch) => {
        console.log('Sakthi murugan', data)
        const service = new APIUserManagement();
        console.log(service.read());
        var flatbuffers = require('flatbuffers').flatbuffers;
        var userToken = require('../schema/token_generated').LoginToken;
        var isToken = userToken.usertoken
        var fbb =  new flatbuffers.Builder(0);
        console.log("fbb",data);
        var token = fbb.createString(data);
        isToken.startusertoken(fbb)
        isToken.addToken(fbb, token)
        var ints = isToken.endusertoken(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array(); //dataBuffer();

        return service.login(value).then((response) => {
            console.log("sundar",response);
            var responseArray = new Uint8Array(response.value)
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray)
            var validToken = isToken.getRootAsusertoken(byteBuffer)
            var getToken = validToken.token();
            console.log("validToken",getToken)
            dispatch(loginSuccessCall(getToken));
        }).catch((error) => {
            dispatch(loginFailed(error));
        });
    }
}
 export const actionGetRoles:  ActionCreator<
 ThunkAction<
     Promise<any>,
     LoginStore,
     null,
     LoginActionTypes
 >
> = (data: any) => {
 return (dispatch: Dispatch) => {
  dispatch(showLoader());
    const service = new APIUserManagement();
    return service.getEmployeeBYToken().then((response) => {
      console.log("AppConstants.TOKEN",response.status)
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../schema/user_generated').user;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.user.getRootAsuser(byteBuffer);
      console.log("Success Sundar", user);
      console.log("user", user)
      // var listLength = user.usersListLength();
      // console.log("listLength", listLength)
      var userobj = user.title();
      var firstName = user.firstName();
      var lastName = user.lastName();
      var photoUrl = user.empPhotoUrl();
      console.log("userobj", userobj)
      console.log("firstName", firstName)
      console.log("lastName", lastName)
      console.log("photoUrl", photoUrl)
      var obj = {
        "id": user.id().toFloat64(),
        "title": user.title(),
        "first_name": user.firstName(),
        "last_name": user.lastName(),
        "emp_id": user.empId(),
        "emp_photo_url": user.empPhotoUrl(),
        "status": user.status(),
        "isSelected": false,
        "email_id": user.emailId(),
        "appraiser_id": user.appraiserId().toFloat64(),
        "created_user_id": user.createdUserId().toFloat64(),
        "updated_user_id": user.updatedUserId().toFloat64(),
        "role_id": user.roleId(),
        "appraiser_name":user.appraiserName(),
        "appraiser_emp_id":user.appraiserEmpId()
      }
      console.log("obj", obj)
      localStorage.setItem("EmpDetails", JSON.stringify(obj));
      localStorage.setItem("RoleID", JSON.stringify(obj.role_id));
      localStorage.setItem("UserId", JSON.stringify(obj.id))
      dispatch(getEmpDetailSuccess(obj));
    }).catch((error) => {
      dispatch(getEmpDetailFailure(error));
    });
 }
}
export const loginSuccess:  ActionCreator<
 ThunkAction<
     Promise<any>,
     LoginStore,
     null,
     LoginActionTypes
 >
> = (data: any) => {
 return (dispatch: Dispatch) => {
    dispatch(showLoader());
    const service = new APIUserManagement();
    return service.getEmployeeBYToken().then((response) => {
      console.log("AppConstants.TOKEN",response.status)
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../schema/user_generated').user;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.user.getRootAsuser(byteBuffer);
      console.log("Success Sundar", user);
      console.log("user", user)
      // var listLength = user.usersListLength();
      // console.log("listLength", listLength)
      var userobj = user.title();
      var firstName = user.firstName();
      var lastName = user.lastName();
      var photoUrl = user.empPhotoUrl();
      console.log("userobj", userobj)
      console.log("firstName", firstName)
      console.log("lastName", lastName)
      console.log("photoUrl", photoUrl)
      var obj = {
        "id": user.id().toFloat64(),
        "title": user.title(),
        "first_name": user.firstName(),
        "last_name": user.lastName(),
        "emp_id": user.empId(),
        "emp_photo_url": user.empPhotoUrl(),
        "status": user.status(),
        "isSelected": false,
        "email_id": user.emailId(),
        "appraiser_id": user.appraiserId().toFloat64(),
        "created_user_id": user.createdUserId().toFloat64(),
        "updated_user_id": user.updatedUserId().toFloat64(),
        "role_id": user.roleId(),
        "appraiser_name":user.appraiserName(),
        "appraiser_emp_id":user.appraiserEmpId()
      }
      console.log("obj", obj)
      localStorage.setItem("EmpDetails", JSON.stringify(obj));
      localStorage.setItem("RoleID", JSON.stringify(obj.role_id));
      localStorage.setItem("UserId", JSON.stringify(obj.id))
      dispatch(getEmpDetailSuccess(obj));
    }).catch((error) => {
      dispatch(getEmpDetailFailure(error));
    });
 }
}