/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */


export enum LoginActionTypeStates {
    INPROGRESS = "_INPROGRESS",
    SUCCESS = "_SUCCESS",
    FAIL = "_FAIL"
}

enum LoginActionTypeKeys {
    SIGNIN_INPROGRESS = "SIGNIN_INPROGRESS",
    SIGNIN_SUCCESS = "SIGNIN_SUCCESS",
    SIGNIN_FAIL = "SIGNIN_FAIL",
    SIGNOUT_INPROGRESS = "SIGNOUT_INPROGRESS",
    SIGNOUT_SUCCESS = "SIGNOUT_SUCCESS",
    SIGNOUT_FAIL = "SIGNOUT_FAIL",
    GET_EMP_DETAIL_SUCCESS = "GET_EMP_DETAIL_SUCCESS",
    GET_EMP_DETAIL_FAILURE = "GET_EMP_DETAIL_FAILURE",
    SHOW_LOADER = "SHOW_LOADER"
}

export default LoginActionTypeKeys;