/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import keys from "./LoginActionTypeKeys";

export interface ISignInSuccessAction {
    readonly type: keys.SIGNIN_SUCCESS;
    readonly tokenValue: any;
    readonly isFetching: Boolean;
}

export interface ISignInInProgressAction {
    readonly type: keys.SIGNIN_INPROGRESS;
}

export interface ISignInFailAction {
    readonly type: keys.SIGNIN_FAIL;
    readonly payload: {
        readonly error: Error;
    };
}

export interface ISignOutSuccessAction {
    readonly type: keys.SIGNOUT_SUCCESS;
}

export interface ISignOutInProgressAction {
    readonly type: keys.SIGNOUT_INPROGRESS;
}

export interface ISignOutFailAction {
    readonly type: keys.SIGNOUT_FAIL;
    readonly payload: {
        readonly error: Error;
    };
}
export interface IShowLoader {
    type: keys.SHOW_LOADER,
    showLoading: Boolean,
}
export interface IGetEmpSuccessInfo {
    readonly type: keys.GET_EMP_DETAIL_SUCCESS;
    readonly value: any;
    readonly isFetching: Boolean;
    readonly showLoading: Boolean,
    readonly isSuccess: Boolean
}
export interface IGetEmpFailureInfo {
    readonly type: keys.GET_EMP_DETAIL_FAILURE;
    readonly showLoading: Boolean,
    readonly isSuccess: Boolean
    readonly payload: {
        readonly error: Error;
    };
}