/**
 * App constant informations
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */
export class AppConstants {
    // "http://192.168.1.15:8080/api/users"
    //static API_BASE_URL = "/api";  //User managment & Login
    //static API_BASE_URL2 = "/api"; // Goal managment and KRA
    static API_BASE_URL = "http://localhost:8089/";
    static API_BASE_URL_KRA = "http://localhost:8086/";
    static API_BASE_URL_MAIL = "http://localhost:8090/";
    // static API_BASE_URL = "https://vthink.vthinkdeveloper.com/";
    // static API_BASE_URL = "http://192.168.1.24:7000/";
    // static API_BASE_URL = "http://192.168.1.25:8087/";
    static API_RESPONSE_TYPE: 'arraybuffer';

   // static API_BASE_URL = "http://192.168.1.25:8086/api";
   // static API_BASE_URL2 = "http://192.168.1.25:8087/api";
    //admin
     //static TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwidXNlcmlkIjoiMiIsInJvbGVpZCI6IjIiLCJlbWFpbCI6Imt1bWFyZXNhbkB2dGhpbmsuY28uaW4iLCJleHAiOjE1ODIxOTI2Nzd9.eV9JU3wqql8rTak7wnBhQqDa7i241C2Tivep2_DpsH0kyRY8VDb-mjiCEwtp-u4U06Bq7rbYtdWQ4qIlYACAUg";
    //Appraiser
    // static TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI1IiwidXNlcmlkIjoiNSIsInJvbGVpZCI6IjEiLCJlbWFpbCI6InNha3RoaUB2dGhpbmsuY28uaW4iLCJleHAiOjE1ODI0MzIzNzZ9.2es4Hqn4Rnf1xKA0I5vPtzLPRZ8Kc-6RUfaUYGiSgXZEYhRHieeXZj8TivyH_tBQsSNqWE7ajEKIu4rqCeJc2A";
   // static TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMDkiLCJ1c2VyaWQiOiIxMDkiLCJyb2xlaWQiOiIwIiwiZW1haWwiOiJzZW50aGlsQHZ0aGluay5jby5pbiIsImV4cCI6MTU4MjM4MjIwOX0.4G46VHoR0dEZQx3VtDv9SEniu4ZWO6U20RRYK6Ty7Zgdk4V0SlQo03lthQZJZqjouPZ9QHNHppFdBsR26p5NGA";
    // static TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI1IiwidXNlcmlkIjoiNSIsInJvbGVpZCI6IjEiLCJlbWFpbCI6InNha3RoaUB2dGhpbmsuY28uaW4iLCJleHAiOjE1ODI0MzIzNzZ9.2es4Hqn4Rnf1xKA0I5vPtzLPRZ8Kc-6RUfaUYGiSgXZEYhRHieeXZj8TivyH_tBQsSNqWE7ajEKIu4rqCeJc2A";
    //Appraisee
    // static TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5IiwidXNlcmlkIjoiOSIsInJvbGVpZCI6IjAi∏LCJlbWFpbCI6InNha3RoaXZlbEB2dGhpbmsuY28uaW4iLCJleHAiOjE1ODIxOTI5MDF9.wbgZVemKnrA_ajJ5EVeECJ29pBbUBddjcN2n1bTagJtfeYHX74IcpGUktIUwF-E3whfZXssKpNirhkmqoLjZQQ";
    static roleKey = {
        isAdmin: '2',
        isAppriser: '1',
        isApprise:'0'
    }
    static roleType = {isAdmin: 'isAdmin', isAppriser: 'isAppriser', isApprise:'isApprise'}
    static clientId = `${process.env.REACT_APP_GOOGLE_CLIENT_ID}`
}