// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var ResponseState = ResponseState || {};

/**
 * @constructor
 */
ResponseState.responsestat = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {ResponseState.responsestat}
 */
ResponseState.responsestat.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {ResponseState.responsestat=} obj
 * @returns {ResponseState.responsestat}
 */
ResponseState.responsestat.getRootAsresponsestat = function(bb, obj) {
  return (obj || new ResponseState.responsestat).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {boolean}
 */
ResponseState.responsestat.prototype.status = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? !!this.bb.readInt8(this.bb_pos + offset) : false;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
ResponseState.responsestat.prototype.message = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {number}
 */
ResponseState.responsestat.prototype.errorCode = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt32(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
ResponseState.responsestat.startresponsestat = function(builder) {
  builder.startObject(3);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {boolean} status
 */
ResponseState.responsestat.addStatus = function(builder, status) {
  builder.addFieldInt8(0, +status, +false);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} messageOffset
 */
ResponseState.responsestat.addMessage = function(builder, messageOffset) {
  builder.addFieldOffset(1, messageOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} errorCode
 */
ResponseState.responsestat.addErrorCode = function(builder, errorCode) {
  builder.addFieldInt32(2, errorCode, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
ResponseState.responsestat.endresponsestat = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {boolean} status
 * @param {flatbuffers.Offset} messageOffset
 * @param {number} errorCode
 * @returns {flatbuffers.Offset}
 */
ResponseState.responsestat.createresponsestat = function(builder, status, messageOffset, errorCode) {
  ResponseState.responsestat.startresponsestat(builder);
  ResponseState.responsestat.addStatus(builder, status);
  ResponseState.responsestat.addMessage(builder, messageOffset);
  ResponseState.responsestat.addErrorCode(builder, errorCode);
  return ResponseState.responsestat.endresponsestat(builder);
}

// Exports for Node.js and RequireJS
this.ResponseState = ResponseState;
