/**
 * Common Service to uses accoss all the page
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */
import { AppConstants } from "../constants/Constant";
export class CommonService {

    static shared = new CommonService()
    userID?: string
    constructor() {
        this.getLoginedUserId();
    }
    /**
     * To get Logined userid from the list
     */ 
    getLoginedUserId() {
        const localStorageValue = JSON.parse(localStorage.getItem('EmpDetails')!!)
        if(localStorageValue){
            this.userID = localStorageValue.id
        }
        //return localStorageValue.id
    }
    /**
     * To convert give date into Custom Date format DD Mon YYYY
     * @param date input value from container
     */
    formatDate(date: any) {
        var newCreatedDate = new Date(date);
        const formattedCreateDate = newCreatedDate.toLocaleDateString('en-GB', {
            day: 'numeric', month: 'short', year: 'numeric'
        }).replace(/ /g, ' ');
        return formattedCreateDate;
    }
    formateGoalData(data:any){
        var newline = String.fromCharCode(13, 10);
        
        return data.replace("\\n"," ")
    }
    /**
     * To check Access token is available in the local storage is not available it will redirect
     */

    checkTokenAvailable(){
        if(localStorage.getItem('accessKey') === null){
            return false
        } else {
            return true
        }
    }
}