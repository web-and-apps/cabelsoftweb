/**
 * API service for user management
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */
import axios from "axios";
import { API,APIKRA } from "./APIInterceptors";
import { AppConstants } from "../constants/Constant";

export class APIUserManagement {
  token: any;
  constructor() {
    this.token = localStorage.getItem('accessKey');
    // this.token = AppConstants.TOKEN
  }
  search(key: String, filterType: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat(
    //   "/users?search=" + key + "&type=" + filterType
    // );
    return API.get("api/users?search=" + key + "&type=" + filterType, {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
    // return API.get(url, {
    //   responseType: "arraybuffer",
    //   headers: {
    //     "Authorization": this.token
    //   }
    // });
  }
  getComments(goalid: String, kraId: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat(
    //   "/kra/" + kraId + "/" + goalid + "/comment"
    // );
    // debugger
    console.log("getComments")
    return API.get("api/kra/" + kraId + "/" + goalid + "/comment", {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
  }
  searchAppraiser(key: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat('/users?search=' + key + '&type=apraisee');
    return API.get('api/users?search=' + key + '&type=appraisee', {
      responseType: 'arraybuffer',
      headers: {
        "Authorization": this.token
      }
    });
    // return API.get(url, {
    //   responseType: 'arraybuffer',
    //   headers: {
    //     "Authorization": this.token
    //   }
    // });
  }

  create(): Promise<any> {
    console.log("CREATE")
    // const url: any = AppConstants.API_BASE_URL.concat("/users");
    return API.post("api/users");
  }

  read(start: Number = 0, limit: Number = 20): Promise<any> {
    console.log("READ")
    // const url: any = AppConstants.API_BASE_URL.concat("/users");
    return axios.get("api/users", {
      responseType: AppConstants.API_RESPONSE_TYPE,
      headers: {
        "Authorization": this.token
      }
    });
  }

  get(): Promise<any> {
    console.log("GET")
    // const url: any = AppConstants.API_BASE_URL.concat("/users");
    return API.get("api/users", {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
  }
  getActiveEmployee(): Promise<any> {
    console.log("GET")
    // const url: any = AppConstants.API_BASE_URL.concat("/users");
    return API.get("api/users?search=&type=active", {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
  }
  getByTabName(tabName: any, data: any): Promise<any> {
    var url: any;
    console.log("GET BY TAB NAME")
    console.log("get tabName", tabName);
    if (tabName === "Appraisee KRA Details") {
      // url = AppConstants.API_BASE_URL.concat("/users");
      url = "api/users";
    } else if (tabName === "Current") {
      url = "api/kra/current";
    } else if (tabName === "History") {
      url = "api/kra/" + data + "/history";
    }
    console.log("get URL", url);
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }

  login(data: any): Promise<any> {
    //https://github.com/axios/axios/issues/1486

    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      //myHeaders.append("Authorization", this.token);

      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: data,
        redirect: "follow"
      };

      fetch(AppConstants.API_BASE_URL_KRA+"api/login", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              resolve(value);
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });

    // });
    // return API.post("/login", data, {
    //     // responseType: 'arraybuffer',
    //     headers: {
    //         'Content-Type': 'application/octet-stream'
    //     }
    // })
  }

  getEmployeeById(id: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat("/users/" + id);
    return API.get("api/users/" + id, {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
    // return API.get("/users/" + id, {
    //   responseType: "arraybuffer",
    //   headers: {
    //     "Authorization": this.token
    //   }
    // });
  }

  getEmployeeBYToken(): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat("/users/myinfo");
    // debugger
    return API.get("api/users/myinfo", {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token,
      }
    });
  }

  showPopup(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
  showInfoAlert(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  selectAll(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
  searchEmployee(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }
  test(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  readID(uid: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat("/users/{uid}");
    const url: any = "api/users/{uid}";
    return API.get(url);
  }
  delete(uid: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat('/users/{uid}');
    const url: any = 'api/users/{uid}';
    return API.delete(url);
  }
  deleteGoal(uid: String): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat('/goal/' + uid);
    const url: any = 'api/goal/' + uid
    return API.delete(url, {
      headers: {
        "Authorization": this.token
      }
    });
  }
  addGoals(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);

      var requestOptions: any = {
        method: 'POST',
        headers: myHeaders,
        body: param,
        redirect: 'follow'
      };

      fetch(AppConstants.API_BASE_URL+"api/goal", requestOptions)
        .then(function (response: any) {
          response.body.getReader().read().then(function (value: any) {
            resolve(value);
          });
        })
        .catch(function (error: any) {
          reject(error)
        });
    });

    }
    logOut(param: any): Promise<any> {
      return new Promise((resolve, reject) => {
          var myHeaders = new Headers();
          myHeaders.append("Content-Type", "application/octet-stream");
          myHeaders.append("Authorization", this.token);

          var requestOptions: any = {
              method: 'POST',
              headers: myHeaders,
              body: param,
              redirect: 'follow'
          };

          fetch(AppConstants.API_BASE_URL+"api/logout", requestOptions)
              .then(function (response: any) {
                  response.body.getReader().read().then(function (value: any) {
                      resolve(value);
                  });
              })
              .catch(function (error: any) {
                  reject(error)
              });
      });

  }

    addComments(param: any,kraId:any): Promise<any> {
        return new Promise((resolve, reject) => {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/octet-stream");
            myHeaders.append("Authorization", this.token);

            var requestOptions: any = {
                method: 'POST',
                headers: myHeaders,
                body: param,
                redirect: 'follow'
            };

            fetch(AppConstants.API_BASE_URL+'api/kra/'+kraId+'/feedback', requestOptions)
                .then(function (response: any) {
                    response.body.getReader().read().then(function (value: any) {
                        resolve(value);
                    });
                })
                .catch(function (error: any) {
                    reject(error)
                });
        });
  }

  updateGoal(param: any, id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: 'PUT',
        headers: myHeaders,
        body: param,
        redirect: 'follow'
      };

      fetch(AppConstants.API_BASE_URL+"api/goal/" + id, requestOptions)
        .then(function (response: any) {
          response.body.getReader().read().then(function (value: any) {
            resolve(value);
          });
        })
        .catch(function (error: any) {
          reject(error)
        });
    });

  }
  // assignGoals(param: any): Promise<any> {
  //     return new Promise((resolve, reject) => {
  //         var myHeaders = new Headers();
  //         myHeaders.append("Content-Type", "application/octet-stream");

  //         var requestOptions: any = {
  //             method: 'POST',
  //             headers: myHeaders,
  //             body: param,
  //             redirect: 'follow'
  //         };

  //         fetch(AppConstants.API_BASE_URL + "/kra", requestOptions)
  //             .then(function (response: any) {
  //                 response.body.getReader().read().then(function (value: any) {
  //                     resolve(value);
  //                 });
  //             })
  //             .catch(function (error: any) {
  //                 reject(error)
  //             });
  //     });
  //   }
  update(): Promise<any> {
    console.log("UPDATE")
    // const url: any = AppConstants.API_BASE_URL.concat("/users");
    const url: any = "api/users"
    return API.put(url);
  }

  // delete(uid: String): Promise<any> {
  //   const url: any = AppConstants.API_BASE_URL.concat("/users/{uid}");
  //   return API.delete(url);
  // }
  // addGoals(param: any): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     var myHeaders = new Headers();
  //     myHeaders.append("Content-Type", "application/octet-stream");

  //     var requestOptions: any = {
  //       method: "POST",
  //       headers: myHeaders,
  //       body: param,
  //       redirect: "follow"
  //     };

  //     fetch(AppConstants.API_BASE_URL + "/kra", requestOptions)
  //       .then(function(response: any) {
  //         response.body
  //           .getReader()
  //           .read()
  //           .then(function(value: any) {
  //             resolve(value);
  //           });
  //       })
  //       .catch(function(error: any) {
  //         reject(error);
  //       });
  //   });
  // }
  assignGoals(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };

      fetch(AppConstants.API_BASE_URL+"api/kra", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              resolve(value);
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }
  editSkillSet(param: any,id:String): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "PUT",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      fetch(AppConstants.API_BASE_URL+"api/skillmax/skillset/"+id, requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
              
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }
  addSkillSet(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      fetch(AppConstants.API_BASE_URL+"api/skillmax/skillset", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
              
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }
  getSkillSetList(searchText:String,start:String,limit:String): Promise<any> {
    console.log("GET")
     const url: any = "/api/skillmax/skillset?search="+searchText+"&start="+start+"&limit="+limit
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Authorization": this.token
      }
    });
  }
  addEmployee(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      console.log("ADD EMPLOYEE")
      fetch(AppConstants.API_BASE_URL+"api/users", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
              
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }

  updateEmployee(param: any, id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "PUT",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };

      console.log("UPDATE EMPLOYEE")
      fetch(AppConstants.API_BASE_URL+"api/users", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              // resolve(value);
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }

  getAppraisee(): Promise<any> {
    // const url: any = AppConstants.API_BASE_URL.concat("/users/appraisee");
    const url: any = "api/users/appraisee";
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }
  getGoals(): Promise<any> {
    const url: any = 'api/goal';
    // return API.get(url, {
    //     headers: {
    //         'Content-Type': 'application/octet-stream'
    //     }
    // }
    // )
    console.log("getGoals",url)
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }
  getTags(key : String): Promise<any> {
    const url: any = 'api/goal/tag?search=' + key
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }

  searchGoal(key: String): Promise<any> {
    const url: any = 'api/goal?search=' + key
    return API.get(url, {
      responseType: 'arraybuffer',
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });

  }

  getKRAUserHistory(data: any): Promise<any> {
    const url: any = "api/kra/" + data + "/history";
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }
  ///api/kra/{empid}/history/{kraId}
  getKRASpecificUserHistory(data: any, type: any): Promise<any> {
    // getKRASpecificUserHistory(data: any,kraId: any): Promise<any> {
    let url: any
    if (type === 'history') {
      url = "api/kra/" + data.userId + "/history/" + data.kraId;
    } else if (type === 'current') {
      url = "api/kra/" + data.empId;
    }

    // const url: any = AppConstants.API_BASE_URL.concat('/kra/'+data+'/history/'+kraId+'');
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }
  getCurrentKRAGoals(data: any): Promise<any> {
    const url: any = "api/kra/current";
    return API.get(url, {
      responseType: "arraybuffer",
      headers: {
        "Content-Type": "application/octet-stream",
        "Authorization": this.token
      }
    });
  }
  closeKRA(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: 'PUT',
        headers: myHeaders,
        body: param,
        redirect: 'follow'
      };

      fetch(AppConstants.API_BASE_URL+'api/kra', requestOptions)
        .then(function (response: any) {
          response.body.getReader().read().then(function (value: any) {
            resolve(value);
          });
        })
        .catch(function (error: any) {
          reject(error)
        });
    });

  }


  appraiserCommentadd(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      fetch(AppConstants.API_BASE_URL+"api/kra/comment", requestOptions)
// Sundar
      // fetch("http://192.168.1.56:8087/api/kra/comment", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
            });
        })
        .catch(function (error: any) {
          if (error.response.status == "400"){
            window.alert("cannot add comment after kra end")
          }
          reject(error);
        });
    });
  }

  appraiserSaveAll(param: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "PUT",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };

      fetch(AppConstants.API_BASE_URL+"api/kra", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              resolve(value);
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }

  appraiserCommentupdate(param: any, kraId: any, goalid: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "PUT",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      
      fetch(AppConstants.API_BASE_URL+"api/kra/" + kraId + "/" + goalid + "/comment", requestOptions)
      //Sundar Changes
      // fetch("http://192.168.1.56:8087/api/kra/" + kraId + "/" + goalid + "/comment", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              resolve(value);
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
  }

  deleteSpecificGoals(kraId: any,goalid:any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);

      var requestOptions: any = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch(AppConstants.API_BASE_URL+"api/kra/" + kraId + "/" + goalid, requestOptions)
        .then(function (response: any) {
          response.body.getReader().read().then(function (value: any) {
            resolve(value);
          });
        })
        .catch(function (error: any) {
          reject(error)
        });
    });

    }
    upload(datas: any) {
      var myHeaderss = new Headers();
      myHeaderss.append("Content-Type", "multipart/form-data");
      //myHeaders.append("Authorization", this.token);

      var requestOptions: any = {
        method: "POST",
        headers: {
         'Content-Type': 'multipart/form-data',
        }
      };

      return axios.post(AppConstants.API_BASE_URL+"api/storage/content",datas, requestOptions);
  }
  deleteDesiredSkillSet(id: any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);

      var requestOptions: any = {
        method: 'DELETE',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch(AppConstants.API_BASE_URL+"api/skillmax/desiredskill/"+id, requestOptions)
        .then(function (response: any) {
          response.body.getReader().read().then(function (value: any) {
            resolve(value);
          });
        })
        .catch(function (error: any) {
          reject(error)
        });
    });

    }
    getDesiredSkillSetList(searchText:String): Promise<any> {
      console.log("GET")
       const url: any = "/api/skillmax/desiredskill?search="+searchText+"&start=&limit="
      return API.get(url, {
        responseType: "arraybuffer",
        headers: {
          "Authorization": this.token
        }
      });
    }
    addDesiredSkillSet(param: any): Promise<any> {
      return new Promise((resolve, reject) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/octet-stream");
        myHeaders.append("Authorization", this.token);
        var requestOptions: any = {
          method: "POST",
          headers: myHeaders,
          body: param,
          redirect: "follow"
        };
        fetch(AppConstants.API_BASE_URL+"api/skillmax/desiredskill", requestOptions)
          .then(function (response: any) {
            response.body
              .getReader()
              .read()
              .then(function (value: any) {
                if(response && response.status && response.status >= 200 && response.status <= 299){
                  resolve(value);
                } else if(response && response.status && response.status == 500){
                    reject("Internal Server Error!");
                }
                else {
                  reject(value);
                }
                
              });
          })
          .catch(function (error: any) {
            reject(error);
          });
      });
    }
    getuserList(): Promise<any> {
       const url: any = "api/users"
      return APIKRA.get(url, {
        responseType: "arraybuffer",
        headers: {
          "Authorization": this.token
        }
      });
    }
    searchUserList(search:String): Promise<any> {
      const url: any = "api/users?search="+search+"&type=all"
     return APIKRA.get(url, {
       responseType: "arraybuffer",
       headers: {
         "Authorization": this.token
       }
     });
   }
    getSkillSetDetails(param:any): Promise<any> {
    return new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/octet-stream");
      myHeaders.append("Authorization", this.token);
      var requestOptions: any = {
        method: "POST",
        headers: myHeaders,
        body: param,
        redirect: "follow"
      };
      fetch(AppConstants.API_BASE_URL+"api/skillmax/report/detail", requestOptions)
        .then(function (response: any) {
          response.body
            .getReader()
            .read()
            .then(function (value: any) {
              if(response && response.status && response.status >= 200 && response.status <= 299){
                resolve(value);
              } else if(response && response.status && response.status == 500){
                  reject("Internal Server Error!");
              }
              else {
                reject(value);
              }
              
            });
        })
        .catch(function (error: any) {
          reject(error);
        });
    });
}
getFilterResults(param:any): Promise<any> {
  return new Promise((resolve, reject) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/octet-stream");
    myHeaders.append("Authorization", this.token);
    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: param,
      redirect: "follow"
    };
    fetch(AppConstants.API_BASE_URL+"api/skillmax/report", requestOptions)
      .then(function (response: any) {
        response.body
          .getReader()
          .read()
          .then(function (value: any) {
            if(response && response.status && response.status >= 200 && response.status <= 299){
              resolve(value);
            } else if(response && response.status && response.status == 500){
                reject("Internal Server Error!");
            }
            else {
              reject(value);
            }
            
          });
      })
      .catch(function (error: any) {
        reject(error);
      });
  });
}
saveEmailTemplate(param:any): Promise<any> {
  return new Promise((resolve, reject) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/octet-stream");
    myHeaders.append("Authorization", this.token);
    var requestOptions: any = {
      method: "POST",
      headers: myHeaders,
      body: param,
      redirect: "follow"
    };
    fetch(AppConstants.API_BASE_URL_MAIL+"api/email/templatecontent", requestOptions)
      .then(function (response: any) {
        response.body
          .getReader()
          .read()
          .then(function (value: any) {
            if(response && response.status && response.status >= 200 && response.status <= 299){
              resolve(value);
            } else if(response && response.status && response.status == 500){
                reject("Internal Server Error!");
            }
            else {
              reject(value);
            }
            
          });
      })
      .catch(function (error: any) {
        reject(error);
      });
  });
}
updateEmailTemplate(param: any,id:String): Promise<any> {
  return new Promise((resolve, reject) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/octet-stream");
    myHeaders.append("Authorization", this.token);
    var requestOptions: any = {
      method: "PUT",
      headers: myHeaders,
      body: param,
      redirect: "follow"
    };
    fetch(AppConstants.API_BASE_URL_MAIL+"api/email/templatecontent"+id, requestOptions)
      .then(function (response: any) {
        response.body
          .getReader()
          .read()
          .then(function (value: any) {
            if(response && response.status && response.status >= 200 && response.status <= 299){
              resolve(value);
            } else if(response && response.status && response.status == 500){
                reject("Internal Server Error!");
            }
            else {
              reject(value);
            }
            
          });
      })
      .catch(function (error: any) {
        reject(error);
      });
  });
}
getEmailTemplate(): Promise<any> {
  console.log("GET")
  // const url: any = AppConstants.API_BASE_URL.concat("/users");
  return API.get("api/email/templatecontent", {
    responseType: "arraybuffer",
    headers: {
      "Authorization": this.token
    }
  });
}
}
