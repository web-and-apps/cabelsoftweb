/**
 * App reducer
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */


import { combineReducers } from "redux";
import LoginReducer from "./Login/reducers/LoginReducers";
import GoalManagementReducers from "./KRA/GoalManagement/reducers/GoalManagementReducers"
import EmployeeManagementReducers from "./KRA/EmployeeManagement/reducers/EmployeeManagementReducers";
import KRAHistoryManagementReducers from "./KRA/KRAHistoryManagement/reducers/KRAHistoryManagementReducers"
import UserGoalDetailManagementReducers from "./KRA/UserGoalDetailManagement/reducers/UserGoalDetailManagementReducers";
import AssignKRAGoalManagementReducers from "./KRA/AssignKRAGoalManagement/reducers/AssignKRAGoalManagementReducers";
import AppraiserManagementReducers from './KRA/AppraiserManagement/reducers/AppraiserManagementReducers'
import MasterSkillsetReducers from "./SkillMatrix/MasterSkillset/reducers/MasterSkillsetActionReducers";
import FilterResourceManagementReducers from "./SkillMatrix/FilterResourceManagement/reducers/FilterResourceManagementReducers"
import DesiredSkillSetReducers from "./SkillMatrix/DesiredSkillSet/reducers/DesiredSkillSetActionReducers";
import EmployeeSkillSetDetailsContainerActionReducers from "./SkillMatrix/EmployeeSkillSetDetailsContainer/reducers/EmployeeSkillSetDetailsContainerActionReducers"
import SkillSetDetailContainerActionReducers from "./SkillMatrix/SkillSetDetailContainer/reducers/SkillSetDetailContainerActionReducers"
// Create the root reducer
const rootReducer = combineReducers({
    loginState: LoginReducer,
    goalManagementState: GoalManagementReducers,
    employeeManagementState: EmployeeManagementReducers,
    kraHistoryManagementState: KRAHistoryManagementReducers,
    userGoalDetailManagementState: UserGoalDetailManagementReducers,
    assignKRAGoalManagementState:AssignKRAGoalManagementReducers,
    appraiserManagementState: AppraiserManagementReducers,
    masterSkillsetState: MasterSkillsetReducers,
    filterResourceManagementState:FilterResourceManagementReducers,
    desiredSkillSetState:DesiredSkillSetReducers,
    employeeSkillSetDetailsContainerState:EmployeeSkillSetDetailsContainerActionReducers,
    skillSetDetailContainerState:SkillSetDetailContainerActionReducers,

});

export default rootReducer;