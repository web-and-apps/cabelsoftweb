import LoginState from "./Login/state/LoginState";
import GoalManagementState from "./KRA/GoalManagement/state/GoalManagementState";
import EmployeeMangementState from "./KRA/EmployeeManagement/state/EmployeeManagementState";
import UserGoalDetailManagementState from './KRA/UserGoalDetailManagement/state/UserGoalDetailManagementState';
import AssignKRAGoalManagementState from './KRA/AssignKRAGoalManagement/state/AssignKRAGoalManagementState';
import AppraiserManagementState from './KRA/AppraiserManagement/state/AppraiserManagementState'
import KRAHistoryManagementState from './KRA/KRAHistoryManagement/state/KRAHistoryManagementState'
import MasterSkillsetState from "./SkillMatrix/MasterSkillset/state/MasterSkillsetState";
import FilterResourceManagementState from "./SkillMatrix/FilterResourceManagement/state/FilterResourceManagementState"
import DesiredSkillSetState from "./SkillMatrix/DesiredSkillSet/state/DesiredSkillSetState"
import EmployeeSkillSetDetailsContainerState from "./SkillMatrix/EmployeeSkillSetDetailsContainer/state/EmployeeSkillSetDetailsContainerState"
import SkillSetDetailContainerState from "./SkillMatrix/SkillSetDetailContainer/state/SkillSetDetailContainerState"

export default interface AppStore {
    loginState: LoginState;
    goalManagementState: GoalManagementState;
    employeeManagementState: EmployeeMangementState;
    userGoalDetailManagementState:UserGoalDetailManagementState;
    assignKRAGoalManagementState:AssignKRAGoalManagementState;
    appraiserManagementState:AppraiserManagementState;
    kraHistoryManagementState : KRAHistoryManagementState;
    masterSkillsetState : MasterSkillsetState;
    filterResourceManagementState:FilterResourceManagementState;  
    desiredSkillSetState: DesiredSkillSetState;
    employeeSkillSetDetailsContainerState:EmployeeSkillSetDetailsContainerState;
    skillSetDetailContainerState:SkillSetDetailContainerState;
}