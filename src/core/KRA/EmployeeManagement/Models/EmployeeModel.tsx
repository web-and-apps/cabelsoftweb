
export class Employee {

    id: number
    title: string
    first_name: string
    last_name: string
    emp_id: string
    emp_photo_url: string
    status: string
    isSelected: boolean
    email_id: string
    appraiser_id: number
    created_user_id: number
    updated_user_id: number
    role_id: number
    empLevel: number
    kraId: any
    kraStartDate: any
    kraEndDate: any

    constructor(user: any, id?: any) {
      
        this.id = user.id().toFloat64()
        this.title = user.title()
        this.first_name = user.firstName()
        this.last_name = user.lastName()
        this.emp_id = user.empId()
        this.emp_photo_url = user.empPhotoUrl()
        this.status = user.status()
        this.isSelected = false
        this.email_id = user.emailId()
        this.appraiser_id = user.appraiserId().toFloat64()
        this.created_user_id = user.createdUserId().toFloat64()
        this.updated_user_id = user.updatedUserId().toFloat64()
        this.role_id = user.roleId()
        this.empLevel = user.empLevel()
        this.kraId = user.kraId()
        this.kraStartDate = user.kraStartDate()
        this.kraEndDate = user.kraEndDate()
    }
}