/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import EmployeeManagementTypes from '../actions/EmployeeManagementTypes'
import EmployeeMangementState from '../state/EmployeeManagementState';
import EmployeeManagementTypeKeys from '../actions/EmployeeManagementTypeKeys';

const initialState: EmployeeMangementState = {
    profileValue: [],
    value: false,
    isEmpty: "dfdfd",
    history:[],
    getEmpDetail: {},
    appraiserList:[],
    success:"",
    showPopup:false,
    loading:false,
    logOut:false,
    errorText:"",
    showLoading: false,
    isSuccess: false,
    successMessage: "",
};

const EmployeeManagementReducers: Reducer<EmployeeMangementState, EmployeeManagementTypes> = (state = initialState, action) => {
    switch (action.type) {
        case EmployeeManagementTypeKeys.MANAGE_PROFILE_SUCCESS: {
            return {
                ...state,
                profileValue: action.value,
                isEmpty: action.isEmpty,
                showLoading: action.showLoading,
            };
        }
        case EmployeeManagementTypeKeys.GET_APPRAISER_LIST: {
            return {
                ...state,
                appraiserList: action.value,
                isEmpty: action.isEmpty,
                showLoading: action.showLoading
            };
        }
        case EmployeeManagementTypeKeys.LOGOUT: {
            return {
                ...state,
                logOut: action.value,
            };
        }
        case EmployeeManagementTypeKeys.SUCCESS: {
            return {
                ...state,
                success: action.value,
            };
        }
        case EmployeeManagementTypeKeys.MANAGE_PROFILE_FAIL: {
            return {
                ...state,
                showLoading: action.showLoading,
            };
        }
        case EmployeeManagementTypeKeys.SHOWLOADING: {
            return {
                ...state,
                showLoading: action.showLoading,
            };
        }
        case EmployeeManagementTypeKeys.HIDELOADING: {
            return {
                ...state,
                showLoading: false,
            };
        }
        case EmployeeManagementTypeKeys.SHOW_POPUP: {
            return {
                ...state,
                showPopup: action.value
            };
        }
        case EmployeeManagementTypeKeys.GET_EMP_DETAIL_SUCCESS: {
            return {
                ...state,
                getEmpDetail: action.value
            };
        }
        case EmployeeManagementTypeKeys.HIDETOASTER: {
            return {
                ...state,
                isSuccess: action.isSuccess
            };
        }
        case EmployeeManagementTypeKeys.GETHISTORY: {
            return {
                ...state,
                history: action.value
            };
        }
        case EmployeeManagementTypeKeys.New_Profile_Value_Added: {
            // console.log(state.profileValue)
            // const temp = state.profileValue.push(action.value)
            return {
                ...state,
                profileValue: action.value,
                showPopup: action.showPopup,
                loading: action.Loading,
                showLoading: action.showLoading,
                isSuccess: action.isSuccess,
                successMessage: action.successMessage,
            };
        }
        case EmployeeManagementTypeKeys.New_Profile_Value_Error: {
            return {
                ...state,
                errorText: action.value,
                showPopup: action.showPopup,
                loading: action.Loading,
            };
        }
        default:
            return state;
    }
    
};

export default EmployeeManagementReducers;

// export class commonFunction {
//     static addNewEmployee(state: EmployeeMangementState, action: any): any {
//         debugger
//         console.log(state.profileValue.push(action.value))
//         return state.profileValue.push(action.value)
//     }
// }