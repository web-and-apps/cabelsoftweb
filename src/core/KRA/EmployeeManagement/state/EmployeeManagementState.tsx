/**
import { showPopup } from '../../KRAHistoryManagement/actions/KRAHistoryManagementActions';
 * Employee Mangement State Informations
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */

export default interface EmployeeMangementState {
    readonly profileValue: any,
    readonly value: any,
    readonly isEmpty: any,
    readonly history:any,
    readonly getEmpDetail: any,
    readonly appraiserList:any,
    readonly success:any,
    readonly showPopup:any,
    readonly loading:any,
    readonly logOut:Boolean,
    readonly errorText:any,
    readonly showLoading: Boolean,
    readonly isSuccess: Boolean,
    readonly successMessage: any,
}