/**
 * Employee Management Container Action
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */
import { ActionCreator, Dispatch } from 'redux';
import { APIUserManagement } from '../../../api/APIUsersManagement';
import { ThunkAction } from 'redux-thunk';
import EmployeeManagementTypeKeys from './EmployeeManagementTypeKeys';
import EmployeeManagementAction from './EmployeeManagementTypes';
import EmployeeMangementStore from '../state/EmployeeManagementState';
import { Employee } from '../Models/EmployeeModel';
export const getAllUserProfile = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.MANAGE_PROFILE_SUCCESS,
        value: data,
        isEmpty: data.isEmpty,
        showLoading: false,
    }
}
export const logOut = (data:Boolean): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.LOGOUT,
        value: data,
    }
}
export const getAppraiserList = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.GET_APPRAISER_LIST,
        value: data,
        isEmpty: data.isEmpty,
        showLoading: false,
    }
}
export const resStatus = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.SUCCESS,
        value: data
    }
}
export const getEmpDetailSuccess = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.GET_EMP_DETAIL_SUCCESS,
        value: data
    }
}
export const getEmpDetailFailure = (error: Error): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.GET_EMP_DETAIL_FAILURE,
        payload: { error }
    }
}
export const getHistory = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.GETHISTORY,
        value: data
    }
}
export const showPopupSuccess = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.SHOW_POPUP,
        value: true,
    }
}
export const hide = (): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.SHOW_POPUP,
        value: false
    };
}
export const getAllUserProfileFailed = (error: Error): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.MANAGE_PROFILE_FAIL,
        showLoading: false,
        payload: { error }
    }
}
export const getAllRecordNotFound = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.MANAGE_NORECORD,
        isEmpty: data.isEmpty
    }
}
export const profileValueAdded = (data: any, value?: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.New_Profile_Value_Added,
        value: data,
        showPopup: false,
        Loading: false,
        showLoading: false,
        isSuccess: true,
        successMessage: value,
    }
}
export const profileValueError = (data: any): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.New_Profile_Value_Error,
        value: data,
        showPopup: true,
        Loading: false,
        showLoading: false,
    }
}
export const showLoader = (): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.SHOWLOADING,
        showLoading: true,
    }
  }
  export const hideLoader = (): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.HIDELOADING,
        showLoading: false,
    }
  }
  export const hideToasterWindow = (): EmployeeManagementAction => {
    return {
        type: EmployeeManagementTypeKeys.HIDETOASTER,
        isSuccess: false,
    }
  }
const getEmployee = (): Promise<any> => {
    return new Promise((resolve, reject) => {
        const service = new APIUserManagement()
        return service.get().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
            }

            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (var j = 0; j < levelLength; j++) {
                    var emp = new Employee(user.userlevel(j))                    
                    if (levels[i] === emp.empLevel) {
                        dataarr.push(emp);
                    }
                }
                levelarray.push(dataarr)
            }
            resolve(levelarray)
        }).catch((error) => {
            reject(error)
        });
    })
}

export const addEmployee: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('../schema/addUser_generated').user;
        var addProfileData = addProfile.userreq
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var title = fbb.createString(param.title);
        var first_name = fbb.createString(param.first_name);
        var last_name = fbb.createString(param.last_name);
        var emp_id = fbb.createString(param.emp_id);
        var emp_photo_url = fbb.createString(param.emp_photo_url);
        var email_id = fbb.createString(param.email_id);
        var appraiser_id = fbb.createLong(param.appraiser_id);
        var created_user_id = fbb.createLong(param.created_user_id);
        var updated_user_id = fbb.createLong(param.updated_user_id);
        // var role_id = fbb.createLong(param.role_id);
        var kraStartDate = fbb.createString(param.kraStartDate);
        var kraEndDate = fbb.createString(param.kraEndDate);
        addProfileData.startuserreq(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addTitle(fbb, title)
        addProfileData.addFirstName(fbb, first_name)
        addProfileData.addLastName(fbb, last_name)
        addProfileData.addEmpId(fbb, emp_id)
        addProfileData.addEmpPhotoUrl(fbb, emp_photo_url)
        addProfileData.addStatus(fbb, Number(param.status))
        addProfileData.addEmailId(fbb, email_id)
        addProfileData.addAppraiserId(fbb, appraiser_id)
        addProfileData.addCreatedUserId(fbb, created_user_id)
        addProfileData.addUpdatedUserId(fbb, updated_user_id)
        addProfileData.addRoleId(fbb, "")
        addProfileData.addStartDate(fbb, kraStartDate)
        addProfileData.addEndDate(fbb, kraEndDate)
        var ints = addProfileData.enduserreq(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.addEmployee(value).then((response) => {
            if (response) {
                let obj: any = {}
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/createresponse_generated').responseStatus
                var responseArray = new Uint8Array(response.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.createresponse.getRootAscreateresponse(byteBuffer);
                obj.id = responseStatus.id();
                obj.message = responseStatus.message();
                getEmployee().then((response) => {
                    dispatch(profileValueAdded(response, "Employee Added Successfully"))
                }).catch((error) =>  {
                    console.log(error)
                })
            }
        }).catch((error: any) => {
            dispatch(hideLoader());
            let responseSuccess
            if(typeof(error) === 'string'){
                responseSuccess = error;
            } else if (typeof(error) === 'object'){
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
                var responseArray = new Uint8Array(error.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.response.getRootAsresponse(byteBuffer);
                responseSuccess = responseStatus.message();
            }
            window.alert(responseSuccess);
            dispatch(profileValueError(responseSuccess));
        });
    }
}
export const updateEmployee: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('../schema/addUser_generated').user;
        var addProfileData = addProfile.userreq
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var title = fbb.createString(param.title);
        var first_name = fbb.createString(param.first_name);
        var last_name = fbb.createString(param.last_name);
        var emp_id = fbb.createString(param.emp_id);
        var emp_photo_url = fbb.createString(param.emp_photo_url);
        var email_id = fbb.createString(param.email_id);
        var appraiser_id = fbb.createLong(param.appraiser_id);
        var created_user_id = fbb.createLong(param.created_user_id);
        var updated_user_id = fbb.createLong(param.updated_user_id);
        // var role_id = fbb.createLong(param.role_id);
        var kraStartDate = fbb.createString(param.kraStartDate);
        var kraEndDate = fbb.createString(param.kraEndDate);
        addProfileData.startuserreq(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addTitle(fbb, title)
        addProfileData.addFirstName(fbb, first_name)
        addProfileData.addLastName(fbb, last_name)
        addProfileData.addEmpId(fbb, emp_id)
        addProfileData.addEmpPhotoUrl(fbb, emp_photo_url)
        addProfileData.addStatus(fbb, Number(param.status))
        addProfileData.addEmailId(fbb, email_id)
        addProfileData.addAppraiserId(fbb, appraiser_id)
        addProfileData.addCreatedUserId(fbb, created_user_id)
        addProfileData.addUpdatedUserId(fbb, updated_user_id)
        addProfileData.addRoleId(fbb, "")
        addProfileData.addStartDate(fbb, kraStartDate)
        addProfileData.addEndDate(fbb, kraEndDate)
        var ints = addProfileData.enduserreq(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.updateEmployee(value, param.id).then((response) => {
            if (response) {
                let obj: any = {}
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/createresponse_generated').responseStatus
                var responseArray = new Uint8Array(response.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.createresponse.getRootAscreateresponse(byteBuffer);
                obj.id = responseStatus.id();
                obj.message = responseStatus.message();
                getEmployee().then((response) => {
                    dispatch(profileValueAdded(response, "Employee Details Updated Successfully"))
                }).catch((error) =>  {
                    console.log(error)
                })
            }
        }).catch((error) => {
            dispatch(hideLoader());
            let responseSuccess
            if(typeof(error) === 'string'){
                responseSuccess = error;
            } else if (typeof(error) === 'object'){
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
                var responseArray = new Uint8Array(error.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.response.getRootAsresponse(byteBuffer);
                responseSuccess = responseStatus.message();
            }
            window.alert(responseSuccess);
            dispatch(profileValueError(responseSuccess));
        });
    }
}

export const getEmployeeDetails: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = () => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        // console.log(service.get());
        return service.getEmployeeBYToken().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/addUser_generated').user;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.user.getRootAsuser(byteBuffer);
            var emp = new Employee(user)
            dispatch(getEmpDetailSuccess(emp));
        }).catch((error) => {
            dispatch(getEmpDetailFailure(error));
        });
    }
}

export const getUserProfile: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = () => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        // console.log(service.get());
        return service.get().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
            }

            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (var j = 0; j < levelLength; j++) {
                    var emp = new Employee(user.userlevel(j))                    
                    if (levels[i] === emp.empLevel) {
                        dataarr.push(emp);
                    }
                }
                levelarray.push(dataarr)
            }
            dispatch(getAllUserProfile(levelarray));
        }).catch((error) => {
            dispatch(getAllUserProfileFailed(error));
        });
    }
}

// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchCharacters: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (term: String, filterType: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.search(term, filterType).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
            }
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (var j = 0; j < levelLength; j++) {
                    let obj: any = {}
                    obj.id = user.userlevel(j).id().toFloat64();
                    obj.title = user.userlevel(j).title();
                    obj.first_name = user.userlevel(j).firstName();
                    obj.last_name = user.userlevel(j).lastName();
                    obj.emp_id = user.userlevel(j).empId();
                    obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
                    obj.status = user.userlevel(j).status();
                    obj.email_id = user.userlevel(j).emailId();
                    obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
                    obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
                    obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
                    obj.role_id = user.userlevel(j).roleId();
                    obj.empLevel = user.userlevel(j).empLevel();
                    obj.kraId = user.userlevel(j).kraId();
                    obj.kraStartDate = user.userlevel(j).kraStartDate();
                    obj.kraEndDate = user.userlevel(j).kraEndDate();
                    obj.isSelected = false;
                    if (levels[i] === obj.empLevel) {
                        dataarr.push(obj);
                    }
                }
                levelarray.push(dataarr)
            }
            dispatch(getAllUserProfile(levelarray));
        }).catch((error) => {
            dispatch(getAllUserProfileFailed(error));
        });
    };
};
// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchAppraiser: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (term: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.searchAppraiser(term).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/fbsdata_generated').UserData;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.users.getRootAsusers(byteBuffer);
            var listLength = user.usersListLength();
            var userobj = user.usersList(0).id();
            var created_user_id = user.usersList(0).createdUserId();
            var updated_user_id = user.usersList(0).updatedUserId();
            var arr = [];
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i <= listLength - 1; i++) {
                let obj: any = {};
                obj.id = user.usersList(i).id().toFloat64();
                obj.title = user.usersList(i).title();
                obj.first_name = user.usersList(i).firstName();
                obj.last_name = user.usersList(i).lastName();
                obj.emp_id = user.usersList(i).empId();
                obj.emp_photo_url = user.usersList(i).empPhotoUrl();
                obj.email_id = user.usersList(i).emailId();
                obj.status = user.usersList(i).status();
                obj.appraiser_id = user.usersList(i).appraiserId();
                obj.created_user_id = user.usersList(i).createdUserId().toString();
                obj.updated_user_id = user.usersList(i).updatedUserId().toString();
                obj.role_id = user.usersList(i).roleId();
                obj.isSelected = false;
                arr.push(obj);
            }
            console.log("AppraiserList", arr)
            dispatch(getAppraiserList(arr));
        }).catch((error) => {
            dispatch(getAllUserProfileFailed(error));
        });
    };
};

export const switchTabAction: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction>

> = (tabName: String) => {

    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.get().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/fbsdata_generated').UserData;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.users.getRootAsusers(byteBuffer);
            var listLength = user.usersListLength();
            var userobj = user.usersList(0).id();
            var created_user_id = user.usersList(0).createdUserId();
            var updated_user_id = user.usersList(0).updatedUserId();
            var arr = [];
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < listLength; i++) {
                let obj: any = {}
                obj.id = user.usersList(i).id().toFloat64();
                obj.title = user.usersList(i).title();
                obj.first_name = user.usersList(i).firstName();
                obj.last_name = user.usersList(i).lastName();
                obj.emp_id = user.usersList(i).empId();
                obj.emp_photo_url = user.usersList(i).empPhotoUrl();
                obj.email_id = user.usersList(i).emailId();
                obj.status = user.usersList(i).status();
                obj.appraiser_id = user.usersList(i).appraiserId();
                obj.created_user_id = user.usersList(i).createdUserId().toString();
                obj.updated_user_id = user.usersList(i).updatedUserId().toString();
                obj.role_id = user.usersList(i).roleId();
                obj.isSelected = false;
                var status = obj.status
                if (tabName === 'Active' && status === 1) {
                    arr.push(obj);
                } if (tabName === 'In-Active' && status === 0) {
                    arr.push(obj);
                } if (tabName === 'All') {
                    arr.push(obj);
                }
            }
            dispatch(getAllUserProfile(arr));
        }).catch((error) => {
            dispatch(getAllUserProfileFailed(error));
        });
    }



};



export const showPopUpAction: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (term: String) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopupSuccess(response));
        }).catch((error) => {
            dispatch(showPopupSuccess(error));
        });
    };
};
export const hidePopUp: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(hide());
        }).catch((error) => {
            dispatch(showPopupSuccess(error));
        });
    }
}
export const getEmployeeKRAHistory: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (data) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.getKRAUserHistory(data).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../KRAHistoryManagement/schema/History/Request/KraHistory_generated').kraUserHistory
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var history = userData.kraHistory.getRootAskraHistory(byteBuffer);
            var historyLength = history.historyListLength();

            let arr = []
            if (historyLength) {
                const obj: any = {};
                obj.id = history.historyList(0).id().toFloat64();
                obj.userId = history.historyList(0).userId().toFloat64();
                obj.startDate = history.historyList(0).startDate();
                obj.endDate = history.historyList(0).endDate();
                obj.name = history.historyList(0).appraiserName();
                obj.appraisalYear = history.historyList(0).appraisalYear();
                arr.push(obj);
            }
            dispatch(getHistory(arr));
        }).catch((error) => {
            dispatch(showPopupSuccess(error));
        });
    }
}

export const logout: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('../../../schema/Logout/loginhistory_generated').Logout;
        var addProfileData = addProfile.loginhistory
        var fbb = new flatbuffers.Builder(0);
        // var id = '';
        var login_date_time = '';
        var user_id = fbb.createLong(param.id);
        var device_id = '';
        var device_name = '';
        var user_ip = '';
        var user_location_name = '';
        var logout_date_time = '';
        addProfileData.startloginhistory(fbb)
        addProfileData.addLoginDateTime(fbb, login_date_time)
        addProfileData.addUserId(fbb, user_id)
        addProfileData.addDeviceId(fbb, device_id)
        addProfileData.addDeviceName(fbb, device_name)
        addProfileData.addUserIp(fbb, user_ip)
        addProfileData.addUserLocationName(fbb, user_location_name)
        addProfileData.addLogoutDateTime(fbb, logout_date_time)
        var ints = addProfileData.endloginhistory(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.logOut(value).then((response) => {
            dispatch(logOut(true))
        }).catch((error) => {
            dispatch(logOut(false))
        });
    }
}
export const hideToaster: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmployeeMangementStore,
        null,
        EmployeeManagementAction
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showInfoAlert().then((response) => {
          dispatch(hideToasterWindow());
        }).catch((error) => {
          dispatch(hideToasterWindow());
        });
      }
}
