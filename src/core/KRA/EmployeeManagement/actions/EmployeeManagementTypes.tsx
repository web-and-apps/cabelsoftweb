/**
 * Employee Management Action Types
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */
import {
    IManageSuccessAction,
    IManageInprogressAction,
    IManageFailAction,
    IManageNoRecord,
    IGetAppraiserList,
    INewProfileValueAdded,
    INewProfileValueError,
    IShowLoading,
    IHideToaster
} from "./EmployeeManagementInterface";
import {
    IManageActiveProfileAction,
    IManageInActiveProfileAction,
    IManageAllProfileAction
} from "./EmployeeManagementInterface";
import {
    IGetCharactersStartAction,
    IGetCharactersSuccessAction,
    IGetCharactersFailureAction,
    IShowPopup,
    IGetHistory,
    IGetEmpSuccessInfo,
    IGetEmpFailureInfo,
    ISuccess,
    ILogOut,
    IHideLoading
} from "./EmployeeManagementInterface";

type EmployeeManagementAction =
    | IManageSuccessAction
    | IManageInprogressAction
    | IManageFailAction
    | IManageNoRecord
    | IManageActiveProfileAction
    | IManageInActiveProfileAction
    | IManageAllProfileAction
    | IGetCharactersStartAction
    | IGetCharactersSuccessAction
    | IGetCharactersFailureAction
    | IShowPopup
    | IGetHistory
    |IGetAppraiserList
    | IGetEmpSuccessInfo
    |ISuccess
    | IGetEmpFailureInfo
    |ILogOut
    |INewProfileValueAdded
    |INewProfileValueError
    | IShowLoading
    |IHideLoading
    |IHideToaster

export default EmployeeManagementAction;