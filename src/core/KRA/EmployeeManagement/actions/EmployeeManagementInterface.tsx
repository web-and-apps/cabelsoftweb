/**
 * Employee Mangement Interface
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */

import keys from './EmployeeManagementTypeKeys';

export interface IManageSuccessAction {
    readonly type: keys.MANAGE_PROFILE_SUCCESS;
    readonly value: any;
    readonly isEmpty: Boolean;
    readonly showLoading: Boolean;
}
export interface IGetAppraiserList {
    readonly type: keys.GET_APPRAISER_LIST;
    readonly value: any;
    readonly isEmpty: Boolean;
    readonly showLoading: Boolean;
}
export interface ILogOut {
    readonly type: keys.LOGOUT;
    readonly value: Boolean;
}
export interface ISuccess {
    readonly type: keys.SUCCESS;
    readonly value: any;
}

export interface IShowPopup {
    readonly type: keys.SHOW_POPUP;
    readonly value: any;
}
export interface IGetEmpSuccessInfo {
    readonly type: keys.GET_EMP_DETAIL_SUCCESS;
    readonly value: any;
}
export interface IGetEmpFailureInfo {
    readonly type: keys.GET_EMP_DETAIL_FAILURE;
    readonly payload: {
        readonly error: Error;
    };
}
export interface IGetHistory {
    readonly type: keys.GETHISTORY;
    readonly value: any;
}

export interface IManageInprogressAction {
    readonly type: keys.MANAGE_PROFILE_INPROGRESS;
}

export interface IManageFailAction {
    readonly type: keys.MANAGE_PROFILE_FAIL;
    readonly showLoading: Boolean;
    readonly payload: {
        readonly error: Error;
    };
}

export interface IManageNoRecord {
    readonly type: keys.MANAGE_NORECORD;
    readonly isEmpty: Boolean;
}

export interface IManageActiveProfileAction {
    readonly type: keys.ACTIVE_PROFILE;
}

export interface IManageInActiveProfileAction {
    readonly type: keys.INACTIVE_PROFILE;
}

export interface IManageAllProfileAction {
    readonly type: keys.ALL_PROFILE;
}

export interface IGetCharactersStartAction {
    readonly type: keys.GET_CHARACTERS_START;
}

export interface IGetCharactersSuccessAction {
    readonly type: keys.GET_CHARACTERS_SUCCESS;
}

export interface IGetCharactersFailureAction {
    readonly type: keys.GET_CHARACTERS_FAILURE;
}
export interface INewProfileValueAdded {
    readonly type: keys.New_Profile_Value_Added;
    readonly value: any;
    readonly showPopup: Boolean;
    readonly Loading: Boolean;
    readonly showLoading: Boolean;
    readonly isSuccess: Boolean;
    readonly successMessage: Boolean;
}
export interface INewProfileValueError {
    readonly type: keys.New_Profile_Value_Error;
    readonly value: any;
    readonly showPopup: Boolean;
    readonly Loading: Boolean;
    readonly showLoading: Boolean;
}
export interface IShowLoading {
    readonly type: keys.SHOWLOADING;
    readonly showLoading: Boolean;
}
export interface IHideLoading {
    readonly type: keys.HIDELOADING;
    readonly showLoading: Boolean;
}
export interface IHideToaster {
    readonly type: keys.HIDETOASTER;
    readonly isSuccess: Boolean;
}