/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import keys from "./UserGoalDetailManagementActionTypeKeys";

export interface IGetUserDetails {
    readonly type: keys.GETUSERDETAILS,
    value: any,
    rating:any,
    showLoading: boolean,
}
export interface IShowPopup {
    readonly type: keys.SHOWPOPUP,
    showPopUp: any
}
export interface IDeleteGoal {
    type: keys.DELETEGOAL,
    isSuccess: true,
    successMessage: any
}
export interface IGetComments {
    readonly type: keys.GETCOMMENTS,
    showPopUp: any,
    commentsData:any,
    fileUrl:any
}
export interface ISuccess {
    readonly type: keys.SUCCESS,
    success: Boolean,
    rating:any,
    isSuccess:any,
    showLoading: boolean,
    successMessage: any
}
export interface IShowLoading {
    readonly type: keys.SHOWLOADING;
    readonly showLoading: boolean;
}
export interface IHideLoading {
    readonly type: keys.HIDELOADING;
    readonly showLoading: boolean;
}
export interface IHideToaster {
    readonly type: keys.HIDETOASTER;
    readonly isSuccess: boolean;
}