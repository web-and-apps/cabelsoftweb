/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import {
    IGetUserDetails,
    IShowPopup,
    IGetComments,
    ISuccess,
    IShowLoading,
    IHideLoading,
    IHideToaster,
    IDeleteGoal
} from "./UserGoalDetailManagementActionInterface";


type UserGoalDetailManagementTypes = 
|IGetUserDetails
|IShowPopup
|IGetComments
|ISuccess
|IHideLoading
|IShowLoading
|IHideLoading
|IHideToaster
|IDeleteGoal


export default UserGoalDetailManagementTypes;