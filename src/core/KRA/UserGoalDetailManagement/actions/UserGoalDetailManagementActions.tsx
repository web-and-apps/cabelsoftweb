/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import UserGoalDetailManagementState from '../state/UserGoalDetailManagementState';
import UserGoalDetailManagementActionTypes from './UserGoalDetailManagementActionTypes';
import UserGoalDetailManagementActionTypeKeys from './UserGoalDetailManagementActionTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';
import { ProfileData } from '../../../../components/KRAProfileCard/KRAProfileCardComponents';

export const getAllUserProfile = (data: any,rating:any, value?: any): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.GETUSERDETAILS,
        value: data,
        rating:rating,
        showLoading: false,
    }
}
export const deleteUserGoal = (value?: any): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.DELETEGOAL,
        isSuccess: true,
        successMessage: value
    }
}
export const success = (rating:any, value?: any): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.SUCCESS,
        success: true,
        rating:rating,
        showLoading: false,
        isSuccess: true,
        successMessage: value
    }
}
// export const hideLoader = (): UserGoalDetailManagementActionTypes => {
//     return {
//         type: UserGoalDetailManagementActionTypeKeys.SHOWLOADING,
//         showLoading: false,
//     }
//   }
export const showLoader = (): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.SHOWLOADING,
        showLoading: true,
    }
  }
export const hideLoader = (): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.HIDELOADING,
        showLoading: false,
    }
  }
  export const hideToasterWindow = (): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.HIDETOASTER,
        isSuccess: false,
    }
  }
const getKraDetails = (data: any): Promise<any> => {
    return new Promise((resolve, reject) => {
        const service = new APIUserManagement();
        return service.getKRASpecificUserHistory(data, 'current').then((response) => {
            console.log(response);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var kraData = require('../schema/kraList_generated').kraList;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var kraValue = kraData.kraList.getRootAskraList(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var kraLength = kraValue.kraresLength();
            var feedBackLength = kraValue.feedbackListLength();
            var kraDetailsListLength = kraValue.kradetailsListLength();
            const kraValueArr = [];
            const feedBackValueArr = [];
            const kraDetailsListArr = [];
            for (let i = 0; i <= kraLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.krares(i).id().toFloat64();
                obj.user_id = kraValue.krares(i).userId().toFloat64();
                obj.start_date = kraValue.krares(i).startDate().toString();
                obj.end_date = kraValue.krares(i).endDate();
                obj.updated_user_id = kraValue.krares(i).updatedUserId().toFloat64();
                obj.created_user_id = kraValue.krares(i).createdUserId().toFloat64();
                obj.status = kraValue.krares(i).status();
                obj.roleId = kraValue.krares(i).roleId();
                obj.userName = kraValue.krares(i).userName();
                obj.user = kraValue.krares(i).user();
                obj.apraisalYear = kraValue.krares(i).apraisalYear();
                obj.rating = kraValue.krares(i).rating();
                kraValueArr.push(obj)
            }
            for (let i = 0; i <= feedBackLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.feedbackList(i).id().toFloat64();
                obj.kra_id = kraValue.feedbackList(i).kraId().toFloat64();
                obj.user_id = kraValue.feedbackList(i).feedbackUserId().toFloat64();
                obj.feedback_comment = kraValue.feedbackList(i).feedbackComment().toString();
                obj.system_comment = kraValue.feedbackList(i).systemComment();
                obj.created_date = kraValue.feedbackList(i).createdDate();
                obj.creator_name = kraValue.feedbackList(i).creatorName();
                obj.creator_image = kraValue.feedbackList(i).creatorImage();
                feedBackValueArr.push(obj)
            }
            for (let i = 0; i <= kraDetailsListLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.kradetailsList(i).id().toFloat64();
                obj.goal_type_id = kraValue.kradetailsList(i).goalType();
                obj.kra_id = kraValue.kradetailsList(i).kraId().toFloat64();
                obj.goalId = kraValue.kradetailsList(i).goalId().toFloat64();
                obj.description = kraValue.kradetailsList(i).goal();
                // obj.created_user_id = "";
                // obj.updated_user_id = "";
                // obj.created_date = "";
                // obj.update_date = "";
                // obj.created_user_name = "";
                // obj.created_user_image = "";
                obj.created_date = kraValue.kradetailsList(i).goalCreatedDate();
                obj.update_date = kraValue.kradetailsList(i).goalUpdatedDate();
                obj.created_user_name = kraValue.kradetailsList(i).goalCreatorName();
                obj.created_user_image = kraValue.kradetailsList(i).goalCreatorImage();
                kraDetailsListArr.push(obj)
            }
            const finalArray = {
                kraValueArr: kraValueArr,
                feedBackValueArr: feedBackValueArr,
                kraDetailsListArr: kraDetailsListArr,
            }
            console.log("finalArray",finalArray)
            resolve(finalArray);
        }).catch((error) => {
            reject(error)
        });
    })
}
export const showPopup = (data: any): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.SHOWPOPUP,
        showPopUp: data
    }
}
export const getCommentdata = (data: any,fileUrl:any): UserGoalDetailManagementActionTypes => {
    return {
        type: UserGoalDetailManagementActionTypeKeys.GETCOMMENTS,
        showPopUp: true,
        commentsData:data,
        fileUrl:fileUrl
    }
}
export const getEmployeeDetails: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (data: any, type: string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.getKRASpecificUserHistory(data, type).then((response) => {
            console.log(response);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var kraData = require('../schema/kraList_generated').kraList;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var kraValue = kraData.kraList.getRootAskraList(byteBuffer);
            var kraLength = kraValue.kraresLength();
            var feedBackLength = kraValue.feedbackListLength();
            var kraDetailsListLength = kraValue.kradetailsListLength();
            const kraValueArr = [];
            const feedBackValueArr = [];
            const kraDetailsListArr = [];
            var rating:any
            for (let i = 0; i <= kraLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.krares(i).id().toFloat64();
                obj.user_id = kraValue.krares(i).userId().toFloat64();
                obj.start_date = kraValue.krares(i).startDate().toString();
                obj.end_date = kraValue.krares(i).endDate();
                obj.updated_user_id = kraValue.krares(i).updatedUserId().toFloat64();
                obj.created_user_id = kraValue.krares(i).createdUserId().toFloat64();
                obj.status = kraValue.krares(i).status();
                obj.roleId = kraValue.krares(i).roleId();
                obj.userName = kraValue.krares(i).userName();
                obj.user = kraValue.krares(i).user();
                obj.apraisalYear = kraValue.krares(i).apraisalYear();
                rating = kraValue.krares(i).rating();
                kraValueArr.push(obj)
            }
            console.log("kraValueArr",kraValueArr)
            for (let i = 0; i <= feedBackLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.feedbackList(i).id().toFloat64();
                obj.kra_id = kraValue.feedbackList(i).kraId().toFloat64();
                obj.user_id = kraValue.feedbackList(i).feedbackUserId().toFloat64();
                obj.feedback_comment = kraValue.feedbackList(i).feedbackComment().toString();
                obj.system_comment = kraValue.feedbackList(i).systemComment();
                obj.created_date = kraValue.feedbackList(i).createdDate();
                obj.creator_name = kraValue.feedbackList(i).creatorName();
                obj.creator_image = kraValue.feedbackList(i).creatorImage();
                feedBackValueArr.push(obj)
            }
            for (let i = 0; i <= kraDetailsListLength - 1; i++) {
                let obj: any = {}
                obj.id = kraValue.kradetailsList(i).id().toFloat64();
                obj.kra_id = kraValue.kradetailsList(i).kraId().toFloat64();
                obj.goalId = kraValue.kradetailsList(i).goalId().toFloat64();
                obj.goal_type_id = kraValue.kradetailsList(i).goalType();
                obj.description = kraValue.kradetailsList(i).goal();
                // obj.created_user_id = kraValue.kradetailsList(i).goalCreatedDate();
                // obj.updated_user_id = kraValue.kradetailsList(i).goalUpdatedDate();
                obj.created_date = kraValue.kradetailsList(i).goalCreatedDate();
                obj.update_date = kraValue.kradetailsList(i).goalUpdatedDate();
                obj.created_user_name = kraValue.kradetailsList(i).goalCreatorName();
                obj.created_user_image = kraValue.kradetailsList(i).goalCreatorImage();
                kraDetailsListArr.push(obj)
            }
            const finalArray = {
                kraValueArr: kraValueArr,
                feedBackValueArr: feedBackValueArr,
                kraDetailsListArr: kraDetailsListArr,
            }
            dispatch(getAllUserProfile(finalArray,rating));
        }).catch((error) => {
            // debugger
            if(error && error.response && error.response.status === 404) {
                window.alert("KRA not found")
            } else {
                window.alert("Internal server error")
            }
            dispatch(hideLoader());
        });
    }
}
export const addComments: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (data: any,userData: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../schema/feedback_generated').AddComments;
        var addcomments = goaldetails.feedback;
         var fbb = new flatbuffers.Builder(0);
         var id = fbb.createLong('');
         var kraid = fbb.createLong(userData.kraId);
         var feedback_user_id = fbb.createLong(userData.userId);
         var feedback_comment = fbb.createString(data);
         var system_comment = fbb.createLong(false);
        //  id = fbb.createLong('');
         addcomments.startfeedback(fbb);
         addcomments.addId(fbb, id)
         addcomments.addKraId(fbb, kraid)
         addcomments.addFeedbackUserId(fbb, feedback_user_id)
         addcomments.addFeedbackComment(fbb, feedback_comment)
         addcomments.addSystemComment(fbb, system_comment)

        var ints = addcomments.endfeedback(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();

        return service.addComments(value,userData.kraId).then((response) => {
            getKraDetails(userData)
            .then((response) => {
                dispatch(showLoader())
                console.log(response)
                var kraValueArr = response.kraValueArr
                var rating = kraValueArr[0].rating
                dispatch(getAllUserProfile(response,rating));
            })
            .catch((error) => {
                dispatch(hideLoader());
                console.log(error)
            })
            

        }).catch((error) => {
            dispatch(hideLoader());
            console.log("add Goals error", error)

        });
    }
}
export const closeKra: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (data: any,KraProfikleData: ProfileData[], urlValue) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        console.log("add comment Data", data)
        console.log("add kraId Data", KraProfikleData[0].id)
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../schema/kra_generated').CloseKra;
        var addcomments = goaldetails.kra;
        var fbb = new flatbuffers.Builder(0);
        var kraid = fbb.createLong(KraProfikleData[0].id);
        var userid = fbb.createLong(KraProfikleData[0].user_id);
        var start_date = fbb.createString(KraProfikleData[0].start_date);
        var end_date = fbb.createString(KraProfikleData[0].end_date);
        var updated_user_id = fbb.createLong(KraProfikleData[0].updated_user_id);
        var created_user_id = fbb.createLong(KraProfikleData[0].created_user_id);
        var status = fbb.createLong(false);
        var rating = data;
        // debugger
        addcomments.startkra(fbb);
        addcomments.addId(fbb, kraid)
        addcomments.addUserId(fbb, userid)
        addcomments.addStartDate(fbb, start_date)
        addcomments.addEndDate(fbb, end_date)
        addcomments.addUpdatedUserId(fbb, updated_user_id)
        addcomments.addCreatedUserId(fbb, created_user_id)
        addcomments.addStatus(fbb, status)
        addcomments.addRating(fbb, rating)

        var ints = addcomments.endkra(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();

        return service.closeKRA(value).then((response) => {
                console.log(response)
                dispatch(success(data,"KRA has been closed successfully"));
        }).catch((error) => {
            dispatch(hideLoader());
            console.log("add Goals error", error)

        });
    }
}
export const actionShowPopup: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopup(!data));   
        }).catch((error) => {
            dispatch(showPopup(data));
        });
    }
}
export const getComments: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (kraId: any, goalId: any) => {
    // var kraid= kraId
    // var isArray = Array.isArray(KraProfileData);
    // if(isArray){
    //     kraid = KraProfileData[0].kra_id;
    // } else {
    //     kraid = KraProfileData;
    // }
    // console.log("kraid",kraid)
    // console.log("goalId",goalId)
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.getComments(goalId, kraId).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../AppraiserManagement/schema/appraisee_generated_generated').Appraisercomment;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var commets = userData.commentList.getRootAscommentList(byteBuffer);
            var commentListLength = commets.commentListLength();
            var commentList = commets.commentList();
            console.log("commentListLength",commentListLength)
            console.log("commentList",commentList)
            var filePath = "";
            var dataArr = []
            // var obj = {
            //   id: 0,
            //   kra_id: "",
            //   goal_id: "",
            //   comment: "",
            //   created_date: "",
            //   updated_date: "",
            //   save_as_draft: "",
            // }
            for(let i=0; i < commentListLength;i++){
              let obj: any = {}
              obj.id= commets.commentList(i).id().toFloat64();
              obj.kra_id= commets.commentList(i).kraId().toFloat64();
              obj.goal_id= commets.commentList(i).goalId().toFloat64();
              obj.comment= commets.commentList(i).comment();
              obj.created_date= commets.commentList(i).createdDate();
              obj.updated_date= commets.commentList(i).updatedDate();
              obj.save_as_draft= commets.commentList(i).saveAsDraft();
              obj.filepath= commets.commentList(i).filepath();
              filePath = commets.commentList(0).filepath();
              dataArr.push(obj)
            }
            
            console.log("dataArr", dataArr)
            dispatch(getCommentdata(dataArr,filePath));  
            dispatch(hideLoader());  
        }).catch((error) => {
            dispatch(hideLoader());
            if (error && error.response && error.response.status === 500){
                window.alert("Internal server error!");
                // dispatch(showPopup("Internal Server Error!"));
            } else if((error && error.response && error.response.status === 404)){
                window.alert("Comment not found");
            } else{
                let responseSuccess
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
                var responseArray = new Uint8Array(error.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.response.getRootAsresponse(byteBuffer);
                responseSuccess = responseStatus.message();
                window.alert(responseSuccess);
            // dispatch(showPopup(error));
            }
        });
    }
}

export const getgoalComments: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (goalId: any, kraid: any) => {
    
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.getComments(goalId, kraid).then((response) => {
            console.log("AppConstants.TOKEN",response.status)
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../AppraiserManagement/schema/appraisee_generated_generated').GetComments;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var commets = userData.comments.getRootAscomments(byteBuffer);
            var filePath = "";
            filePath = commets.commentList(0).filepath();
             var obj = {
              "id": commets.id().toFloat64(),
              "kra_id": commets.kraId().toFloat64(),
              "goal_id": commets.goalId().toFloat64(),
              "comment": commets.comment(),
              "created_date": commets.createdDate(),
              "updated_date": commets.updatedDate(),
              "save_as_draft": commets.saveAsDraft(),
              "filepath": commets.filepath(),
            }
            console.log("obj", obj)
            dispatch(getCommentdata(obj,filePath));   
        }).catch((error) => {
            dispatch(hideLoader());
            if (error.status === 404){

            }else{
            dispatch(showPopup(error));
            }
        });
    }
}
export const deleteGoal: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (kraId: any,goalId:any,userData: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.deleteSpecificGoals(kraId,goalId).then((response) => {
            getKraDetails(userData)
            .then((response) => {
                console.log(response)
                var kraValueArr = response.kraValueArr
                var rating = kraValueArr[0].rating
                dispatch(getAllUserProfile(response,rating));
                dispatch(deleteUserGoal("Goal deleted successfully"));
            }).catch((error) =>  {
                dispatch(hideLoader());
                console.log(error)
            })
        }).catch((error) => {
            dispatch(hideLoader());
        });
    }
}

export const hideToaster: ActionCreator<
    ThunkAction<
        Promise<any>,
        UserGoalDetailManagementState,
        null,
        UserGoalDetailManagementActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showInfoAlert().then((response) => {
          dispatch(hideToasterWindow());
        }).catch((error) => {
          dispatch(hideToasterWindow());
        });
      }
}