/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */


export enum UserGoalDetailManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum UserGoalDetailManagementActionTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    HIDEPOPUP = "HIDE_POPUP",
    GETUSERDETAILS = "GET_USER_DETAILS",
    SHOWPOPUP = "SHOWPOPUP",
    GETCOMMENTS = "GETCOMMENTS",
    SUCCESS = "SUCCESS",
    SHOWLOADING = "SHOWLOADING",
    HIDELOADING = "HIDELOADING",
    HIDETOASTER = "HIDETOASTER",
    DELETEGOAL = "DELETEGOAL",
}

export default UserGoalDetailManagementActionTypeKeys;