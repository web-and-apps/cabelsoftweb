/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */
import { KRAGoalModels } from "../../../../components/KRAGoal/KRAGoalComponents";
export default interface UserGoalDetailManagementState {
    userDetails:any;
    showPopUp:Boolean;
    commentData:any;
    closeKRA:Boolean;
    rating:any;
    showLoading:any;
    fileUrl:any;
    isSuccess:Boolean;
    successMessage:any;
}