/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import UserGoalDetailManagementState from '../state/UserGoalDetailManagementState';
import UserGoalDetailManagementActionTypes from '../actions/UserGoalDetailManagementActionTypes';
import UserGoalDetailManagementActionTypeKeys from '../actions/UserGoalDetailManagementActionTypeKeys';

const initialState: UserGoalDetailManagementState = {
    userDetails:{},
    showPopUp:false,
    commentData:'',
    closeKRA:false,
    rating:0,
    showLoading: false,
    fileUrl:"",
    isSuccess:false,
    successMessage:"",
};

const UserGoalDetailManagementReducers: Reducer<UserGoalDetailManagementState, UserGoalDetailManagementActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case UserGoalDetailManagementActionTypeKeys.GETUSERDETAILS: {
            return {
                ...state,
                userDetails: action.value,
                rating: Number(action.rating),
                showLoading: action.showLoading,
            };
        }
        case UserGoalDetailManagementActionTypeKeys.SHOWPOPUP: {
            return {
                ...state,
                showPopUp: action.showPopUp,
                showLoading:false
            };
        }
        case UserGoalDetailManagementActionTypeKeys.SHOWLOADING: {
            return {
                ...state,
                showLoading: action.showLoading
            };
        }
        case UserGoalDetailManagementActionTypeKeys.DELETEGOAL: {
            return {
                ...state,
                isSuccess: action.isSuccess,
                successMessage: action.successMessage,
            };
        }
        case UserGoalDetailManagementActionTypeKeys.HIDELOADING: {
            return {
                ...state,
                showLoading: action.showLoading
            };
        }
        case UserGoalDetailManagementActionTypeKeys.HIDETOASTER: {
            return {
                ...state,
                isSuccess: action.isSuccess
            };
        }
        case UserGoalDetailManagementActionTypeKeys.SUCCESS: {
            return {
                ...state,
                closeKRA: action.success,
                rating: action.rating,
                showLoading: action.showLoading,
                isSuccess: action.isSuccess,
                successMessage: action.successMessage,
            };
        }
        case UserGoalDetailManagementActionTypeKeys.GETCOMMENTS: {
            return {
                ...state,
                showPopUp: action.showPopUp,
                commentData: action.commentsData,
                fileUrl: action.fileUrl,
                showLoading:false
            };
        }
        default:
            return state;
    }
};

export default UserGoalDetailManagementReducers;