/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import keys from "./AssignKRAGoalManagementActionTypeKeys";

export interface IManageSuccessAction {
    readonly type: keys.MANAGE_PROFILE_SUCCESS;
    readonly value: any;
    readonly levelStatus:any;
    readonly isEmpty: Boolean;
}
export interface ISelectIndividualEmployee {
    readonly type: keys.SELECTINDIVIDUALEMPLOYEE;
    readonly profileValue: any;
    readonly levelStatus:any;
    readonly isEmpty: Boolean;
}
export interface IManageFailAction {
    readonly type: keys.MANAGE_PROFILE_FAIL;
    readonly payload: {
        readonly error: Error;
    };
}
export interface IManageFailAction {
    readonly type: keys.MANAGE_PROFILE_FAIL;
    readonly payload: {
        readonly error: Error;
    };
}
export interface ISelectAll {
    readonly type: keys.SELECTALL;
    readonly value: Boolean;
    readonly goalValue:any
}
export interface ISelectAllEmployee {
    readonly type: keys.SELECTALLEMPLOYEE;
    readonly value: Boolean;
    readonly profileValue:any;
    readonly levelStatus:any;
}
export interface IGetAllGoals {
    readonly type: keys.GETALLGOALS;
    readonly goalValue:any,
    readonly isEmpty: Boolean;
}
export interface IGoalAssigned {
    readonly type: keys.GOAL_ASSIGNED;
    isAssigned:any;
    isSuccess: Boolean;
    successMessage: any
}
export interface IShowLoading {
    readonly type: keys.SHOWLOADING;
    showLoading: Boolean;
}
export interface IHideLoading {
    readonly type: keys.HIDELOADING;
    showLoading: Boolean;
}
