/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */


export enum AssignKRAGoalManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum AssignKRAGoalManagementActionTypeKeys {
    MANAGE_PROFILE_SUCCESS = "MANAGE_PROFILE_SUCCESS",
    MANAGE_PROFILE_FAIL = "MANAGE_PROFILE_FAIL",
    SELECTALL = "SELECTALL",
    GETALLGOALS = "GETALLGOALS",
    GOAL_ASSIGNED = "GOAL_ASSIGNED",
    SELECTALLEMPLOYEE = "SELECTALLEMPLOYEE",
    SELECTINDIVIDUALEMPLOYEE = "SELECTINDIVIDUALEMPLOYEE",
    SHOWLOADING = "SHOWLOADING",
    HIDELOADING = "HIDELOADING",
}

export default AssignKRAGoalManagementActionTypeKeys;