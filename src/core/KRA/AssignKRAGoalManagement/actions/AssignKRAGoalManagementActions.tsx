/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import AssignKRAGoalManagementState from '../state/AssignKRAGoalManagementState';
import AssignKRAGoalManagementActionTypes from './AssignKRAGoalManagementActionTypes';
import AssignKRAGoalManagementActionTypeKeys from './AssignKRAGoalManagementActionTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';
import { EmployeeCardModel } from '../../../../components/EmployeeCard/EmployeeCardComponents'
import { KRAGoalModels } from "../../../../components/KRAGoal/KRAGoalComponents";

export const getAllUserProfile = (data: any,levelStatus:any): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.MANAGE_PROFILE_SUCCESS,
        value: data,
        levelStatus:levelStatus,
        isEmpty: false
    }
}
export const getAllGoals = (data: any): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.GETALLGOALS,
        goalValue: data,
        isEmpty: true,
    }
}
export const selectAll = (data: Boolean, goalsArray: any): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.SELECTALL,
        value: data,
        goalValue: goalsArray,
    }
}
export const SelectIndividualEmployee = (data: Boolean, profilvalue: any,levelStatus:[]): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.SELECTINDIVIDUALEMPLOYEE,
        profileValue: profilvalue,
        isEmpty:data,
        levelStatus:levelStatus,
    }
}
export const selectAllEmployees = (data: Boolean, profilvalue: any,levelStatus:[]): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.SELECTALLEMPLOYEE,
        value: data,
        profileValue: profilvalue,
        levelStatus:levelStatus,
    }
}
export const getAllUserProfileFailed = (error: Error): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.MANAGE_PROFILE_FAIL,
        payload: { error }
    }
}
export const goalAssignedSuccessfully = (value?: any): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.GOAL_ASSIGNED,
        isAssigned: true,
        isSuccess: true,
        successMessage: value
    }
}
export const showLoader = (): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.SHOWLOADING,
        showLoading: true,
    }
  }
export const hideLoader = (): AssignKRAGoalManagementActionTypes => {
    return {
        type: AssignKRAGoalManagementActionTypeKeys.HIDELOADING,
        showLoading: false,
    }
  }
export const selectAllAction: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (data: Boolean, goalArray: Array<KRAGoalModels>) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.selectAll().then((response) => {
            if (goalArray.length > 0) {
                var customizedGoalsArr = goalArray.length > 0 ? goalArray.filter(data => data.goal_type_id == '2') : [];
                var Checkfalse = goalArray.length > 0 ? customizedGoalsArr.filter(data => data.isSelected == false) : [];
                var Checktrue = goalArray.length > 0 ? customizedGoalsArr.filter(data => data.isSelected == true) : [];
                if (Checkfalse.length === customizedGoalsArr.length){
                    for (let i = 0; i < customizedGoalsArr.length; i++) {
                        customizedGoalsArr[i].isSelected = true;
                     } 
                    data = true
                } else if (Checktrue.length === customizedGoalsArr.length){
                    for (let i = 0; i < customizedGoalsArr.length; i++) {
                        customizedGoalsArr[i].isSelected = false;
                     }  
                    data = false
                } else{
                    for (let i = 0; i < customizedGoalsArr.length; i++) {
                        customizedGoalsArr[i].isSelected = true;
                     } 
                }
             }
             localStorage.setItem("goalarray", JSON.stringify(goalArray));
                dispatch(selectAll(!data, goalArray));
        }).catch((error) => {
            dispatch(selectAll(data, goalArray));
        });
    };
};
export const selectIndividualGoal: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (data: Boolean, goalArray: any,selectedGoal:any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.selectAll().then((response) => {
            if (goalArray.length > 0) {
                for (let i=0;i < goalArray.length; i++){
                    // var goalData: Array<KRAGoalModels>
                    let goal = goalArray[i]
                    if (selectedGoal === goal) {
                        goalArray[i].isSelected = !goalArray[i].isSelected
                    }
                }
            }
            localStorage.setItem("goalarray", JSON.stringify(goalArray));
                dispatch(selectAll(!data, goalArray));
        }).catch((error) => {
            dispatch(selectAll(data, goalArray));
        });
    };
};

export const selectindividualEmployee: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (data:Boolean,singleSelectionData:EmployeeCardModel, profileArray: any,levelStatus:any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.selectAll().then((response) => {
            if (profileArray.length > 0) {
                for (let i = 0; i < profileArray.length; i++) {
                    var employeeArray: Array<EmployeeCardModel>
                    employeeArray = profileArray[i]
                     for (let j = 0; j < employeeArray.length; j++) {
                         if (singleSelectionData === employeeArray[j]){
                            selectUnSelectInLocalData(employeeArray[j],!employeeArray[j].isSelected)
                            employeeArray[j].isSelected = !employeeArray[j].isSelected  
                         }
                     } 
                     var CheckTrue = employeeArray.length > 0 ? employeeArray.filter(data => data.isSelected == true) : [];
                     var emptyArray: Array<Boolean>
                     emptyArray = levelStatus
                     if (CheckTrue.length === employeeArray.length){   
                        emptyArray[i] = true
                     } else{
                        emptyArray[i] = false
                     }
                 }
             } 
             dispatch(SelectIndividualEmployee(!data,profileArray,levelStatus));
        }).catch((error) => {
             dispatch(SelectIndividualEmployee(data,profileArray,levelStatus));
        });
    };
};

export const selectAllEmployeeAction: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (index:any,data: Boolean, profileArray: any,levelStatus:any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.selectAll().then((response) => {
            if (index === "all") {
                var emptyArray: Array<Boolean>
                emptyArray = levelStatus
                var checkArrayStatus = emptyArray.every(a => a === emptyArray[0])
                if (checkArrayStatus === true) {              
                   if (emptyArray[0] === false) {
                    checkArrayStatus = false
                   }
                }
                for (let i = 0; i < profileArray.length; i++) {
                    var employeeArray: Array<EmployeeCardModel>
                    employeeArray = profileArray[i]
                    if (checkArrayStatus === false){
                        emptyArray[i] = true
                    }else{
                        emptyArray[i] = false
                    }
                    for (let j = 0; j < employeeArray.length; j++) {
                        selectUnSelectInLocalData(employeeArray[j],!checkArrayStatus)
                        employeeArray[j].isSelected = !checkArrayStatus;
                    }
                }
            } else {
                let employeeArray: Array<EmployeeCardModel>
                employeeArray = profileArray[index]
                var emptyArray: Array<Boolean>
                emptyArray = levelStatus
                for (let j = 0; j < employeeArray.length; j++) {
                    selectUnSelectInLocalData(employeeArray[j],!emptyArray[index])
                    employeeArray[j].isSelected = !emptyArray[index];
                    
                } 
                emptyArray[index] = !emptyArray[index]
             }
            dispatch(selectAllEmployees(!data, profileArray,levelStatus));
        }).catch((error) => {
            dispatch(selectAllEmployees(data, profileArray,levelStatus));
        });
    };
};
export const getUserProfile: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        // console.log(service.get());
        return service.getActiveEmployee().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            var levelStatus = [];

            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
                levelStatus.push(false)
            }
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (let j = 0; j < levelLength; j++) {
                    let obj: any = {}
                    obj.id = user.userlevel(j).id().toFloat64();
                    obj.title = user.userlevel(j).title();
                    obj.first_name = user.userlevel(j).firstName();
                    obj.last_name = user.userlevel(j).lastName();
                    obj.emp_id = user.userlevel(j).empId();
                    obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
                    obj.status = user.userlevel(j).status();
                    obj.email_id = user.userlevel(j).emailId();
                    obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
                    obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
                    obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
                    obj.role_id = user.userlevel(j).roleId();
                    obj.empLevel = user.userlevel(j).empLevel();
                    obj.kraId = user.userlevel(j).kraId();
                    obj.kraStartDate = user.userlevel(j).kraStartDate();
                    obj.kraEndDate = user.userlevel(j).kraEndDate();
                    obj.isSelected = false;
                    if (levels[i] === obj.empLevel) {
                        dataarr.push(obj);
                    }
                }
                levelarray.push(dataarr)
            }
            localStorage.setItem("levelarray", JSON.stringify(levelarray));
            localStorage.setItem("levelStatus", JSON.stringify(levelStatus));
            dispatch(getAllUserProfile(levelarray,levelStatus));
            dispatch(hideLoader())
        }).catch((error) => {
            localStorage.setItem("levelarray", JSON.stringify([]));
            localStorage.setItem("levelStatus", JSON.stringify([]));
            dispatch(getAllUserProfileFailed(error));
            dispatch(hideLoader())
        });
    }
}
export const assignGoals: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (profileData: any, goalArray: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        console.log("add goal DAta", profileData)
        console.log("add goal DAta", goalArray)
        console.log("add goal DAta", profileData[0].id)
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../schema/kradetailsreq_generated').AssignKRAGoals;
        // var assignGoal = goaldetails.kradetailsList
        var assignGoal = goaldetails.kradetailsreq
        //    assignGoal.length = 2
        var addGoals = []
        var addusers = []
        var fbb = new flatbuffers.Builder(0);
        for (let i = 0; i < profileData.length; i++) {
            var profile: [EmployeeCardModel] = profileData[i]
            for (let j = 0; j < profile.length; j++) {
                if (profile[j].isSelected == true) {
                    var emp_id = fbb.createLong(profile[j].id);
                    console.log("add goal DAta", profile[j].id)
                    addusers.push(emp_id)
                }
            }
        }
        for (let j = 0; j < goalArray.length; j++) {
            if (goalArray[j].isSelected == true) {
                var goal_id = fbb.createLong(goalArray[j].id);
                console.log("add goal DAta", goalArray[j].id)
                addGoals.push(goal_id)
            }
        }
        var goalvalues = assignGoal.createGoalsVector(fbb, addGoals)
        var usersvalues = assignGoal.createUsersVector(fbb, addusers)
        assignGoal.startkradetailsreq(fbb)
        assignGoal.addGoals(fbb, goalvalues)
        assignGoal.addUsers(fbb, usersvalues)
        var ints = assignGoal.endkradetailsreq(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        // console.log("sundar", value);

        return service.assignGoals(value).then((response) => {
            console.log("add Goals", response)
            dispatch(goalAssignedSuccessfully("KRA assigned successfully"))
            dispatch(hideLoader())

        }).catch((error) => {
            console.log("add Goals error", error)
            dispatch(hideLoader())
        });
    }
}
export const getGoals: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        return service.getGoals().then((response) => {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../GoalManagement/schema/ResponseSchema/Users/goalresponse_generated').GoalDetails
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var goal = userData.goalresponse.getRootAsgoalresponse(byteBuffer);
                var goalLength = goal.goalListLength();
                var arr = [];
                // var obj = {
                //     "id": "",
                //     "goal_type_id": "",
                //     "description": "",
                //     "created_user_id": "",
                //     "updated_user_id": "",
                //     "created_date": "",
                //     "update_date": "",
                //     "created_user_name": "",
                //     "created_user_image": ""
                // }
                for (let i = 0; i < goalLength; i++) {
                    let obj: any = {}
                    obj.id = goal.goalList(i).id() ? goal.goalList(i).id() : '';
                    obj.goal_type_id = goal.goalList(i).goalTypeId() ? goal.goalList(i).goalTypeId() : '';
                    obj.description = goal.goalList(i).description() ? goal.goalList(i).description() : '';
                    obj.created_user_id = goal.goalList(i).createdUserId() ? goal.goalList(i).createdUserId() : '';
                    obj.updated_user_id = goal.goalList(i).updatedUserId() ? goal.goalList(i).updatedUserId() : '';
                    obj.created_date = goal.goalList(i).createdDate() ? goal.goalList(i).createdDate() : '';
                    obj.update_date = goal.goalList(i).updateDate() ? goal.goalList(i).updateDate() : '';
                    obj.created_user_name = goal.goalList(i).createdUserName() ? goal.goalList(i).createdUserName() : '';
                    obj.created_user_image = goal.goalList(i).createdUserImage() ? goal.goalList(i).createdUserImage() : '';
                    obj.isSelected = false;
                    arr.push(obj);
                }
                localStorage.setItem("goalarray", JSON.stringify(arr));
                dispatch(getAllGoals(arr));
                dispatch(hideLoader());
            }
        }).catch((error) => {
            console.log(error)
            dispatch(getAllGoals(error));
        });
    }
}
// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchCharacters: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (term: String, filterType: String) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.search(term, filterType).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            var levelStatus = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
                levelStatus.push(false)
            }
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (let j = 0; j < levelLength; j++) {
                    let obj: any = {}
                    obj.id = user.userlevel(j).id().toFloat64();
                    obj.title = user.userlevel(j).title();
                    obj.first_name = user.userlevel(j).firstName();
                    obj.last_name = user.userlevel(j).lastName();
                    obj.emp_id = user.userlevel(j).empId();
                    obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
                    obj.status = user.userlevel(j).status();
                    obj.email_id = user.userlevel(j).emailId();
                    obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
                    obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
                    obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
                    obj.role_id = user.userlevel(j).roleId();
                    obj.empLevel = user.userlevel(j).empLevel();
                    obj.kraId = user.userlevel(j).kraId();
                    obj.kraStartDate = user.userlevel(j).kraStartDate();
                    obj.kraEndDate = user.userlevel(j).kraEndDate();
                    obj.isSelected = false;
                    if (levels[i] === obj.empLevel) {
                        dataarr.push(obj);
                    }
                }
                levelarray.push(dataarr)
            }
            dispatch(getAllUserProfile(levelarray,levelStatus));
        }).catch((error) => {
            dispatch(getAllUserProfileFailed(error));
        });
    };
};

export const searchGoalsApi: ActionCreator<
    ThunkAction<
    Promise<any>,
    AssignKRAGoalManagementState,
    null,
    AssignKRAGoalManagementActionTypes
    >
> = (term: String) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.searchGoal(term).then((response) =>  {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../GoalManagement/schema/ResponseSchema/Users/goalresponse_generated').GoalDetails
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var goal = userData.goalresponse.getRootAsgoalresponse(byteBuffer);
                var goalLength = goal.goalListLength();
                var arr = [];
                // var obj = {
                //     "id": "",
                //     "goal_type_id": "",
                //     "description": "",
                //     "created_user_id": "",
                //     "updated_user_id": "",
                //     "created_date": "",
                //     "update_date": "",
                //     "created_user_name": "",
                //     "created_user_image": ""
                // }
                for (let i = 0; i < goalLength; i++) {
                    let obj: any = {}
                    obj.id = goal.goalList(i).id() ? goal.goalList(i).id() : '';
                    obj.goal_type_id = goal.goalList(i).goalTypeId() ? goal.goalList(i).goalTypeId() : '';
                    obj.description = goal.goalList(i).description() ? goal.goalList(i).description() : '';
                    obj.created_user_id = goal.goalList(i).createdUserId() ? goal.goalList(i).createdUserId() : '';
                    obj.updated_user_id = goal.goalList(i).updatedUserId() ? goal.goalList(i).updatedUserId() : '';
                    obj.created_date = goal.goalList(i).createdDate() ? goal.goalList(i).createdDate() : '';
                    obj.update_date = goal.goalList(i).updateDate() ? goal.goalList(i).updateDate() : '';
                    obj.created_user_name = goal.goalList(i).createdUserName() ? goal.goalList(i).createdUserName() : '';
                    obj.created_user_image = goal.goalList(i).createdUserImage() ? goal.goalList(i).createdUserImage() : '';
                    obj.isSelected = false;                 
                    arr.push(obj);
                }
                let goalArray = JSON.parse(localStorage.getItem('goalarray')!!)
                if (goalArray) {
                    for (let i = 0; i < goalLength; i++) {
                        let newGoal = arr[i]
                        for (var j = 0; j < goalArray.length; j++) {
                            let oldGoal = goalArray[j]
                            if (newGoal.id === oldGoal.id) {
                                newGoal.isSelected = oldGoal.isSelected
                            }
                        }
                    }
                }
                dispatch(getAllGoals(arr));
            }
        }).catch((error) => {
            dispatch(getAllGoals(error));
        });
    };
};


// Local Search For Employee
export const searchEmployee: ActionCreator<
    ThunkAction<
        Promise<any>,
        AssignKRAGoalManagementState,
        null,
        AssignKRAGoalManagementActionTypes
    >
> = (searchString:any,FilterType:any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        // Get Data From Local Storage
        var levelarray: any
        var Statuslevel: any
        if (localStorage.getItem('levelarray') !== "" || localStorage.getItem('levelarray') !== null) {
            levelarray = JSON.parse(localStorage.getItem('levelarray')!!)
        }
        if (localStorage.getItem('levelStatus') !== "" || localStorage.getItem('levelStatus') !== null) {
            Statuslevel = JSON.parse(localStorage.getItem('levelStatus')!!)
        }
        const service = new APIUserManagement();
        return service.searchEmployee().then((response) => {
            var dataarr = [];
            var statusArray = [];
            if (levelarray.length > 0) {
                for (let i = 0; i < levelarray.length; i++) {
                    var emptyArray: Array<Boolean>
                    emptyArray = Statuslevel
                    var employeeArray: Array<EmployeeCardModel>
                    employeeArray = levelarray[i]
                    employeeArray = employeeArray.filter(function(e){
                      return  e.first_name.toLowerCase().includes(searchString.toLowerCase()) ||  e.emp_id.toLowerCase().includes(searchString.toLowerCase())
                    });
                    if (employeeArray.length > 0) {
                        dataarr.push(employeeArray)
                        statusArray.push(emptyArray[i])
                    }
                    var CheckTrue = employeeArray.length > 0 ? employeeArray.filter(data => data.isSelected == true) : [];
                     if (CheckTrue.length === employeeArray.length){   
                        statusArray[i] = true
                     } else{
                        statusArray[i] = false
                     }
                }
            }
            dispatch(getAllUserProfile(dataarr,statusArray));
            dispatch(hideLoader())
        }).catch((error) => {
        });
    };
};

// Select and UnSelect Local Data Based on the user Action
function selectUnSelectInLocalData(singleSelectionData:EmployeeCardModel, selected:Boolean){
 // Change the value in local Array
 var levelarray: any
 var Statuslevel: any
 if (localStorage.getItem('levelarray') !== "" || localStorage.getItem('levelarray') !== null) {
     levelarray = JSON.parse(localStorage.getItem('levelarray')!!)
 }
 if (localStorage.getItem('levelStatus') !== "" || localStorage.getItem('levelStatus') !== null) {
     Statuslevel = JSON.parse(localStorage.getItem('levelStatus')!!)
 }
 if (levelarray.length > 0) {
    for (let i = 0; i < levelarray.length; i++) {
        var employeeArray: Array<EmployeeCardModel>
        employeeArray = levelarray[i]
         for (let j = 0; j < employeeArray.length; j++) {
             if (singleSelectionData.emp_id === employeeArray[j].emp_id){
                employeeArray[j].isSelected = selected
             }
         } 
         var CheckTrue = employeeArray.length > 0 ? employeeArray.filter(data => data.isSelected == true) : [];
         var emptyArray: Array<Boolean>
         emptyArray = Statuslevel
         if (CheckTrue.length === employeeArray.length){   
            emptyArray[i] = true
         } else{
            emptyArray[i] = false
         }
     }
     localStorage.setItem("levelarray", JSON.stringify(levelarray));
     localStorage.setItem("levelStatus", JSON.stringify(Statuslevel));
 } 
}