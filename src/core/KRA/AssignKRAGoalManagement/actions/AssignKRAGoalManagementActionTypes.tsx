/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import {
    IManageFailAction,
    IManageSuccessAction,
    ISelectAll,
    IGetAllGoals,
    IGoalAssigned,
    ISelectAllEmployee,
    ISelectIndividualEmployee,
    IHideLoading,
    IShowLoading,
} from "./AssignKRAGoalManagementActionInterface";


type AssignKRAGoalManagementTypes = 
|IManageSuccessAction
|IManageFailAction
|ISelectAll
|IGetAllGoals
|ISelectAllEmployee
|ISelectIndividualEmployee
|IGoalAssigned
|IShowLoading
|IHideLoading


export default AssignKRAGoalManagementTypes;