/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */
export default interface AssignKRAGoalManagementState {
    readonly profileValue: any,
    readonly value: any,
    readonly isEmpty: Boolean,
    readonly selectAll:Boolean,
    readonly goalsArray:any,
    readonly isAssigned: Boolean,
    readonly allEmployeesSelected : Boolean,
    readonly levelStatus : any,
    readonly viewGoals:Boolean
    readonly showLoading:Boolean,
    readonly isSuccess:Boolean,
    readonly successMessage:any,
}