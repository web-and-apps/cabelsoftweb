/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import AssignKRAGoalManagementState from '../state/AssignKRAGoalManagementState';
import AssignKRAGoalManagementActionTypes from '../actions/AssignKRAGoalManagementActionTypes';
import AssignKRAGoalManagementActionTypeKeys from '../actions/AssignKRAGoalManagementActionTypeKeys';

const initialState: AssignKRAGoalManagementState = {
    profileValue: [],
    value: false,
    isEmpty: false,
    selectAll:false,
    goalsArray:[],
    isAssigned: false,
    allEmployeesSelected:false,
    levelStatus:[],
    viewGoals:false,
    showLoading:false,
    isSuccess: false,
    successMessage: "",
};

const AssignKRAGoalManagementReducers: Reducer<AssignKRAGoalManagementState, AssignKRAGoalManagementActionTypes> = (state = initialState, action) => {
    switch (action.type) {
    case AssignKRAGoalManagementActionTypeKeys.MANAGE_PROFILE_SUCCESS: {
        return {
            ...state,
            profileValue: action.value,
            isEmpty: action.isEmpty,
            levelStatus:action.levelStatus,
        };
    }
    case AssignKRAGoalManagementActionTypeKeys.SELECTALL: {
        return {
            ...state,
            isEmpty: action.value,
            goalsArray: action.goalValue

        };
    }
    case AssignKRAGoalManagementActionTypeKeys.SELECTINDIVIDUALEMPLOYEE: {
        return {
            ...state,
            profileValue: action.profileValue,
            isEmpty: action.isEmpty,
            levelStatus:action.levelStatus

        };
    }
    case AssignKRAGoalManagementActionTypeKeys.SELECTALLEMPLOYEE: {
        return {
            ...state,
            allEmployeesSelected: action.value,
            profileValue: action.profileValue,
            levelStatus:action.levelStatus

        };
    }
    case AssignKRAGoalManagementActionTypeKeys.GETALLGOALS: {
        return {
            ...state,
            goalsArray: action.goalValue,
            viewGoals:action.isEmpty,
        };
    }
    case AssignKRAGoalManagementActionTypeKeys.GOAL_ASSIGNED: {
        return {
            ...state,
            isAssigned: action.isAssigned,
            isSuccess: action.isSuccess,
            successMessage: action.successMessage
        };
    }
    case AssignKRAGoalManagementActionTypeKeys.SHOWLOADING: {
        return {
            ...state,
            showLoading: action.showLoading
        };
    }
    case AssignKRAGoalManagementActionTypeKeys.HIDELOADING: {
        return {
            ...state,
            showLoading: action.showLoading
        };
    }
    default:
        return state;

}};

export default AssignKRAGoalManagementReducers;