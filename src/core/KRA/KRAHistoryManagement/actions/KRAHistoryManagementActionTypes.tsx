/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */

import {
    IHideAcion,
    IShowAction,
    IGetHistory,
    IManageSuccessAction,
    ITabChange,
    IShowLoading,
    IHideLoading
} from "./KRAHistoryManagementActionInterface";


type KRAHistoryManagementTypes =
    |IHideAcion
    |IManageSuccessAction
    |IGetHistory
    |ITabChange
    |IShowAction
    |IShowLoading
    |IHideLoading;

export default KRAHistoryManagementTypes;