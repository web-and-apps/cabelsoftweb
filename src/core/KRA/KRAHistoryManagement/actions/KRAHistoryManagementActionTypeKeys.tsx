/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */


export enum KRAHistoryManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum KRAHistoryManagementActionTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    HIDEPOPUP = "HIDE_POPUP",
    GETALLGOALS = "GET_ALL_GOALS",
    GETHISTORY = 'GET_HISTORY',
    MANAGE_PROFILE_SUCCESS = "MANAGE_PROFILE_SUCCESS",
    TAB_CHANGE = "TAB_CHANGE",
    SHOWLOADING = "SHOWLOADING",
    HIDELOADING = "HIDELOADING",
}

export default KRAHistoryManagementActionTypeKeys;