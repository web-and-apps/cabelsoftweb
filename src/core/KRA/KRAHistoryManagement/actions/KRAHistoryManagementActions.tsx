/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import KRAHistoryManagementState from '../state/KRAHistoryManagementState';
import KRAHistoryManagementActionTypes from './KRAHistoryManagementActionTypes';
import KRAHistoryManagementActionTypeKeys from './KRAHistoryManagementActionTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';

export const showPopup = (): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.SHOW_POPUP,
        showPopup: true,
        text: "Success"
    };
}
export const tabChange = (data: any): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.TAB_CHANGE,
        value: data
    };
}
export const getAllUserProfile = (data: any): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.MANAGE_PROFILE_SUCCESS,
        value: data,
        isEmpty: data.isEmpty
    }
}
export const getHistory = (data: any, current: any): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.GETHISTORY,
        value: data,
        current: current,
        showHistory:true,
        showLoading: false,
    }
}
export const hide = (): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.SHOW_POPUP,
        showPopup: false,
        text: "Success"
    };
}

// export const hidePopup = (error: Error): KRAHistoryManagementActionTypes => {
//     return {
//         type: KRAHistoryManagementActionTypeKeys.HIDEPOPUP,
//         payload: { error }
//     };
// }
export const hidePopup = (error: Error): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.HIDEPOPUP
    };
}
export const showLoader = (): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.SHOWLOADING,
        showLoading: true,
    };
}
export const hideLoader = (): KRAHistoryManagementActionTypes => {
    return {
        type: KRAHistoryManagementActionTypeKeys.HIDELOADING,
        showLoading: false,
    };
}

export const actionShowPopup: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopup());
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
export const hidePopUp: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(hide());
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}

export const getEmployeeWithLevel: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.getAppraisee().then((response) => {
            console.log(response);
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/History/Request/appraisee_generated1').UserData
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.levels.getRootAslevels(byteBuffer);
            console.log("user", user)
            // var listLength = user.level1();
            var listLength1 = user.level1Length();
            var listLength2 = user.level2Length();
            var listLength3 = user.level3Length();
            var listLength4 = user.level4Length();
            var listLength5 = user.level5Length();
            // console.log("userobj", listLength);
            console.log("userobj", listLength2);
            // var arr = {
            //     level1: [],
            //     level2: {},
            //     level3: {},
            //     level4: {},
            //     level5: {},
            // };
            var level1 = [];
            var level2 = [];
            var level3 = [];
            var level4 = [];
            var level5 = [];
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            if (listLength1 > 0) {
                for (let i = 0; i < listLength1; i++) {
                    let obj: any = {}
                    obj.id = user.level1(i).id().toString();
                    obj.title = user.level1(i).title();
                    obj.first_name = user.level1(i).firstName();
                    obj.last_name = user.level1(i).lastName();
                    obj.emp_id = user.level1(i).empId();
                    obj.emp_photo_url = user.level1(i).empPhotoUrl();
                    obj.email_id = user.level1(i).emailId();
                    obj.status = user.level1(i).status();
                    obj.appraiser_id = user.level1(i).appraiserId();
                    obj.created_user_id = user.level1(i).createdUserId().toString();
                    obj.updated_user_id = user.level1(i).updatedUserId().toString();
                    obj.role_id = user.level1(i).roleId();
                    obj.isSelected = false;
                    level1.push(obj);
                }
            }
            if (listLength2 > 0) {
                for (let i = 0; i < listLength2; i++) {
                    let obj: any = {}
                    obj.id = user.level2(i).id().toString();
                    obj.title = user.level2(i).title();
                    obj.first_name = user.level2(i).firstName();
                    obj.last_name = user.level2(i).lastName();
                    obj.emp_id = user.level2(i).empId();
                    obj.emp_photo_url = user.level2(i).empPhotoUrl();
                    obj.email_id = user.level2(i).emailId();
                    obj.status = user.level2(i).status();
                    obj.appraiser_id = user.level2(i).appraiserId();
                    obj.created_user_id = user.level2(i).createdUserId().toString();
                    obj.updated_user_id = user.level2(i).updatedUserId().toString();
                    obj.role_id = user.level2(i).roleId();
                    obj.isSelected = false;
                    level2.push(obj);
                }
            }
            if (listLength3 > 0) {
                for (let i = 0; i < listLength3; i++) {
                    let obj: any = {}
                    obj.id = user.level3(i).id().toString();
                    obj.title = user.level3(i).title();
                    obj.first_name = user.level3(i).firstName();
                    obj.last_name = user.level3(i).lastName();
                    obj.emp_id = user.level3(i).empId();
                    obj.emp_photo_url = user.level3(i).empPhotoUrl();
                    obj.email_id = user.level3(i).emailId();
                    obj.status = user.level3(i).status();
                    obj.appraiser_id = user.level3(i).appraiserId();
                    obj.created_user_id = user.level3(i).createdUserId().toString();
                    obj.updated_user_id = user.level3(i).updatedUserId().toString();
                    obj.role_id = user.level3(i).roleId();
                    obj.isSelected = false;
                    level3.push(obj);
                }
            }
            if (listLength4 > 0) {
                for (let i = 0; i < listLength4; i++) {
                    let obj: any = {}
                    obj.id = user.level4(i).id().toString();
                    obj.title = user.level4(i).title();
                    obj.first_name = user.level4(i).firstName();
                    obj.last_name = user.level4(i).lastName();
                    obj.emp_id = user.level4(i).empId();
                    obj.emp_photo_url = user.level4(i).empPhotoUrl();
                    obj.email_id = user.level4(i).emailId();
                    obj.status = user.level4(i).status();
                    obj.appraiser_id = user.level4(i).appraiserId();
                    obj.created_user_id = user.level4(i).createdUserId().toString();
                    obj.updated_user_id = user.level4(i).updatedUserId().toString();
                    obj.role_id = user.level4(i).roleId();
                    obj.isSelected = false;
                    level4.push(obj);
                }
            }
            if (listLength5 > 0) {
                for (let i = 0; i < listLength5; i++) {
                    let obj: any = {}
                    obj.id = user.level5(i).id().toString();
                    obj.title = user.level5(i).title();
                    obj.first_name = user.level5(i).firstName();
                    obj.last_name = user.level5(i).lastName();
                    obj.emp_id = user.level5(i).empId();
                    obj.emp_photo_url = user.level5(i).empPhotoUrl();
                    obj.email_id = user.level5(i).emailId();
                    obj.status = user.level5(i).status();
                    obj.appraiser_id = user.level5(i).appraiserId();
                    obj.created_user_id = user.level5(i).createdUserId().toString();
                    obj.updated_user_id = user.level5(i).updatedUserId().toString();
                    obj.role_id = user.level5(i).roleId();
                    obj.isSelected = false;
                    level5.push(obj);
                }
            }
            const finalArray = {
                level1: level1,
                level2: level2,
                level3: level3,
                level4: level4,
                level5: level5,
            }
            // console.log("obj", obj)
            console.log("arr", finalArray)
            dispatch(getAllUserProfile(finalArray));
        }).catch((error) => {
            // dispatch();
        });
    }
}
export const getEmployeeKRAHistory: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = (data) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.getKRAUserHistory(data).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/History/Request/KraHistory_generated').kraUserHistory
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var history = userData.kraHistory.getRootAskraHistory(byteBuffer);
            var historyLength = history.historyListLength();
            var currentLength = history.currentLength();
            var current = []
            let arr = []
            if (historyLength) {
                for (let i = 0; i < historyLength; i++) {
                    const obj: any = {};
                    obj.id = history.historyList(i).id().toFloat64();
                    obj.userId = history.historyList(i).userId().toFloat64();
                    obj.startDate = history.historyList(i).startDate();
                    obj.endDate = history.historyList(i).endDate();
                    obj.name = history.historyList(i).appraiserName();
                    obj.appraisalYear = history.historyList(i).appraisalYear();
                    obj.appraiserId = history.historyList(i).appraiserId();
                    obj.updatedUserId = history.historyList(i).updatedUserId().toFloat64();
                    obj.createdUserId = history.historyList(i).createdUserId().toFloat64();
                    arr.push(obj);
                }
            }
            if (currentLength) {
                for (let i = 0; i < currentLength; i++) {
                    const currentobj: any = {};
                    currentobj.id = history.current(i).id().toFloat64();
                    currentobj.userId = history.current(i).userId().toFloat64();
                    currentobj.startDate = history.current(i).startDate();
                    currentobj.endDate = history.current(i).endDate();
                    currentobj.name = history.current(i).appraiserName();
                    currentobj.appraisalYear = history.current(i).appraisalYear();
                    currentobj.updatedUserId = history.current(i).updatedUserId().toFloat64();
                    currentobj.createdUserId = history.current(i).createdUserId().toFloat64();
                    currentobj.status = history.current(i).status();
                    currentobj.appraiserId = history.current(i).appraiserId();
                    currentobj.appraiserName = history.current(i).appraiserName();
                    current.push(currentobj)
                }
            }
            console.log('current',current)
            console.log('arr',arr)
            dispatch(getHistory(arr, current));
        }).catch((error) => {
            if(error && error.response && error.response.status === 404) {
                window.alert("KRA not found")
            } else {
                window.alert("Internal server error")
            }
            dispatch(hideLoader());
            // dispatch(showPopupSuccess(error));
        });
    }
}

export const switchTab: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = (data) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(tabChange(data));
            dispatch(hideLoader())
        }).catch((error) => {
            dispatch(hideLoader())
            // dispatch(showPopupSuccess(error));
        });
    }
}
export const getUserProfile: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        // console.log(service.get());
        return service.get().then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
            }
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (var j = 0; j < levelLength; j++) {
                    let obj: any = {}
                    obj.id = user.userlevel(j).id().toFloat64();
                    obj.title = user.userlevel(j).title();
                    obj.first_name = user.userlevel(j).firstName();
                    obj.last_name = user.userlevel(j).lastName();
                    obj.emp_id = user.userlevel(j).empId();
                    obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
                    obj.status = user.userlevel(j).status();
                    obj.email_id = user.userlevel(j).emailId();
                    obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
                    obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
                    obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
                    obj.role_id = user.userlevel(j).roleId();
                    obj.empLevel = user.userlevel(j).empLevel();
                    obj.kraId = user.userlevel(j).kraId();
                    obj.kraStartDate = user.userlevel(j).kraStartDate();
                    obj.kraEndDate = user.userlevel(j).kraEndDate();
                    obj.isSelected = false;
                    if (levels[i] === obj.empLevel) {
                        dataarr.push(obj);
                    }
                }
                levelarray.push(dataarr)
            }
            dispatch(getAllUserProfile(levelarray));
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchCharacters: ActionCreator<
    ThunkAction<
        Promise<any>,
        KRAHistoryManagementState,
        null,
        KRAHistoryManagementActionTypes
    >
> = (term: String, filterType: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        return service.search(term, filterType).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
            var levelLength = user.userlevelLength();
            var levelsLength = user.levelsLength();
            var levelarray = [];
            var levels = [];
            for (let i = 0; i < levelsLength; i++) {
                levels.push(user.levels(i))
            }
            // var obj = {
            //     "id": 0,
            //     "title": "",
            //     "first_name": "",
            //     "last_name": "",
            //     "emp_id": "",
            //     "emp_photo_url": "",
            //     "status": 0,
            //     "isSelected": false,
            //     "email_id": "",
            //     "appraiser_id": "",
            //     "created_user_id": "",
            //     "updated_user_id": "",
            //     "role_id": 0
            // }
            for (let i = 0; i < levelsLength; i++) {
                var dataarr = [];
                for (var j = 0; j < levelLength; j++) {
                    let obj: any = {}
                    obj.id = user.userlevel(j).id().toFloat64();
                    obj.title = user.userlevel(j).title();
                    obj.first_name = user.userlevel(j).firstName();
                    obj.last_name = user.userlevel(j).lastName();
                    obj.emp_id = user.userlevel(j).empId();
                    obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
                    obj.status = user.userlevel(j).status();
                    obj.email_id = user.userlevel(j).emailId();
                    obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
                    obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
                    obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
                    obj.role_id = user.userlevel(j).roleId();
                    obj.empLevel = user.userlevel(j).empLevel();
                    obj.kraId = user.userlevel(j).kraId();
                    obj.kraStartDate = user.userlevel(j).kraStartDate();
                    obj.kraEndDate = user.userlevel(j).kraEndDate();
                    obj.isSelected = false;
                    if (levels[i] === obj.empLevel) {
                        dataarr.push(obj);
                    }
                }
                levelarray.push(dataarr)
            }
            dispatch(getAllUserProfile(levelarray));
            dispatch(hideLoader());
        }).catch((error) => {
            dispatch(getAllUserProfile([]));
            dispatch(hideLoader());
        });
    };
};