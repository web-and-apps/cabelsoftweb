/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */

import keys from "./KRAHistoryManagementActionTypeKeys";

export interface IShowAction {
    readonly type: keys.SHOW_POPUP,
    showPopup: Boolean,
    text: any
}
export interface IManageSuccessAction {
    readonly type: keys.MANAGE_PROFILE_SUCCESS;
    readonly value: any;
    readonly isEmpty: Boolean;
}

export interface IGetHistory {
    readonly type: keys.GETHISTORY;
    readonly value: any;
    readonly current: any;
    readonly showHistory:Boolean;
    readonly showLoading:Boolean;
}
export interface ITabChange {
    readonly type: keys.TAB_CHANGE;
    readonly value: any;
}


export interface IHideAcion {
    readonly type: keys.HIDEPOPUP;
}
export interface IShowLoading {
    readonly type: keys.SHOWLOADING;
    readonly showLoading: Boolean;
}
export interface IHideLoading {
    readonly type: keys.HIDELOADING;
    readonly showLoading: Boolean;
}

// export interface ISignInFailAction {
//     readonly type: keys.SIGNIN_FAIL;
//     readonly payload: {
//         readonly error: Error;
//     };
// }