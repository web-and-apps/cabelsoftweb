/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */
export default interface KRAHistoryManagementState {
    readonly profileValue: any,
    readonly value: any,
    readonly isEmpty: any,
    readonly history:any,
    readonly current:any,
    readonly tabChange:any,
    readonly showHistory:Boolean,
    readonly showLoading:Boolean,
}