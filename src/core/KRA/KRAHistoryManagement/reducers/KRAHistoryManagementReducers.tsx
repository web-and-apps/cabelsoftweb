/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthi_dev)
 */

import { Reducer } from 'redux';
import KRAHistoryManagementState from '../state/KRAHistoryManagementState';
import KRAHistoryManagementActionTypes from '../actions/KRAHistoryManagementActionTypes';
import KRAHistoryManagementActionTypeKeys from '../actions/KRAHistoryManagementActionTypeKeys';

const initialState: KRAHistoryManagementState = {
    profileValue: [],
    value: false,
    isEmpty: "dfdfd",
    history:[],
    current:[],
    tabChange: "History",
    showHistory:false,
    showLoading:false,
};

const KRAHistoryManagementReducers: Reducer<KRAHistoryManagementState, KRAHistoryManagementActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case KRAHistoryManagementActionTypeKeys.MANAGE_PROFILE_SUCCESS: {
            return {
                ...state,
                profileValue: action.value,
                isEmpty: action.isEmpty,
            };
        }
        case KRAHistoryManagementActionTypeKeys.GETHISTORY: {
            return {
                ...state,
                history: action.value,
                current: action.current,
                showHistory: action.showHistory,
                showLoading: action.showLoading,
            };
        }
        case KRAHistoryManagementActionTypeKeys.TAB_CHANGE: {
            return {
                ...state,
                tabChange: action.value
            };
        }
        case KRAHistoryManagementActionTypeKeys.SHOWLOADING: {
            return {
                ...state,
                showLoading: action.showLoading
            };
        }
        case KRAHistoryManagementActionTypeKeys.HIDELOADING: {
            return {
                ...state,
                showLoading: action.showLoading
            };
        }
        default:
            return state;
    }};

export default KRAHistoryManagementReducers;