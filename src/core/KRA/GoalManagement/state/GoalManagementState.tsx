/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */
import { KRAGoalModels } from "../../../../components/KRAGoal/KRAGoalComponents";
export default interface GoalManagementState {
    goalDetail: KRAGoalModels[],
    text: string;
    showPopup: Boolean;
    goalAdded:Boolean;
    isEmpty:Boolean;
    success:Boolean;
    tagList : any;
    showTagList:Boolean;
    showLoading: Boolean;
    clearSearch: Boolean;
    isSuccess: Boolean,
    successMessage: any,
}