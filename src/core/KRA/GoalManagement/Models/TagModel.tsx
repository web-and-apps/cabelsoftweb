/**
 * Tag Model
 * @version 1.0.0
 * @author [Parthiban](http://192.168.1.28:8080/parthiban) 
 */

 export class TagModel{
     id : number
     tag : string

     constructor(tagdata : any){
        this.id = tagdata.id().toFloat64()
        this.tag = tagdata.tag()
     }

 }