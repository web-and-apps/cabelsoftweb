/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import GoalManagementState from '../state/GoalManagementState';
import GoalManagementActionTypes from './GoalManagementActionTypes';
import GoalManagementActionTypeKeys from './GoalManagementActionTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';
import { TagModel } from '../Models/TagModel';

export const showPopup = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.SHOW_POPUP,
        showPopup: true,
        text: "Success"
    };
}
export const getAllTags = (data : any,showList:Boolean): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.GETALLTAGES,
        tagList : data,
        showList:showList
    };
}
export const getAllGoals = (data: any): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.GETALLGOALS,
        value: data,
        isEmpty: true
    }
}
export const hide = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.SHOW_POPUP,
        showPopup: false,
        text: "Success"
    };
}
export const goalAdded = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.GOALADDED,
        showPopup: false,
        goalAdded:true
    };
}
export const resStatus = (status:Boolean): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.SUCCESS,
        success: status,
    };
}
export const newGoalAdded = (data: any, value?: any): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.NEW_GOAL_ADDED,
        showPopup: false,
        goalAdded: true,
        value: data,
        showLoading: false,
        isSuccess: true,
        successMessage: value
    };
}
// export const hidePopup = (error: Error): GoalManagementActionTypes => {
//     return {
//         type: GoalManagementActionTypeKeys.HIDEPOPUP,
//         payload: { error }
//     };
// }
export const hidePopup = (error: Error): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.HIDEPOPUP
    };
}
export const showLoader = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.SHOWLOADING,
        showLoading: true,
    }
  }
export const hideLoader = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.HIDELOADING,
        showLoading: false,
    }
  }
export const hideToasterWindow = (): GoalManagementActionTypes => {
    return {
        type: GoalManagementActionTypeKeys.HIDETOASTER,
        isSuccess: false,
    }
  }  
const getAllKraGoal = (): Promise<any> => {
    return new Promise((resolve, reject) => {
        const service = new APIUserManagement();
        return service.getGoals().then((response) => {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../schema/ResponseSchema/Users/goalresponse_generated').GoalDetails
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var goal = userData.goalresponse.getRootAsgoalresponse(byteBuffer);
                var goalLength = goal.goalListLength();
                var arr = [];
                var obj = {
                    "id": "",
                    "goal_type_id": "",
                    "description": "",
                    "created_user_id": "",
                    "updated_user_id": "",
                    "created_date": "",
                    "update_date": "",
                    "created_user_name": "",
                    "created_user_image": "",
                    "tag" : ""
                }
                for (var i = 0; i < goalLength; i++) {
                    let obj: any = {}
                    obj.id = goal.goalList(i).id() ? goal.goalList(i).id() : '';
                    obj.goal_type_id = goal.goalList(i).goalTypeId() ? goal.goalList(i).goalTypeId() : '';
                    obj.description = goal.goalList(i).description() ? goal.goalList(i).description() : '';
                    obj.created_user_id = goal.goalList(i).createdUserId() ? goal.goalList(i).createdUserId() : '';
                    obj.updated_user_id = goal.goalList(i).updatedUserId() ? goal.goalList(i).updatedUserId() : '';
                    obj.created_date = goal.goalList(i).createdDate() ? goal.goalList(i).createdDate() : '';
                    obj.update_date = goal.goalList(i).updateDate() ? goal.goalList(i).updateDate() : '';
                    obj.created_user_name = goal.goalList(i).createdUserName() ? goal.goalList(i).createdUserName() : '';
                    obj.created_user_image = goal.goalList(i).createdUserImage() ? goal.goalList(i).createdUserImage() : '';
                    obj.isSelected = false;                 
                    var tagLength = goal.goalList(i).tagLength();
                    var tags_name : string = ""
                     for (var j = 0; j < tagLength; j++) {
                        var new_str =( goal.goalList(i).tag(j));
                        tags_name =  tags_name.concat( new_str).concat(",");
                       }
                     obj.tag = tags_name !== "" ? tags_name.substring(0,tags_name.length-1) : "" ;
                    arr.push(obj);
                    console.log(obj);
                }
                console.log("obj", obj)
                console.log("arr", typeof (arr));
                resolve(arr);
            }
        }).catch((error) => {
            console.log(error)
         //   resolve(arr);
            reject(error);
        }); 
    })
}
export const actionShowPopup: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopup());
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
export const hidePopUp: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(hide());
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
export const hideTagList: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (hide:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(getAllTags('',!hide));
        }).catch((error) => {
            dispatch(getAllTags('',!hide));
        });
    }
}
export const addGoals: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        console.log("add goal DAta", param)
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../../../schema/GoalsModuleSchema/AddGoal/addGoal_generated').addGoal;
        var addGoal = goaldetails.goal
        var fbb = new flatbuffers.Builder(0);
        console.log("fbb", param);
        var id = fbb.createLong(param.goal_type_id);
      //  var goalTypeId = fbb.createInt(param.goal_type_id);
        var description = fbb.createString(param.description);
        var createdUserId = fbb.createLong(param.created_user_id);
        var addUpdatedUserId = fbb.createLong(param.updated_user_id);
        var createdDate = fbb.createString("ffghfg");
        var updatedDate = fbb.createString("fghfgh");
        let tagList;
        if(param.tagName && param.tagName.includes(',')){
            var tempTagValue = param.tagName.split(',');
            const tagFbbValue = []
            for(let i =0 ; i < tempTagValue.length; i++){
                var tagValue = fbb.createString(tempTagValue[i])
                tagFbbValue.push(tagValue)
            }
            tagList = addGoal.createTagVector(fbb,tagFbbValue)
        }
        addGoal.startgoal(fbb)
        addGoal.addId(fbb,id)
        addGoal.addGoalTypeId(fbb,param.goal_type_id)
        addGoal.addDescription(fbb,description)
        addGoal.addCreatedUserId(fbb,createdUserId)
        addGoal.addUpdatedUserId(fbb,addUpdatedUserId)
        addGoal.addCreatedDate(fbb,createdDate)
        addGoal.addUpdatedDate(fbb,updatedDate)
        addGoal.addTag(fbb,tagList)
        var ints = addGoal.endgoal(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        console.log("sundar", value);
        return service.addGoals(value).then((response) => {
            console.log("add Goals", response)
            if(Response){
                let obj: any = {}
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/createresponse_generated').responseStatus
                var responseArray = new Uint8Array(response.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.createresponse.getRootAscreateresponse(byteBuffer);
                obj.id = responseStatus.id();
                obj.message = responseStatus.message();
                getAllKraGoal().then((response) => {
                    console.log('newAddedValue', response)
                    dispatch(newGoalAdded(response, "Goal added Successfully"))
                }).catch((error) =>  {
                    dispatch(hideLoader())
                    console.log(error)
                })
                // dispatch(hideLoader())
            }
            // dispatch(goalAdded());
        }).catch((error) => {
            console.log("add Goals error", error)
            dispatch(hideLoader())
            dispatch(hidePopup(error));
        });
    }
}
export const updateGoal: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        console.log("add goal DAta", param)
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../../../schema/GoalsModuleSchema/AddGoal/addGoal_generated').addGoal;
        var addGoal = goaldetails.goal
        var fbb = new flatbuffers.Builder(0);
        console.log("fbb", param);
        var id = fbb.createLong(param.id);
      //  var goalTypeId = fbb.createInt(param.goal_type_id);
        var description = fbb.createString(param.description);
        var createdUserId = fbb.createLong(param.created_user_id);
        var addUpdatedUserId = fbb.createLong(param.updated_user_id);
        var createdDate = fbb.createString("ffghfg");
        var updatedDate = fbb.createString("fghfgh");
        let tagList;
        if(param.tagName && param.tagName.includes(',')){
            var tempTagValue = param.tagName.split(',');
            const tagFbbValue = []
            for(let i =0 ; i < tempTagValue.length; i++){
                var tagValue = fbb.createString(tempTagValue[i])
                tagFbbValue.push(tagValue)
            }
            tagList = addGoal.createTagVector(fbb,tagFbbValue)
        }
        addGoal.startgoal(fbb)
        addGoal.addId(fbb,id)
        addGoal.addGoalTypeId(fbb,param.goal_type_id)
        addGoal.addDescription(fbb,description)
        addGoal.addCreatedUserId(fbb,createdUserId)
        addGoal.addUpdatedUserId(fbb,addUpdatedUserId)
        addGoal.addCreatedDate(fbb,createdDate)
        addGoal.addUpdatedDate(fbb,updatedDate)
        addGoal.addTag(fbb,tagList)
        var ints = addGoal.endgoal(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        console.log("sundar", value);

        return service.updateGoal(value, param.id).then((response) => {
            // dispatch(goalAdded());
            if(Response){
                let obj: any = {}
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/createresponse_generated').responseStatus
                var responseArray = new Uint8Array(response.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.createresponse.getRootAscreateresponse(byteBuffer);
                obj.id = responseStatus.id();
                obj.message = responseStatus.message();
                getAllKraGoal().then((response) => {
                    console.log('newAddedValue', response)
                    dispatch(newGoalAdded(response, "Goal updated successfully"))
                }).catch((error) =>  {
                    console.log(error)
                    dispatch(hideLoader());
                })
                // dispatch(hideLoader());
            }
        }).catch((error) => {
            dispatch(hideLoader());
            dispatch(hidePopup(error));
        });
    }
}
export const deleteGoal: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (param: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        return service.deleteGoal(param).then((response) => {
            getAllKraGoal().then((responses) => {
                console.log('newAddedValue', responses)
                dispatch(newGoalAdded(responses, "Goal deleted successfully"))
                dispatch(hideLoader())
            }).catch((error) =>  {
                console.log(error)
            })
            // if (response) {
            //     var flatbuffers = require('flatbuffers').flatbuffers
            //     var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
            //     // var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
            //     var responseArray = new Uint8Array(response.data);
            //     var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            //     var responseStatus = userData.responsestat.getRootAsresponsestat(byteBuffer);
            //     var responseSuccess = responseStatus.status
            //     getAllKraGoal().then((responses) => {
            //         console.log('newAddedValue', responses)
            //         dispatch(newGoalAdded(responses))
            //     }).catch((error) =>  {
            //         console.log(error)
            //     })
            // }
        }).catch((error) => {
            dispatch(hideLoader())
            dispatch(resStatus(false));
        });
    }
}

export const getGoals: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader())
        const service = new APIUserManagement();
        return service.getGoals().then((response) => {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../schema/ResponseSchema/Users/goalresponse_generated').GoalDetails
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var goal = userData.goalresponse.getRootAsgoalresponse(byteBuffer);
                var goalLength = goal.goalListLength();
                var arr = [];
                var obj = {
                    "id": "",
                    "goal_type_id": "",
                    "description": "",
                    "created_user_id": "",
                    "updated_user_id": "",
                    "created_date": "",
                    "update_date": "",
                    "created_user_name": "",
                    "created_user_image": "",
                    "tag" : ""
                }
                for (var i = 0; i < goalLength; i++) {
                    let obj: any = {}
                    obj.id = goal.goalList(i).id() ? goal.goalList(i).id() : '';
                    obj.goal_type_id = goal.goalList(i).goalTypeId() ? goal.goalList(i).goalTypeId() : '';
                    obj.description = goal.goalList(i).description() ? goal.goalList(i).description() : '';
                    obj.created_user_id = goal.goalList(i).createdUserId() ? goal.goalList(i).createdUserId() : '';
                    obj.updated_user_id = goal.goalList(i).updatedUserId() ? goal.goalList(i).updatedUserId() : '';
                    obj.created_date = goal.goalList(i).createdDate() ? goal.goalList(i).createdDate() : '';
                    obj.update_date = goal.goalList(i).updateDate() ? goal.goalList(i).updateDate() : '';
                    obj.created_user_name = goal.goalList(i).createdUserName() ? goal.goalList(i).createdUserName() : '';
                    obj.created_user_image = goal.goalList(i).createdUserImage() ? goal.goalList(i).createdUserImage() : '';
                    obj.isSelected = false;                 
                    var tagLength = goal.goalList(i).tagLength();
                    var tags_name : string = ""
                     for (var j = 0; j < tagLength; j++) {
                        var new_str =( goal.goalList(i).tag(j));
                        tags_name =  tags_name.concat( new_str).concat(",");
                       }
                     obj.tag = tags_name !== "" ? tags_name.substring(0,tags_name.length-1) : "" ;
                    arr.push(obj);
                    console.log(obj);
                }
                console.log("obj", obj)
                console.log("arr", typeof (arr));
                dispatch(getAllGoals(arr));
                dispatch(hideLoader());
            }
        }).catch((error) => {
            console.log(error)
            dispatch(getAllGoals(error));
            dispatch(hideLoader())
        });
    }
}

// get all Tag list

export const getTags: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (searchtext : any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.getTags(searchtext).then((response) => {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var tagData = require('../../../schema/GoalsModuleSchema/Tag/tag_generated').tag
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var tag = tagData.tagres.getRootAstagres(byteBuffer);
                var tagLength = tag.tagListLength();
                var arr = [];               
                for (var i = 0; i < tagLength; i++) {
                    let obj = new TagModel(tag.tagList(i));
                    arr.push(obj);
                }
                console.log("arr", typeof (arr));
                dispatch(getAllTags(arr,true));
            }
        }).catch((error) => {
            console.log(error)      
            dispatch(getAllTags("",false));
        });
    }
}

// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchGoals: ActionCreator<
    ThunkAction<
    Promise<any>,
    GoalManagementState,
    null,
    GoalManagementActionTypes
    >
> = (term: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        return service.searchGoal(term).then((response) =>  {
            if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../schema/ResponseSchema/Users/goalresponse_generated').GoalDetails
                var responseArray = new Uint8Array(response.data);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var goal = userData.goalresponse.getRootAsgoalresponse(byteBuffer);
                var goalLength = goal.goalListLength();
                var arr = [];
                var obj = {
                    "id": "",
                    "goal_type_id": "",
                    "description": "",
                    "created_user_id": "",
                    "updated_user_id": "",
                    "created_date": "",
                    "update_date": "",
                    "created_user_name": "",
                    "created_user_image": "",
                    "tag" : ""
                }
                for (var i = 0; i < goalLength; i++) {
                    let obj: any = {}
                    obj.id = goal.goalList(i).id() ? goal.goalList(i).id() : '';
                    obj.goal_type_id = goal.goalList(i).goalTypeId() ? goal.goalList(i).goalTypeId() : '';
                    obj.description = goal.goalList(i).description() ? goal.goalList(i).description() : '';
                    obj.created_user_id = goal.goalList(i).createdUserId() ? goal.goalList(i).createdUserId() : '';
                    obj.updated_user_id = goal.goalList(i).updatedUserId() ? goal.goalList(i).updatedUserId() : '';
                    obj.created_date = goal.goalList(i).createdDate() ? goal.goalList(i).createdDate() : '';
                    obj.update_date = goal.goalList(i).updateDate() ? goal.goalList(i).updateDate() : '';
                    obj.created_user_name = goal.goalList(i).createdUserName() ? goal.goalList(i).createdUserName() : '';
                    obj.created_user_image = goal.goalList(i).createdUserImage() ? goal.goalList(i).createdUserImage() : '';
                    obj.isSelected = false;                 
                    arr.push(obj);
                }
                console.log("obj", obj)
                console.log("arr", typeof (arr));
                dispatch(getAllGoals(arr));
                dispatch(hideLoader());
            }
        }).catch((error) => {
            dispatch(getAllGoals(error));
            dispatch(hideLoader());
        });
    };
};

export const hideToaster: ActionCreator<
    ThunkAction<
        Promise<any>,
        GoalManagementState,
        null,
        GoalManagementActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showInfoAlert().then((response) => {
          dispatch(hideToasterWindow());
        }).catch((error) => {
          dispatch(hideToasterWindow());
        });
      }
}


// export const actionSubmit: ActionCreator<
//     ThunkAction<
//         Promise<any>,
//         GoalManagementState,
//         null,
//         GoalManagementActionTypes
//     >
// > = () => {
//     return (dispatch: Dispatch) => {
//         const service = new APIUserManagement();
//         console.log(service.get());
//         var muser = {
//             "id":1,
//             "title":"Mr",
//             "first_name":"Sundar",
//             "last_name":"Essakimuthu",
//             "emp_id":"VT020",
//             "emp_photo_url":"https://www.bootdey.com/img/Content/avatar/avatar7.png",
//             "email_id":"sundar@vthink.co.in",
//             "appraiser_id":"VT001",
//             "created_user_id":""
//         }
//         var fbb =  new flatbuffers.Builder(0);
//         var title = fbb.createString(muser.title)
//         var fName = fbb.createString(muser.first_name)
//         var lName = fbb.createString(muser.last_name)
//         var empId = fbb.createString(muser.emp_id)
//         var photoUrl = fbb.createString(muser.emp_photo_url.toString())
//         var emailId = fbb.createString(muser.email_id)
//         var appraiseeId = fbb.createString(muser.appraiser_id.toString())
//         user.startuser(fbb)
//         user.addId(fbb, muser.id)
//         user.addTitle(fbb, title)
//         user.addFirstName(fbb, fName)
//         user.addLastName(fbb, lName)
//         user.addEmpId(fbb, empId)
//         user.addEmpPhotoUrl(fbb, photoUrl)
//         user.addStatus(fbb, muser.status.ordinal.toByte())
//         user.addEmailId(fbb, emailId)
//         user.addAppraiserId(fbb, appraiseeId)
//         user.addCreatedUserId(fbb, muser.created_user_id)
//         user.addUpdatedUserId(fbb, muser.updated_user_id)
//         user.addRoleId(fbb, muser.role_id.ordinal.toByte())
//         var ints = user.enduser(fbb)
//         fbb.finish(ints)
//         return service.get().then((response) => {
//             var flatbuffers = require('flatbuffers').flatbuffers
//             var userData = require('../schema/ResponseSchema/Users/fbsdata_generated').UsersData
//             var responseArray = new Uint8Array(response.data);
//             var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
//             var user = userData.users.getRootAsusers(byteBuffer);
//             console.log("user",user)
//             var userobj = user.usersList(0).firstName();
//             var listLength = user.usersListLength();
//             console.log("listLength",listLength)
//             dispatch(showPopup());
//         }).catch((error) => {
//             dispatch(hidePopup(error));
//         });
//     }
// }
