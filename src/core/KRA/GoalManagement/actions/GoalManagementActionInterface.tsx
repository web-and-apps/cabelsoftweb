/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import keys from "./GoalManagementActionTypeKeys";

export interface IShowAction {
    readonly type: keys.SHOW_POPUP,
    showPopup: Boolean,
    text: any
}
export interface IGetAllTags {
    readonly type: keys.GETALLTAGES,
    tagList: any,
    showList:Boolean,
}
export interface ISuccessResponse {
    readonly type: keys.SUCCESS,
    success: Boolean
}
export interface IGetAllGoals {
    readonly type: keys.GETALLGOALS;
    readonly value: any;
    readonly isEmpty: Boolean;
}

export interface IHideAcion {
    readonly type: keys.HIDEPOPUP;
}
export interface IGoalAdded{
    readonly type: keys.GOALADDED,
    showPopup: Boolean,
    goalAdded: Boolean
}
export interface IShowLoading{
    readonly type: keys.SHOWLOADING,
    showLoading: Boolean,
}
export interface IHideLoading{
    readonly type: keys.HIDELOADING,
    showLoading: Boolean,
}
export interface IHideToaster{
    readonly type: keys.HIDETOASTER,
    isSuccess: Boolean,
}
export interface INewGoalAdded{
    readonly type: keys.NEW_GOAL_ADDED,
    readonly showPopup: Boolean,
    readonly goalAdded: Boolean,
    readonly value: any;
    showLoading: Boolean;
    isSuccess: Boolean;
    successMessage: any;
}

// export interface ISignInFailAction {
//     readonly type: keys.SIGNIN_FAIL;
//     readonly payload: {
//         readonly error: Error;
//     };
// }