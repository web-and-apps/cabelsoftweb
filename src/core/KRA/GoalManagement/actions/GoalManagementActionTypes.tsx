/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import {
    IHideAcion,
    IShowAction,
    IGetAllGoals,
    IGoalAdded,
    ISuccessResponse,
    INewGoalAdded,
    IGetAllTags,
    IHideLoading,
    IShowLoading,
    IHideToaster,
} from "./GoalManagementActionInterface";


type GoalManagementActionTypes =
    | IHideAcion
    |IGetAllGoals
    |IGoalAdded
    |ISuccessResponse
    | IShowAction
    |INewGoalAdded
    |IGetAllTags
    |IShowLoading
    |IHideLoading
    |IHideToaster

export default GoalManagementActionTypes;