/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */


export enum GoalManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum GoalManagementActionTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    HIDEPOPUP = "HIDE_POPUP",
    GETALLGOALS = "GET_ALL_GOALS",
    GOALADDED = "GOAL_ADDED",
    SUCCESS = "SUCCESS",
    NEW_GOAL_ADDED = "NEW_GOAL_ADDED",
    GETALLTAGES = "GETALLTAGES",
    SHOWLOADING = "SHOWLOADING",
    HIDELOADING = "HIDELOADING",
    HIDETOASTER = "HIDETOASTER"
}

export default GoalManagementActionTypeKeys;