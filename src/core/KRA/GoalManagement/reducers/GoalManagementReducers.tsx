/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import GoalManagementState from '../state/GoalManagementState';
import GoalManagementActionTypes from '../actions/GoalManagementActionTypes';
import GoalManagementActionTypeKeys from '../actions/GoalManagementActionTypeKeys';

const initialState: GoalManagementState = {
    goalDetail:[],
    showPopup: false,
    text: "dfdfd",
    goalAdded: false,
    isEmpty:false,
    success:false,
    tagList :[],
    showTagList:false,
    showLoading: false,
    clearSearch:false,
    isSuccess: false,
    successMessage: "",
};

const GoalManagementReducers: Reducer<GoalManagementState, GoalManagementActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case GoalManagementActionTypeKeys.GETALLGOALS: {
            return {
                ...state,
                goalDetail: action.value,
                isEmpty: action.isEmpty,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.NEW_GOAL_ADDED: {
            return {
                ...state,
                goalDetail: action.value,
                showPopup: action.showPopup,
                goalAdded: action.goalAdded,
                showLoading: action.showLoading,
                clearSearch : true,
                isSuccess: action.isSuccess,
                successMessage: action.successMessage
            };
        }
        case GoalManagementActionTypeKeys.SHOW_POPUP: {
            return {
                ...state,
                showPopup: action.showPopup,
                text: action.text,
                showTagList: false,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.SUCCESS: {
            return {
                ...state,
                success: action.success,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.GOALADDED: {
            return {
                ...state,
                showPopup: action.showPopup,
                goalAdded: action.goalAdded,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.GETALLTAGES: {
            return {
                ...state,
                tagList: action.tagList,
                showTagList: action.showList
            };
        }
        case GoalManagementActionTypeKeys.SHOWLOADING: {
            return {
                ...state,
                showLoading: action.showLoading,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.HIDELOADING: {
            return {
                ...state,
                showLoading: action.showLoading,
                clearSearch:false
            };
        }
        case GoalManagementActionTypeKeys.HIDETOASTER: {
            return {
                ...state,
                isSuccess: action.isSuccess
            };
        }
        default:
            return state;
    }
};

export default GoalManagementReducers;