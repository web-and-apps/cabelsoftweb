/**
 * Employee Mangement State Informations
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */

export default interface AppraiserManagementState {
    readonly profileValue: any,
    readonly value: any,
    readonly isEmpty: Boolean,
    readonly history:any,
    readonly getEmpDetail: any,
    readonly tabName: any,
    readonly getCurrentKRAGoalsList: any,
    readonly userkra : any,
    readonly commentData:any;
    readonly showinfoalert : any;
    readonly kradetails : any;
    readonly showLoading : Boolean;
    readonly rating : any;
    readonly commentsValue : any;
    readonly fileData: any;
    readonly showinfoValue: any;
}