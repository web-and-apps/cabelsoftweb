/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */


export enum AppraiserManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum AppraiserManagementTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    HIDEPOPUP = "HIDE_POPUP",
    GETALLGOALS = "GET_ALL_GOALS",
    GETHISTORY = 'GET_HISTORY',
    MANAGE_PROFILE_SUCCESS = "MANAGE_PROFILE_SUCCESS",
    GET_EMP_DETAIL_SUCCESS = "GET_EMP_DETAIL_SUCCESS",
    GET_EMP_DETAIL_FAILURE = "GET_EMP_DETAIL_FAILURE",
    GETEMPCURRENTGOALS = "GET_EMP_CURRENT_GOALS",
    GETCOMMENTS = "GETCOMMENTS",
    SHOWINFOALERT = "SHOWINFOALERT",
    SHOWLOADING = "SHOWLOADING",
    REFRESHGOAL = "REFRESHGOAL",
    GETALLUSERCOMMENTS = "GETALLUSERCOMMENTS",
    FILEUPLOAD = "FILEUPLOAD",
}

export default AppraiserManagementTypeKeys;