/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import AppraiserManagementState from '../state/AppraiserManagementState';
import AppraiserManagementTypes from './AppraiserManagementTypes';
import AppraiserManagementTypeKeys from './AppraiserManagementTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';
export const getCurrentKRAGoalsSuccess = (data: any, kradetails : any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.GETEMPCURRENTGOALS,
    value: data,
    isEmpty: true,
    tabName: "Current",
    kradetails : kradetails,
    showLoading: false
  }
}
export const refreshCurrentKRAGoal = (data: any, value?: any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.REFRESHGOAL,
     value: data,
    isEmpty: true,
    tabName: "Current",
    showLoading: false,
    showinfoalert: true,
    showinfoValue: value,
  }
}
export const getEmpDetailSuccess = (data: any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.GET_EMP_DETAIL_SUCCESS,
    value: data,
    showLoading: false
  }
}
export const fileUploadData = (data: any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.FILEUPLOAD,
    value: data,
    showLoading: false
  }
}
export const getEmpDetailFailure = (error: Error): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.GET_EMP_DETAIL_FAILURE,
    showLoading: false,
    payload: { error }
  }
}
export const ShowinfoAlert = (data : any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.SHOWINFOALERT,
    showinfoalert: data,
    showPopup: false,
    showLoading: false,
  }
}
export const showPopup = (data : any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.SHOW_POPUP,
    showPopup: data,
    text: "Success",
    showLoading: false,
  };
}
export const getAllUserProfile = (data: any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.MANAGE_PROFILE_SUCCESS,
    value: data,
    isEmpty: false,
    tabName: "Appraisee KRA Details",
    showLoading: false,
  }
}
export const getAllUserProfileFailed = (error: Error): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.GET_EMP_DETAIL_FAILURE,
    payload: { error },
    showLoading: false,
  }
}
export const getHistory = (data: any): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.GETHISTORY,
    value: data,
    tabName: "History",
    isEmpty: true,
    showLoading: false,
  }
}
export const hide = (): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.HIDEPOPUP,
    showPopup: false,
    showLoading: false,
  };
}

// export const hidePopup = (error: Error): KRAHistoryManagementActionTypes => {
//     return {
//         type: KRAHistoryManagementActionTypeKeys.HIDEPOPUP,
//         payload: { error }
//     };
// }
export const hidePopup = (error: Error): AppraiserManagementTypes => {
  return {
    type: AppraiserManagementTypeKeys.HIDEPOPUP,
    showPopup: false,
    showLoading: false,
  };
}
export const getCommentdata = (data: any,filePath:any): AppraiserManagementTypes => {
  return {
      type: AppraiserManagementTypeKeys.GETCOMMENTS,
      showPopUp: true,
      commentsData:data,
      fileUploadData:filePath,
      showLoading: false,
  }
}
export const showLoader = (): AppraiserManagementTypes => {
  return {
      type: AppraiserManagementTypeKeys.SHOWLOADING,
      showLoading: true,
  }
}
export const getAllUserComments = (data: any,rating:any): AppraiserManagementTypes => {
  return {
      type: AppraiserManagementTypeKeys.GETALLUSERCOMMENTS,
      commentsValue: data,
      rating:rating,
      showLoading: false
  }
}
export const actionShowPopup: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data:Boolean) => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    return service.showPopup().then((response) => {
      dispatch(showPopup(!data));
    }).catch((error) => {
      dispatch(showPopup(data));
    });
  }
}
export const hidePopUp: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = () => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    return service.showPopup().then((response) => {
      dispatch(hide());
    }).catch((error) => {
      dispatch(hidePopup(error));
    });
  }
}

export const hideInfoAlert: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = () => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    return service.showInfoAlert().then((response) => {
      dispatch(ShowinfoAlert(false));
    }).catch((error) => {
      dispatch(ShowinfoAlert(false));
    });
  }
}

export const getUserProfile: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = () => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader());
    const service = new APIUserManagement();
    // console.log(service.get());
    return service.get().then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
      var levelLength = user.userlevelLength();
      var levelsLength = user.levelsLength();
      var levelarray = [];
      var levels = [];
      for (let i = 0; i < levelsLength; i++) {
        levels.push(user.levels(i))
      }
      // var obj = {
      //   "id": 0,
      //   "title": "",
      //   "first_name": "",
      //   "last_name": "",
      //   "emp_id": "",
      //   "emp_photo_url": "",
      //   "status": 0,
      //   "isSelected": false,
      //   "email_id": "",
      //   "appraiser_id": "",
      //   "created_user_id": "",
      //   "updated_user_id": "",
      //   "role_id": 0
      // }
      for (let i = 0; i < levelsLength; i++) {
        var dataarr = [];
        for (let j = 0; j < levelLength; j++) {
          let obj: any = {}
          obj.id = user.userlevel(j).id().toFloat64();
          obj.title = user.userlevel(j).title();
          obj.first_name = user.userlevel(j).firstName();
          obj.last_name = user.userlevel(j).lastName();
          obj.emp_id = user.userlevel(j).empId();
          obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
          obj.status = user.userlevel(j).status();
          obj.email_id = user.userlevel(j).emailId();
          obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
          obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
          obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
          obj.role_id = user.userlevel(j).roleId();
          obj.empLevel = user.userlevel(j).empLevel();
          obj.kraId = user.userlevel(j).kraId();
          obj.kraStartDate = user.userlevel(j).kraStartDate();
          obj.kraEndDate = user.userlevel(j).kraEndDate();
          obj.isSelected = false;
          if (levels[i] === obj.empLevel) {
            dataarr.push(obj);
          }
        }
        levelarray.push(dataarr)
      }
      dispatch(getAllUserProfile(levelarray));
    }).catch((error) => {
      dispatch(getAllUserProfileFailed(error));
    });
  }
}
export const getEmployeeKRAHistory: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data) => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader());
    const service = new APIUserManagement();
    return service.getKRAUserHistory(data).then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../schema/KraHistory_generated').kraUserHistory
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var history = userData.kraHistory.getRootAskraHistory(byteBuffer);
      var historyLength = history.historyListLength();

      let arr = []
      if (historyLength) {
        const obj: any = {};
        obj.id = history.historyList(0).id().toFloat64();
        obj.userId = history.historyList(0).userId().toFloat64();
        obj.startDate = history.historyList(0).startDate();
        obj.endDate = history.historyList(0).endDate();
        obj.name = history.historyList(0).appraiserName();
        obj.appraisalYear = history.historyList(0).appraisalYear();
        obj.updatedUserId = history.historyList(0).updatedUserId();
        obj.createdUserId = history.historyList(0).createdUserId();
        obj.appraiserId = history.historyList(0).appraiserId();
        console.log(obj);
        arr.push(obj);
      }
      console.log("get Histroy", arr)
      dispatch(getHistory(arr));
    }).catch((error) => {
      // dispatch(showPopupSuccess(error));
    });
  }
}

export const getEmployeeKRADetails: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data: any, type: string) => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader())
    const service = new APIUserManagement();
    return service.getKRASpecificUserHistory(data, type).then((response) => {
      console.log(response);
      var flatbuffers = require('flatbuffers').flatbuffers;
      var kraData = require('../schema/kraList_generated').kraList;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var kraValue = kraData.kraList.getRootAskraList(byteBuffer);
      var kraLength = kraValue.kraresLength();
      var feedBackLength = kraValue.feedbackListLength();
      var kraDetailsListLength = kraValue.kradetailsListLength();
      const kraValueArr = [];
      const feedBackValueArr = [];
      const kraDetailsListArr = [];
      var rating:any
      for (let i = 0; i <= kraLength - 1; i++) {
          let obj: any = {}
          obj.id = kraValue.krares(i).id().toFloat64();
          obj.user_id = kraValue.krares(i).userId().toFloat64();
          obj.start_date = kraValue.krares(i).startDate().toString();
          obj.end_date = kraValue.krares(i).endDate();
          obj.updated_user_id = kraValue.krares(i).updatedUserId().toFloat64();
          obj.created_user_id = kraValue.krares(i).createdUserId().toFloat64();
          obj.status = kraValue.krares(i).status();
          obj.roleId = kraValue.krares(i).roleId();
          obj.userName = kraValue.krares(i).userName();
          obj.user = kraValue.krares(i).user();
          obj.apraisalYear = kraValue.krares(i).apraisalYear();
          rating = kraValue.krares(i).rating();
          kraValueArr.push(obj)
      }
      // console.log("kraValueArr",kraValueArr)
      for (let i = 0; i <= feedBackLength - 1; i++) {
          let obj: any = {}
          obj.id = kraValue.feedbackList(i).id().toFloat64();
          obj.kra_id = kraValue.feedbackList(i).kraId().toFloat64();
          obj.user_id = kraValue.feedbackList(i).feedbackUserId().toFloat64();
          obj.feedback_comment = kraValue.feedbackList(i).feedbackComment().toString();
          obj.system_comment = kraValue.feedbackList(i).systemComment();
          obj.created_date = kraValue.feedbackList(i).createdDate();
          obj.creator_name = kraValue.feedbackList(i).creatorName();
          obj.creator_image = kraValue.feedbackList(i).creatorImage();
          feedBackValueArr.push(obj)
      }
      for (let i = 0; i <= kraDetailsListLength - 1; i++) {
          let obj: any = {}
          obj.id = kraValue.kradetailsList(i).id().toFloat64();
          obj.kra_id = kraValue.kradetailsList(i).kraId().toFloat64();
          obj.goalId = kraValue.kradetailsList(i).goalId().toFloat64();
          obj.goal_type_id = kraValue.kradetailsList(i).goalType();
          obj.description = kraValue.kradetailsList(i).goal();
          // obj.created_user_id = kraValue.kradetailsList(i).goalCreatedDate();
          // obj.updated_user_id = kraValue.kradetailsList(i).goalUpdatedDate();
          obj.created_date = kraValue.kradetailsList(i).goalCreatedDate();
          obj.update_date = kraValue.kradetailsList(i).goalUpdatedDate();
          obj.created_user_name = kraValue.kradetailsList(i).goalCreatorName();
          obj.created_user_image = kraValue.kradetailsList(i).goalCreatorImage();
          kraDetailsListArr.push(obj)
      }
      const finalArray = {
          kraValueArr: kraValueArr,
          feedBackValueArr: feedBackValueArr,
          kraDetailsListArr: kraDetailsListArr,
      }
      dispatch(getAllUserComments(finalArray,rating));
    }).catch((error) => {
      dispatch(getEmpDetailFailure(error));
    });
  }
}
export const getEmployeeDetails: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data: any, type: string) => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader())
    const service = new APIUserManagement();
    return service.getEmployeeBYToken().then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../schema/addUser_generated').user;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.user.getRootAsuser(byteBuffer);
      console.log("Success Sundar", user);
      console.log("user", user)
      // var listLength = user.usersListLength();
      // console.log("listLength", listLength)
      var userobj = user.title();
      var firstName = user.firstName();
      var lastName = user.lastName();
      var photoUrl = user.empPhotoUrl();
      console.log("userobj", userobj)
      console.log("firstName", firstName)
      console.log("lastName", lastName)
      console.log("photoUrl", photoUrl)
      var obj = {
        "id": user.id().toFloat64(),
        "title": user.title(),
        "first_name": user.firstName(),
        "last_name": user.lastName(),
        "emp_id": user.empId(),
        "emp_photo_url": user.empPhotoUrl(),
        "status": user.status(),
        "isSelected": false,
        "email_id": user.emailId(),
        "appraiser_id": user.appraiserId().toFloat64(),
        "created_user_id": user.createdUserId().toFloat64(),
        "updated_user_id": user.updatedUserId().toFloat64(),
        "role_id": user.roleId()
      }
      console.log("obj", obj)
      dispatch(getEmpDetailSuccess(obj));
    }).catch((error) => {
      dispatch(getEmpDetailFailure(error));
    });
  }
}
export const fileUpload: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data: any) => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    return service.showPopup().then((response) => {
      dispatch(fileUploadData(data));
    }).catch((error) => {
      dispatch(getEmpDetailFailure(error));
    });
  }
}
export const switchTabAction: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes>
> = (tabName: String, data: any, id: any) => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader());
    const service = new APIUserManagement();
    // var flatbuffers = require("flatbuffers").flatbuffers;
    console.log("Sundar ID", id)
    return service.getByTabName(tabName, id).then((response) => {
      if (tabName === "Appraisee KRA Details") {
        var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
      var levelLength = user.userlevelLength();
      var levelsLength = user.levelsLength();
      var levelarray = [];
      var levels = [];
      for (let i = 0; i < levelsLength; i++) {
        levels.push(user.levels(i))
      }
      // var obj = {
      //   "id": 0,
      //   "title": "",
      //   "first_name": "",
      //   "last_name": "",
      //   "emp_id": "",
      //   "emp_photo_url": "",
      //   "status": 0,
      //   "isSelected": false,
      //   "email_id": "",
      //   "appraiser_id": "",
      //   "created_user_id": "",
      //   "updated_user_id": "",
      //   "role_id": 0
      // }
      for (let i = 0; i < levelsLength; i++) {
        var dataarr = [];
        for (var j = 0; j < levelLength; j++) {
          let obj: any = {}
          obj.id = user.userlevel(j).id().toFloat64();
          obj.title = user.userlevel(j).title();
          obj.first_name = user.userlevel(j).firstName();
          obj.last_name = user.userlevel(j).lastName();
          obj.emp_id = user.userlevel(j).empId();
          obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
          obj.status = user.userlevel(j).status();
          obj.email_id = user.userlevel(j).emailId();
          obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
          obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
          obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
          obj.role_id = user.userlevel(j).roleId();
          obj.empLevel = user.userlevel(j).empLevel();
          obj.kraId = user.userlevel(j).kraId();
          obj.kraStartDate = user.userlevel(j).kraStartDate();
          obj.kraEndDate = user.userlevel(j).kraEndDate();
          obj.isSelected = false;
          if (levels[i] === obj.empLevel) {
            dataarr.push(obj);
          }
        }
        levelarray.push(dataarr)
      }
      dispatch(getAllUserProfile(levelarray));
      } else if (tabName === "Current") {
        var flatbufferss = require('flatbuffers').flatbuffers
        var getCurrentGoals = require('../../../schema/GoalsModuleSchema/getCurrentGoal_generated').getCurrentGoals
        var currentResponseArray = new Uint8Array(response.data);
        var currentByteBuffer = new flatbufferss.ByteBuffer(currentResponseArray);
        var CurrentKRAGoals = getCurrentGoals.userkragoalres.getRootAsuserkragoalres(currentByteBuffer);
       // var CurrentKRAGoalsLength = CurrentKRAGoals.kraresLength();
      //  var CurrentKRADetailGoals = getCurrentGoals.userkradetailsres.getRootAsuserkradetailsres(currentByteBuffer);
        var CurrentKRADetailGoalsLength = CurrentKRAGoals.kradetailsListLength();
        var kradetailsList = CurrentKRAGoals.kradetailsList();
        var id = CurrentKRAGoals.kradetailsList(0).id().toFloat64();
        var CurrentKRAGoalsLength = CurrentKRAGoals.krares(0);
        let kra_obj: any = {};
        kra_obj.id = CurrentKRAGoalsLength.id().toFloat64();
        kra_obj.userId = CurrentKRAGoalsLength.userId().toFloat64();
        kra_obj.start_date = CurrentKRAGoalsLength.startDate ();
        kra_obj.end_date = CurrentKRAGoalsLength.endDate ();
        kra_obj.updated_user_id = CurrentKRAGoalsLength.updatedUserId().toFloat64();
        kra_obj.created_user_id = CurrentKRAGoalsLength.createdUserId ().toFloat64();
        kra_obj.status = CurrentKRAGoalsLength.status();
    
       
        // console.log("get CurrentKRAGoals", CurrentKRAGoalsLength)
        // console.log("get CCurrentKRADetailGoalsLength", CurrentKRADetailGoalsLength)
        // console.log("get id", id)
        // console.log("get kradetailsList", kradetailsList)
        let arr = []
        if (CurrentKRADetailGoalsLength > 0) {
          for (let i = 0; i < CurrentKRADetailGoalsLength; i++) {
            let obj: any = {};
            obj.id = CurrentKRAGoals.kradetailsList(i).id().toFloat64();
            obj.kra_id = CurrentKRAGoals.kradetailsList(i).kraId().toFloat64();
            obj.goal_id = CurrentKRAGoals.kradetailsList(i).goalId().toFloat64();
            obj.goal_type = CurrentKRAGoals.kradetailsList(i).goalType();
            obj.goal_created_date = CurrentKRAGoals.kradetailsList(i).goalCreatedDate();
            obj.goal_updated_date = CurrentKRAGoals.kradetailsList(i).goalUpdatedDate();
            obj.goal_creator_name = CurrentKRAGoals.kradetailsList(i).goalCreatorName();
            obj.goal_creator_image = CurrentKRAGoals.kradetailsList(i).goalCreatorImage();
            obj.updated_on = CurrentKRAGoals.kradetailsList(i).updatedOn();
            obj.save_as_draft = CurrentKRAGoals.kradetailsList(i).saveAsDraft();
            obj.goal = CurrentKRAGoals.kradetailsList(i).goal();
            arr.push(obj);
          }
        }
        console.log("arr", arr)
        dispatch(getCurrentKRAGoalsSuccess(arr , kra_obj));
      } else if (tabName === "History") {
        var flatbuffersss = require('flatbuffers').flatbuffers
        var userDataKRAHistory = require("../schema/KraHistory_generated")
          .kraUserHistory;
        var KRAHistoryResponseArray = new Uint8Array(response.data);
        var KRAHistoryByteBuffer = new flatbuffersss.ByteBuffer(KRAHistoryResponseArray);
        var history = userDataKRAHistory.kraHistory.getRootAskraHistory(KRAHistoryByteBuffer);
        var historyLength = history.historyListLength();

        let arr = [];
        for (let i = 0; i < historyLength; i++) {
          const obj: any = {};
          obj.id = history
            .historyList(i)
            .id()
            .toFloat64();
          obj.userId = history
            .historyList(i)
            .userId()
            .toFloat64();
          obj.startDate = history.historyList(i).startDate();
          obj.endDate = history.historyList(i).endDate();
          obj.name = history.historyList(i).appraiserName();
          obj.appraisalYear = history.historyList(i).appraisalYear();
          obj.appraiserId = history.historyList(i).appraiserId();
          console.log(obj);
          arr.push(obj);
        }
        console.log("get Histroy", arr);
        dispatch(getHistory(arr));
      }
    }).catch((error) => {
      if (error && error.response && error.response.status === 500){
        // window.alert("Internal server error!");
        // dispatch(showPopup("Internal Server Error!"));
    } else if((error && error.response && error.response.status === 404)){
        // window.alert("kra not found");
    } else{
        let responseSuccess
        var flatbuffers = require('flatbuffers').flatbuffers
        var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
        var responseArray = new Uint8Array(error.value);
        var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
        var responseStatus = userData.response.getRootAsresponse(byteBuffer);
        responseSuccess = responseStatus.message();
        // window.alert(responseSuccess);
    // dispatch(showPopup(error));
    }
      if (tabName === "Appraisee KRA Details") {
        dispatch(getAllUserProfile([]));
      }else if (tabName === "Current") {
        dispatch(getCurrentKRAGoalsSuccess([],''));
      }else if (tabName === "History") {
        dispatch(getHistory([]));
      }
      dispatch(getAllUserProfileFailed(error));
    });
  }
};

export const getCurrentKRAGoals: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (data) => {
  return (dispatch: Dispatch) => {
    dispatch(showLoader());
    const service = new APIUserManagement();
    return service.getCurrentKRAGoals(data).then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var getCurrentGoals = require('../../../schema/GoalsModuleSchema/getCurrentGoal_generated').getCurrentGoals
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var CurrentKRAGoals = getCurrentGoals.userkragoalres.getRootAsuserkragoalres(byteBuffer);
      var CurrentKRAGoalsLength = CurrentKRAGoals.krares(0);
     
      let kra_obj: any = {};
      kra_obj.id = CurrentKRAGoalsLength.id().toFloat64();
      kra_obj.userId = CurrentKRAGoalsLength.userId().toFloat64();
      kra_obj.start_date = CurrentKRAGoalsLength.startDate ();
      kra_obj.end_date = CurrentKRAGoalsLength.endDate ();
      kra_obj.updated_user_id = CurrentKRAGoalsLength.updatedUserId().toFloat64();
      kra_obj.created_user_id = CurrentKRAGoalsLength.createdUserId ().toFloat64();
      kra_obj.status = CurrentKRAGoalsLength.status();
     
    
     
      var CurrentKRADetailGoalsLength = CurrentKRAGoals.kradetailsListLength();
      var kradetailsList = CurrentKRAGoals.kradetailsList();
      var id = CurrentKRAGoals.kradetailsList(0).id().toFloat64();
      // console.log("get CurrentKRAGoals", CurrentKRAGoalsLength)
      // console.log("get CCurrentKRADetailGoalsLength", CurrentKRADetailGoalsLength)
      // console.log("get id", id)
      // console.log("get kradetailsList", kradetailsList)
      let arr = []
      if (CurrentKRADetailGoalsLength > 0) {
        for (let i = 0; i < CurrentKRADetailGoalsLength; i++) {
          let obj: any = {};
          obj.id = CurrentKRAGoals.kradetailsList(i).id().toFloat64();
          obj.kra_id = CurrentKRAGoals.kradetailsList(i).kraId().toFloat64();
          obj.goal_id = CurrentKRAGoals.kradetailsList(i).goalId().toFloat64();
          obj.goal_type = CurrentKRAGoals.kradetailsList(i).goalType();
          obj.goal_created_date = CurrentKRAGoals.kradetailsList(i).goalCreatedDate();
          obj.goal_updated_date = CurrentKRAGoals.kradetailsList(i).goalUpdatedDate();
          obj.goal_creator_name = CurrentKRAGoals.kradetailsList(i).goalCreatorName();
          obj.goal_creator_image = CurrentKRAGoals.kradetailsList(i).goalCreatorImage();
          obj.updated_on = CurrentKRAGoals.kradetailsList(i).updatedOn();
          obj.save_as_draft = CurrentKRAGoals.kradetailsList(i).saveAsDraft();
          obj.goal = CurrentKRAGoals.kradetailsList(i).goal();
          arr.push(obj);
        }
      }
      console.log("arr", arr)
      dispatch(getCurrentKRAGoalsSuccess(arr , kra_obj));
    }).catch((error) => {
      dispatch(getCurrentKRAGoalsSuccess([],''));
    });
  }
}
const getCurrentKRAGoalsList = (data: any): Promise<any> => {
  return new Promise((resolve, reject) => {
    const service = new APIUserManagement();
    return service.getCurrentKRAGoals(data).then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var getCurrentGoals = require('../../../schema/GoalsModuleSchema/getCurrentGoal_generated').getCurrentGoals
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var CurrentKRAGoals = getCurrentGoals.userkragoalres.getRootAsuserkragoalres(byteBuffer);
      var CurrentKRAGoalsLength = CurrentKRAGoals.krares(0);
     
      let kra_obj: any = {};
      kra_obj.id = CurrentKRAGoalsLength.id().toFloat64();
      kra_obj.userId = CurrentKRAGoalsLength.userId().toFloat64();
      kra_obj.start_date = CurrentKRAGoalsLength.startDate ();
      kra_obj.end_date = CurrentKRAGoalsLength.endDate ();
      kra_obj.updated_user_id = CurrentKRAGoalsLength.updatedUserId().toFloat64();
      kra_obj.created_user_id = CurrentKRAGoalsLength.createdUserId ().toFloat64();
      kra_obj.status = CurrentKRAGoalsLength.status();
     
    
     
      var CurrentKRADetailGoalsLength = CurrentKRAGoals.kradetailsListLength();
      var kradetailsList = CurrentKRAGoals.kradetailsList();
      var id = CurrentKRAGoals.kradetailsList(0).id().toFloat64();
      // console.log("get CurrentKRAGoals", CurrentKRAGoalsLength)
      // console.log("get CCurrentKRADetailGoalsLength", CurrentKRADetailGoalsLength)
      // console.log("get id", id)
      // console.log("get kradetailsList", kradetailsList)
      let arr = []
      if (CurrentKRADetailGoalsLength > 0) {
        for (let i = 0; i < CurrentKRADetailGoalsLength; i++) {
          let obj: any = {};
          obj.id = CurrentKRAGoals.kradetailsList(i).id().toFloat64();
          obj.kra_id = CurrentKRAGoals.kradetailsList(i).kraId().toFloat64();
          obj.goal_id = CurrentKRAGoals.kradetailsList(i).goalId().toFloat64();
          obj.goal_type = CurrentKRAGoals.kradetailsList(i).goalType();
          obj.goal_created_date = CurrentKRAGoals.kradetailsList(i).goalCreatedDate();
          obj.goal_updated_date = CurrentKRAGoals.kradetailsList(i).goalUpdatedDate();
          obj.goal_creator_name = CurrentKRAGoals.kradetailsList(i).goalCreatorName();
          obj.goal_creator_image = CurrentKRAGoals.kradetailsList(i).goalCreatorImage();
          obj.updated_on = CurrentKRAGoals.kradetailsList(i).updatedOn();
          obj.save_as_draft = CurrentKRAGoals.kradetailsList(i).saveAsDraft();
          obj.goal = CurrentKRAGoals.kradetailsList(i).goal();
          arr.push(obj);
        }
      }
        resolve(arr);
    }).catch((error) => {
        reject(error)
    });
})
}

const getCurrentGoals = (data: any,type:any): Promise<any> => {
  debugger
  return new Promise((resolve, reject) => {
      const service = new APIUserManagement();
      return service.getKRASpecificUserHistory(data, type).then((response) => {
        console.log(response);
        var flatbuffers = require('flatbuffers').flatbuffers;
        var kraData = require('../schema/kraList_generated').kraList;
        var responseArray = new Uint8Array(response.data);
        var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
        var kraValue = kraData.kraList.getRootAskraList(byteBuffer);
        var kraLength = kraValue.kraresLength();
        var feedBackLength = kraValue.feedbackListLength();
        var kraDetailsListLength = kraValue.kradetailsListLength();
        const kraValueArr = [];
        const feedBackValueArr = [];
        const kraDetailsListArr = [];
        let rating: any;
        for (let i = 0; i <= kraLength - 1; i++) {
            let obj: any = {}
            obj.id = kraValue.krares(i).id().toFloat64();
            obj.user_id = kraValue.krares(i).userId().toFloat64();
            obj.start_date = kraValue.krares(i).startDate().toString();
            obj.end_date = kraValue.krares(i).endDate();
            obj.updated_user_id = kraValue.krares(i).updatedUserId().toFloat64();
            obj.created_user_id = kraValue.krares(i).createdUserId().toFloat64();
            obj.status = kraValue.krares(i).status();
            obj.roleId = kraValue.krares(i).roleId();
            obj.userName = kraValue.krares(i).userName();
            obj.user = kraValue.krares(i).user();
            obj.apraisalYear = kraValue.krares(i).apraisalYear();
            rating = kraValue.krares(i).rating();
            kraValueArr.push(obj)
        }
        // console.log("kraValueArr",kraValueArr)
        for (let i = 0; i <= feedBackLength - 1; i++) {
            let obj: any = {}
            obj.id = kraValue.feedbackList(i).id().toFloat64();
            obj.kra_id = kraValue.feedbackList(i).kraId().toFloat64();
            obj.user_id = kraValue.feedbackList(i).feedbackUserId().toFloat64();
            obj.feedback_comment = kraValue.feedbackList(i).feedbackComment().toString();
            obj.system_comment = kraValue.feedbackList(i).systemComment();
            obj.created_date = kraValue.feedbackList(i).createdDate();
            obj.creator_name = kraValue.feedbackList(i).creatorName();
            obj.creator_image = kraValue.feedbackList(i).creatorImage();
            feedBackValueArr.push(obj)
        }
        for (let i = 0; i <= kraDetailsListLength - 1; i++) {
            let obj: any = {}
            obj.id = kraValue.kradetailsList(i).id().toFloat64();
            obj.kra_id = kraValue.kradetailsList(i).kraId().toFloat64();
            obj.goalId = kraValue.kradetailsList(i).goalId().toFloat64();
            obj.goal_type_id = kraValue.kradetailsList(i).goalType();
            obj.description = kraValue.kradetailsList(i).goal();
            // obj.created_user_id = kraValue.kradetailsList(i).goalCreatedDate();
            // obj.updated_user_id = kraValue.kradetailsList(i).goalUpdatedDate();
            obj.created_date = kraValue.kradetailsList(i).goalCreatedDate();
            obj.update_date = kraValue.kradetailsList(i).goalUpdatedDate();
            obj.created_user_name = kraValue.kradetailsList(i).goalCreatorName();
            obj.created_user_image = kraValue.kradetailsList(i).goalCreatorImage();
            kraDetailsListArr.push(obj)
        }
        const finalArray = {
            kraValueArr: kraValueArr,
            feedBackValueArr: feedBackValueArr,
            kraDetailsListArr: kraDetailsListArr,
        }
        resolve(finalArray);
    }).catch((error) => {
        reject(error)
    });
})
}




export const addAndUpdateAppraiserComment: ActionCreator<
    ThunkAction<
    Promise<any>,
    AppraiserManagementState,
     null,
    AppraiserManagementTypes
>
> = (param: any , type : String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addcomment = require('../schema/appraisee_generated_generated').Appraisercomment;
        var addCommentData = addcomment.comments
        if(param.created_date == null){
          param.created_date = ""
        }
        if(param.updated_date == null){
          param.updated_date = ""
        }
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var kra_id = fbb.createLong(param.kra_id);
        var goal_id = fbb.createLong(param.goal_id);
        var comment = fbb.createString(param.comment);
        var created_date = fbb.createString(param.created_date);
        var updated_date = fbb.createString(param.updated_date);
        var save_as_draft = param.save_as_draft
        var fileName = param.filepath ? param.filepath: '';
        var filepath = fbb.createString(fileName);
        addCommentData.startcomments(fbb)
        addCommentData.addId(fbb, id)
        addCommentData.addKraId(fbb, kra_id)
        addCommentData.addGoalId(fbb, goal_id)
        addCommentData.addComment(fbb, comment)
        addCommentData.addCreatedDate(fbb, created_date)
        addCommentData.addUpdatedDate(fbb, updated_date)
        addCommentData.addSaveAsDraft(fbb, save_as_draft)
        addCommentData.addFilepath(fbb, filepath)
      
      
        var ints = addCommentData.endcomments(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        if(type === "add"){
          return service.appraiserCommentadd(value).then((response) => {
            getCurrentKRAGoalsList(param.empId)
            .then((response) => {
            if (response) {
                dispatch(refreshCurrentKRAGoal(response, "Your Comments Saved Successfully"));
            }
          })
        }).catch((error) => {
          dispatch(showPopup(false));
          let responseSuccess
          if(typeof(error) === 'string'){
            responseSuccess = error;
        } else if (typeof(error) === 'object'){
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
            var responseArray = new Uint8Array(error.value);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var responseStatus = userData.response.getRootAsresponse(byteBuffer);
            responseSuccess = responseStatus.message();
        }
        window.alert(responseSuccess);
        });
        }else {
          return service.appraiserCommentupdate(value ,param.kra_id,param.goal_id).then((response) => {
            getCurrentKRAGoalsList(param.empId)
            .then((response) => {
            if (response) {
                dispatch(refreshCurrentKRAGoal(response, "Your comments Saved Successfully"));
            }
          })
        }).catch((error) => {
          dispatch(showPopup(false));
          let responseSuccess
          if(typeof(error) === 'string'){
            responseSuccess = error;
        } else if (typeof(error) === 'object'){
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
            var responseArray = new Uint8Array(error.value);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var responseStatus = userData.response.getRootAsresponse(byteBuffer);
            responseSuccess = responseStatus.message();
        }
        window.alert(responseSuccess);
        });
        }
    }
}


export const AppraiserSaveAll: ActionCreator<
    ThunkAction<
    Promise<any>,
    AppraiserManagementState,
     null,
    AppraiserManagementTypes
>
> = (param: any ) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addcomment = require('../schema/kra_generated').KraUpdate;
        var addCommentData = addcomment.kra
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var user_id = fbb.createLong(param.user_id);
        var start_date = fbb.createString(param.start_date);
        var end_date = fbb.createString(param.end_date);
        var updated_user_id = fbb.createLong(param.updated_user_id);
        var created_user_id = fbb.createLong(param.created_user_id);
        var status = param.status;
        var rating =param.rating;
        var saved_as_draft = param.saved_as_draft
      
       
        addCommentData.startkra(fbb)
        addCommentData.addId(fbb, id)
        addCommentData.addUserId(fbb, user_id)
        addCommentData.addStartDate(fbb, start_date)
        addCommentData.addEndDate(fbb, end_date)
        addCommentData.addUpdatedUserId(fbb, updated_user_id)
        addCommentData.addCreatedUserId(fbb, created_user_id)
        addCommentData.addStatus(fbb, status)
        addCommentData.addRating(fbb, rating)
        addCommentData.addSavedAsDraft(fbb, saved_as_draft)
      
      
        var ints = addCommentData.endkra(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();

       
        return service.appraiserSaveAll(value).then((response) => {
          getCurrentKRAGoalsList(param.empId)
            .then((responses) => {
              if (response) {
                dispatch(refreshCurrentKRAGoal(responses, "Your Kra Saved Successfully"));
              }
              if (response) {
                var flatbuffers = require('flatbuffers').flatbuffers
                var userData = require('../../../schema/ResponseStatus/response_generated').ResponseStatus
                var responseArray = new Uint8Array(response.value);
                var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
                var responseStatus = userData.response.getRootAsresponse(byteBuffer);
                var responseSuccess = responseStatus.message();
              //  dispatch(hide());
                // window.alert(responseSuccess);
                dispatch(ShowinfoAlert(true));
            }
              }).catch((error) => {
                dispatch(ShowinfoAlert(false));
              })
        }).catch((error) => {
          dispatch(hidePopup(error));
        });
        }
    }


// <Promise<Return Type>, State Interface, Type of Param, Type of Action>
export const searchCharacters: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (term: String, filterType: String) => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    return service.search(term, "active").then((response) => {
      var flatbuffers = require('flatbuffers').flatbuffers
      var userData = require('../../../schema/UserLevelBasis/userlevelbassis_generated').LevelBasis;
      var responseArray = new Uint8Array(response.data);
      var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
      var user = userData.userlevelList.getRootAsuserlevelList(byteBuffer);
      var levelLength = user.userlevelLength();
      var levelsLength = user.levelsLength();
      var levelarray = [];
      var levels = [];
      for (let i = 0; i < levelsLength; i++) {
        levels.push(user.levels(i))
      }
      // var obj = {
      //   "id": 0,
      //   "title": "",
      //   "first_name": "",
      //   "last_name": "",
      //   "emp_id": "",
      //   "emp_photo_url": "",
      //   "status": 0,
      //   "isSelected": false,
      //   "email_id": "",
      //   "appraiser_id": "",
      //   "created_user_id": "",
      //   "updated_user_id": "",
      //   "role_id": 0
      // }
      for (let i = 0; i < levelsLength; i++) {
        var dataarr = [];
        for (var j = 0; j < levelLength; j++) {
          let obj: any = {}
          obj.id = user.userlevel(j).id().toFloat64();
          obj.title = user.userlevel(j).title();
          obj.first_name = user.userlevel(j).firstName();
          obj.last_name = user.userlevel(j).lastName();
          obj.emp_id = user.userlevel(j).empId();
          obj.emp_photo_url = user.userlevel(j).empPhotoUrl();
          obj.status = user.userlevel(j).status();
          obj.email_id = user.userlevel(j).emailId();
          obj.appraiser_id = user.userlevel(j).appraiserId().toFloat64();
          obj.created_user_id = user.userlevel(j).createdUserId().toFloat64();
          obj.updated_user_id = user.userlevel(j).updatedUserId().toFloat64();
          obj.role_id = user.userlevel(j).roleId();
          obj.empLevel = user.userlevel(j).empLevel();
          obj.kraId = user.userlevel(j).kraId();
          obj.kraStartDate = user.userlevel(j).kraStartDate();
          obj.kraEndDate = user.userlevel(j).kraEndDate();
          obj.isSelected = false;
          if (levels[i] === obj.empLevel) {
            dataarr.push(obj);
          }
        }
        levelarray.push(dataarr)
      }
      dispatch(getAllUserProfile(levelarray));
    }).catch((error) => {
      dispatch(getAllUserProfileFailed(error));
    });
  };
};

export const getgoalComments: ActionCreator<
  ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >
> = (goalId: any, kraid: any) => {
    
    return (dispatch: Dispatch) => {
      dispatch(showLoader());
        const service = new APIUserManagement();
        return service.getComments(goalId, kraid).then((response) => {
            var flatbuffers = require('flatbuffers').flatbuffers
            var userData = require('../schema/appraisee_generated_generated').Appraisercomment;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var commets = userData.commentList.getRootAscommentList(byteBuffer);
            // debugger
            var commentListLength = commets.commentListLength();
            var commentList = commets.commentList();
            var filePath = "";
            console.log("commentListLength",commentListLength)
            console.log("commentList",commentList)
            var dataArr = []
            // var obj = {
            //   id: 0,
            //   kra_id: "",
            //   goal_id: "",
            //   comment: "",
            //   created_date: "",
            //   updated_date: "",
            //   save_as_draft: "",
            // }
            for(let i=0; i < commentListLength;i++){
              let obj: any = {}
              obj.id= commets.commentList(i).id().toFloat64();
              obj.kra_id= commets.commentList(i).kraId().toFloat64();
              obj.goal_id= commets.commentList(i).goalId().toFloat64();
              obj.comment= commets.commentList(i).comment();
              obj.created_date= commets.commentList(i).createdDate();
              obj.updated_date= commets.commentList(i).updatedDate();
              obj.save_as_draft= commets.commentList(i).saveAsDraft();
              obj.filepath= commets.commentList(i).filepath();
              filePath = commets.commentList(0).filepath();
              dataArr.push(obj)
            }
            
            console.log("dataArr", dataArr)
            dispatch(getCommentdata(dataArr,filePath));   
        }).catch((error) => {
            if (error && error.response && error.response.status === 404){
              dispatch(getCommentdata([],""))
            }else{
            }
        });
    }
}
export const overAllSaveAction: ActionCreator<ThunkAction<
  Promise<any>,
  AppraiserManagementState,
  null,
  AppraiserManagementTypes
>> = (data: any) => {
  return (dispatch: Dispatch) => {
    const service = new APIUserManagement();
    var flatbuffers = require("flatbuffers").flatbuffers;
    var addcomment = require("../schema/kra_generated").user;
    var addCommentData = addcomment.comments;
    var fbb = new flatbuffers.Builder(0);
    var id = fbb.createLong(data.id);
    var user_id = fbb.createLong(data.user_id);
    var start_date = fbb.createString(data.start_date);
    var end_date = fbb.createString(data.end_date);
    var updated_user_id = fbb.createLong(data.updated_user_id);
    var created_user_id = fbb.createLong(data.created_user_id);
    var status = fbb.createString(data.status);
    var rating = fbb.createLong(data.rating);
    var saved_as_draft = data.saved_as_draft;

    addCommentData.startkra(fbb);
    addCommentData.addId(fbb, id);
    addCommentData.addUserId(fbb, user_id);
    addCommentData.addStartDate(fbb, start_date);
    addCommentData.addEndDate(fbb, end_date);
    addCommentData.addUpdatedUserId(fbb, updated_user_id);
    addCommentData.addCreatedUserId(fbb, created_user_id);
    addCommentData.addStatus(fbb, status);
    addCommentData.addRating(fbb, rating);
    addCommentData.addSavedAsDraft(fbb, saved_as_draft);

    var ints = addCommentData.endkra(fbb);
    fbb.finish(ints);
    var value = fbb.asUint8Array();

    return service
      .appraiserSaveAll(value)
      .then(response => {
        if (response) {
          var flatbuffers = require("flatbuffers").flatbuffers;
          var userData = require("../../../schema/ResponseStatus/response_generated")
            .ResponseStatus;
          var responseArray = new Uint8Array(response.value);
          var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
          var responseStatus = userData.response.getRootAsresponse(byteBuffer);
          var responseSuccess = responseStatus.message();
          window.alert(responseSuccess)
          dispatch(hide());
        }
      })
      .catch(error => {
        dispatch(hidePopup(error));
      });
  };
};
export const addComments: ActionCreator<
    ThunkAction<
    Promise<any>,
    AppraiserManagementState,
    null,
    AppraiserManagementTypes
  >> = (data: any,userData: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader());
        const service = new APIUserManagement();
        console.log("add comment Data", data)
        console.log("add kraId Data", userData.kraId)
        var flatbuffers = require('flatbuffers').flatbuffers;
        var goaldetails = require('../../UserGoalDetailManagement/schema/feedback_generated').AddComments;
        var addcomments = goaldetails.feedback;
         var fbb = new flatbuffers.Builder(0);
         var id = fbb.createLong('');
         var kraid = fbb.createLong(userData.kraId);
         var feedback_user_id = fbb.createLong(userData.userId);
         var feedback_comment = fbb.createString(data);
         var system_comment = fbb.createLong(false);
         var id = fbb.createLong('');
         addcomments.startfeedback(fbb);
         addcomments.addId(fbb, id)
         addcomments.addKraId(fbb, kraid)
         addcomments.addFeedbackUserId(fbb, feedback_user_id)
         addcomments.addFeedbackComment(fbb, feedback_comment)
         addcomments.addSystemComment(fbb, system_comment)

        var ints = addcomments.endfeedback(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();

        return service.addComments(value,userData.kraId).then((response) => {
          getCurrentGoals(userData,'current')
            .then((response) => {
                dispatch(showLoader())
                console.log(response)
                var kraValueArr = response.kraValueArr
                var rating = kraValueArr[0].rating
                dispatch(getAllUserComments(response,rating));
            })
            .catch((error) => {
                dispatch(getAllUserComments([],0.0));
                console.log(error)
            })
            

        }).catch((error) => {
            dispatch(getAllUserComments([],0.0));
            console.log("add Goals error", error)

        });
    }
}
