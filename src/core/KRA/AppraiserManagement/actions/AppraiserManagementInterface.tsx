/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import keys from "./AppraiserManagementTypeKeys";

export interface IShowAction {
    readonly type: keys.SHOW_POPUP,
    showPopup: Boolean,
    text: any,
    showLoading: false
}
export interface IManageSuccessAction {
    readonly type: keys.MANAGE_PROFILE_SUCCESS;
    readonly value: any;
    readonly isEmpty: Boolean;
    readonly showLoading: Boolean;
    readonly tabName: any
}
export interface IGetComments {
    readonly type: keys.GETCOMMENTS,
    showPopUp: any,
    commentsData:any,
    fileUploadData:any,
    readonly showLoading: boolean;
}
export interface IGetHistory {
    readonly type: keys.GETHISTORY;
    readonly value: any;
    readonly tabName: any;
    readonly isEmpty: any;
    readonly showLoading: boolean;
}

export interface IHideAcion {
    readonly type: keys.HIDEPOPUP,
    showPopup: Boolean,
    readonly showLoading: boolean;
}

export interface ShowinfoAlertAcion {
    readonly type: keys.SHOWINFOALERT,
    showinfoalert: Boolean,
    showPopup: boolean,
    readonly showLoading: boolean;
}

export interface IGetEmpSuccessInfo {
    readonly type: keys.GET_EMP_DETAIL_SUCCESS;
    readonly value: any;
    readonly showLoading: boolean;
}
export interface IFileUpload {
    readonly type: keys.FILEUPLOAD;
    readonly value: any;
    readonly showLoading: boolean;
}
export interface IShowLoading {
    readonly type: keys.SHOWLOADING;
    readonly showLoading: boolean;
}
export interface IGetEmpCurrentGoals {
    readonly type: keys.GETEMPCURRENTGOALS;
    readonly value: any;
    readonly isEmpty:any;
    readonly tabName: any;
    readonly kradetails : any;
    readonly showLoading : boolean;
}
export interface IRefreshGoal{
    readonly type: keys.REFRESHGOAL;
    readonly value: any;
    readonly isEmpty:any;
    readonly tabName: any;
    readonly showLoading : boolean;
    readonly showinfoalert : boolean;
    readonly showinfoValue : any;

}
export interface IGetUserComments{
    readonly type: keys.GETALLUSERCOMMENTS;
    readonly commentsValue: any;
    readonly rating:any;
    readonly showLoading : boolean;

}
export interface IGetEmpFailureInfo {
    readonly type: keys.GET_EMP_DETAIL_FAILURE;
    readonly showLoading: boolean;
    readonly payload: {
        readonly error: Error;
    };
}

// export interface IShowPopup {
//     readonly type: keys.SHOW_POPUP;
//     readonly value: any;
// }