/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import {
    IHideAcion,
    IShowAction,
    IGetHistory,
    IGetEmpSuccessInfo,
    IGetEmpFailureInfo,
    IManageSuccessAction,
    IGetEmpCurrentGoals,
    IGetComments,
    ShowinfoAlertAcion,
    IShowLoading,
    IRefreshGoal,
    IGetUserComments,
    IFileUpload
} from "./AppraiserManagementInterface";
import { IShowPopup } from "../../EmployeeManagement/actions/EmployeeManagementInterface";


type KRAHistoryManagementTypes =
    |IShowAction
    |IHideAcion
    |IManageSuccessAction
    |IGetHistory
    |IGetEmpSuccessInfo
    |IGetEmpFailureInfo
    |IShowAction
    |IGetComments
    |ShowinfoAlertAcion
    |IGetEmpCurrentGoals
    |IRefreshGoal
    |IGetUserComments
    |IFileUpload
    |IShowLoading ;

export default KRAHistoryManagementTypes;