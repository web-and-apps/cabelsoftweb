/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import AppraiserManagementTypes from '../actions/AppraiserManagementTypes'
import AppraiserManagementState from '../state/AppraiserManagementState';
import AppraiserManagementTypeKeys from '../actions/AppraiserManagementTypeKeys';

const initialState: AppraiserManagementState = {
    profileValue: [],
    value: false,
    isEmpty: false,
    history:[],
    getEmpDetail: {},
    getCurrentKRAGoalsList: [],
    tabName: "Appraisee KRA Details",
    userkra : [],
    commentData:'',
    showinfoalert : '',
    kradetails : '',
    showLoading: false,
    rating: '',
    commentsValue: '',
    fileData:'',
    showinfoValue: ''
};

const AppraiserManagementReducers: Reducer<AppraiserManagementState, AppraiserManagementTypes> = (state = initialState, action) => {
    switch (action.type) {
        case AppraiserManagementTypeKeys.MANAGE_PROFILE_SUCCESS: {
            return {
                ...state,
                profileValue: action.value,
                isEmpty: action.isEmpty,
                tabName:action.tabName,
                showLoading: action.showLoading
            };
        }
        case AppraiserManagementTypeKeys.SHOW_POPUP: {
            return {
                ...state,
                value: action.showPopup,
                getEmpDetail : action.text,
                showLoading: action.showLoading,
            };
        }
        case AppraiserManagementTypeKeys.FILEUPLOAD: {
            return{
                ...state,
                fileData: action.value,
                showLoading: action.showLoading

            }
        }
        case AppraiserManagementTypeKeys.SHOWLOADING: {
            return {
                ...state,
                showLoading: action.showLoading,
            };
        }
         case AppraiserManagementTypeKeys.HIDEPOPUP: {
            return {
                ...state,
                value: action.showPopup,
                showLoading: action.showLoading
            };
        }
        case AppraiserManagementTypeKeys.GETCOMMENTS: {
            return {
                ...state,
                value   : action.showPopUp,
                commentData: action.commentsData,
                fileData: action.fileUploadData,
                showLoading: action.showLoading,
            };
        }
        case AppraiserManagementTypeKeys.GET_EMP_DETAIL_SUCCESS: {
            return {
                ...state,
                getEmpDetail: action.value,
            };
        }
        case AppraiserManagementTypeKeys.SHOWINFOALERT: {
            return {
                ...state,
                showinfoalert: action.showinfoalert,
                showPopup: false,
                showLoading: action.showLoading
            };
        }
        case AppraiserManagementTypeKeys.GETHISTORY: {
            return {
                ...state,
                history: action.value,
                tabName: "History",
                isEmpty: action.isEmpty,
                showLoading: action.showLoading

            };
        }
        case AppraiserManagementTypeKeys.GETEMPCURRENTGOALS: {
            return {
                ...state,
                getCurrentKRAGoalsList: action.value,
                tabName: "Current",
                isEmpty: action.isEmpty,
                kradetails : action.kradetails,
                showLoading: action.showLoading
            };
        }
        case AppraiserManagementTypeKeys.REFRESHGOAL: {
            return {
                ...state,
                getCurrentKRAGoalsList: action.value,
                tabName: "Current",
                isEmpty: action.isEmpty,
                showLoading: action.showLoading,
                value: false,
                showinfoalert: action.showinfoalert,
                showinfoValue: action.showinfoValue,
            };
        }
        case AppraiserManagementTypeKeys.GETALLUSERCOMMENTS: {
            return {
                ...state,
                commentsValue: action.commentsValue,
                rating:action.rating,
                showLoading: action.showLoading
            };
        }
        default:
            return state;
    }
};

export default AppraiserManagementReducers;