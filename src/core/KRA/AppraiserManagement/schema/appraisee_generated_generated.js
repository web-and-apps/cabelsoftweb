// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var Appraisercomment = Appraisercomment || {};

/**
 * @constructor
 */
Appraisercomment.commentList = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {Appraisercomment.commentList}
 */
Appraisercomment.commentList.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {Appraisercomment.commentList=} obj
 * @returns {Appraisercomment.commentList}
 */
Appraisercomment.commentList.getRootAscommentList = function(bb, obj) {
  return (obj || new Appraisercomment.commentList).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {number} index
 * @param {Appraisercomment.comments=} obj
 * @returns {Appraisercomment.comments}
 */
Appraisercomment.commentList.prototype.commentList = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? (obj || new Appraisercomment.comments).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
Appraisercomment.commentList.prototype.commentListLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
Appraisercomment.commentList.startcommentList = function(builder) {
  builder.startObject(1);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} commentListOffset
 */
Appraisercomment.commentList.addCommentList = function(builder, commentListOffset) {
  builder.addFieldOffset(0, commentListOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
Appraisercomment.commentList.createCommentListVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
Appraisercomment.commentList.startCommentListVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
Appraisercomment.commentList.endcommentList = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} commentListOffset
 * @returns {flatbuffers.Offset}
 */
Appraisercomment.commentList.createcommentList = function(builder, commentListOffset) {
  Appraisercomment.commentList.startcommentList(builder);
  Appraisercomment.commentList.addCommentList(builder, commentListOffset);
  return Appraisercomment.commentList.endcommentList(builder);
}

/**
 * @constructor
 */
Appraisercomment.comments = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {Appraisercomment.comments}
 */
Appraisercomment.comments.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {Appraisercomment.comments=} obj
 * @returns {Appraisercomment.comments}
 */
Appraisercomment.comments.getRootAscomments = function(bb, obj) {
  return (obj || new Appraisercomment.comments).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
Appraisercomment.comments.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
Appraisercomment.comments.prototype.kraId = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
Appraisercomment.comments.prototype.goalId = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
Appraisercomment.comments.prototype.comment = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
Appraisercomment.comments.prototype.createdDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
Appraisercomment.comments.prototype.updatedDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 14);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {boolean}
 */
Appraisercomment.comments.prototype.saveAsDraft = function() {
  var offset = this.bb.__offset(this.bb_pos, 16);
  return offset ? !!this.bb.readInt8(this.bb_pos + offset) : false;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
Appraisercomment.comments.prototype.filepath = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 18);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Builder} builder
 */
Appraisercomment.comments.startcomments = function(builder) {
  builder.startObject(8);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
Appraisercomment.comments.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} kraId
 */
Appraisercomment.comments.addKraId = function(builder, kraId) {
  builder.addFieldInt64(1, kraId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} goalId
 */
Appraisercomment.comments.addGoalId = function(builder, goalId) {
  builder.addFieldInt64(2, goalId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} commentOffset
 */
Appraisercomment.comments.addComment = function(builder, commentOffset) {
  builder.addFieldOffset(3, commentOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} createdDateOffset
 */
Appraisercomment.comments.addCreatedDate = function(builder, createdDateOffset) {
  builder.addFieldOffset(4, createdDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} updatedDateOffset
 */
Appraisercomment.comments.addUpdatedDate = function(builder, updatedDateOffset) {
  builder.addFieldOffset(5, updatedDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {boolean} saveAsDraft
 */
Appraisercomment.comments.addSaveAsDraft = function(builder, saveAsDraft) {
  builder.addFieldInt8(6, +saveAsDraft, +false);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} filepathOffset
 */
Appraisercomment.comments.addFilepath = function(builder, filepathOffset) {
  builder.addFieldOffset(7, filepathOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
Appraisercomment.comments.endcomments = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} offset
 */
Appraisercomment.comments.finishcommentsBuffer = function(builder, offset) {
  builder.finish(offset);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Long} kraId
 * @param {flatbuffers.Long} goalId
 * @param {flatbuffers.Offset} commentOffset
 * @param {flatbuffers.Offset} createdDateOffset
 * @param {flatbuffers.Offset} updatedDateOffset
 * @param {boolean} saveAsDraft
 * @param {flatbuffers.Offset} filepathOffset
 * @returns {flatbuffers.Offset}
 */
Appraisercomment.comments.createcomments = function(builder, id, kraId, goalId, commentOffset, createdDateOffset, updatedDateOffset, saveAsDraft, filepathOffset) {
  Appraisercomment.comments.startcomments(builder);
  Appraisercomment.comments.addId(builder, id);
  Appraisercomment.comments.addKraId(builder, kraId);
  Appraisercomment.comments.addGoalId(builder, goalId);
  Appraisercomment.comments.addComment(builder, commentOffset);
  Appraisercomment.comments.addCreatedDate(builder, createdDateOffset);
  Appraisercomment.comments.addUpdatedDate(builder, updatedDateOffset);
  Appraisercomment.comments.addSaveAsDraft(builder, saveAsDraft);
  Appraisercomment.comments.addFilepath(builder, filepathOffset);
  return Appraisercomment.comments.endcomments(builder);
}

// Exports for Node.js and RequireJS
this.Appraisercomment = Appraisercomment;
