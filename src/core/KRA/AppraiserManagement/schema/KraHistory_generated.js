// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var kraUserHistory = kraUserHistory || {};

/**
 * @constructor
 */
kraUserHistory.kraHistory = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {kraUserHistory.kraHistory}
 */
kraUserHistory.kraHistory.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {kraUserHistory.kraHistory=} obj
 * @returns {kraUserHistory.kraHistory}
 */
kraUserHistory.kraHistory.getRootAskraHistory = function(bb, obj) {
  return (obj || new kraUserHistory.kraHistory).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {number} index
 * @param {kraUserHistory.current=} obj
 * @returns {kraUserHistory.current}
 */
kraUserHistory.kraHistory.prototype.current = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? (obj || new kraUserHistory.current).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
kraUserHistory.kraHistory.prototype.currentLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {number} index
 * @param {kraUserHistory.history=} obj
 * @returns {kraUserHistory.history}
 */
kraUserHistory.kraHistory.prototype.historyList = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? (obj || new kraUserHistory.history).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
kraUserHistory.kraHistory.prototype.historyListLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
kraUserHistory.kraHistory.startkraHistory = function(builder) {
  builder.startObject(2);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} currentOffset
 */
kraUserHistory.kraHistory.addCurrent = function(builder, currentOffset) {
  builder.addFieldOffset(0, currentOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.kraHistory.createCurrentVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
kraUserHistory.kraHistory.startCurrentVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} historyListOffset
 */
kraUserHistory.kraHistory.addHistoryList = function(builder, historyListOffset) {
  builder.addFieldOffset(1, historyListOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.kraHistory.createHistoryListVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
kraUserHistory.kraHistory.startHistoryListVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.kraHistory.endkraHistory = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} offset
 */
kraUserHistory.kraHistory.finishkraHistoryBuffer = function(builder, offset) {
  builder.finish(offset);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} currentOffset
 * @param {flatbuffers.Offset} historyListOffset
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.kraHistory.createkraHistory = function(builder, currentOffset, historyListOffset) {
  kraUserHistory.kraHistory.startkraHistory(builder);
  kraUserHistory.kraHistory.addCurrent(builder, currentOffset);
  kraUserHistory.kraHistory.addHistoryList(builder, historyListOffset);
  return kraUserHistory.kraHistory.endkraHistory(builder);
}

/**
 * @constructor
 */
kraUserHistory.current = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {kraUserHistory.current}
 */
kraUserHistory.current.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {kraUserHistory.current=} obj
 * @returns {kraUserHistory.current}
 */
kraUserHistory.current.getRootAscurrent = function(bb, obj) {
  return (obj || new kraUserHistory.current).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.current.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.current.prototype.userId = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.current.prototype.startDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.current.prototype.endDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.current.prototype.updatedUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.current.prototype.createdUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 14);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {boolean}
 */
kraUserHistory.current.prototype.status = function() {
  var offset = this.bb.__offset(this.bb_pos, 16);
  return offset ? !!this.bb.readInt8(this.bb_pos + offset) : false;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.current.prototype.appraiserId = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 18);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.current.prototype.appraiserName = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 20);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.current.prototype.appraisalYear = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 22);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Builder} builder
 */
kraUserHistory.current.startcurrent = function(builder) {
  builder.startObject(10);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
kraUserHistory.current.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} userId
 */
kraUserHistory.current.addUserId = function(builder, userId) {
  builder.addFieldInt64(1, userId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} startDateOffset
 */
kraUserHistory.current.addStartDate = function(builder, startDateOffset) {
  builder.addFieldOffset(2, startDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} endDateOffset
 */
kraUserHistory.current.addEndDate = function(builder, endDateOffset) {
  builder.addFieldOffset(3, endDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} updatedUserId
 */
kraUserHistory.current.addUpdatedUserId = function(builder, updatedUserId) {
  builder.addFieldInt64(4, updatedUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} createdUserId
 */
kraUserHistory.current.addCreatedUserId = function(builder, createdUserId) {
  builder.addFieldInt64(5, createdUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {boolean} status
 */
kraUserHistory.current.addStatus = function(builder, status) {
  builder.addFieldInt8(6, +status, +false);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraiserIdOffset
 */
kraUserHistory.current.addAppraiserId = function(builder, appraiserIdOffset) {
  builder.addFieldOffset(7, appraiserIdOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraiserNameOffset
 */
kraUserHistory.current.addAppraiserName = function(builder, appraiserNameOffset) {
  builder.addFieldOffset(8, appraiserNameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraisalYearOffset
 */
kraUserHistory.current.addAppraisalYear = function(builder, appraisalYearOffset) {
  builder.addFieldOffset(9, appraisalYearOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.current.endcurrent = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Long} userId
 * @param {flatbuffers.Offset} startDateOffset
 * @param {flatbuffers.Offset} endDateOffset
 * @param {flatbuffers.Long} updatedUserId
 * @param {flatbuffers.Long} createdUserId
 * @param {boolean} status
 * @param {flatbuffers.Offset} appraiserIdOffset
 * @param {flatbuffers.Offset} appraiserNameOffset
 * @param {flatbuffers.Offset} appraisalYearOffset
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.current.createcurrent = function(builder, id, userId, startDateOffset, endDateOffset, updatedUserId, createdUserId, status, appraiserIdOffset, appraiserNameOffset, appraisalYearOffset) {
  kraUserHistory.current.startcurrent(builder);
  kraUserHistory.current.addId(builder, id);
  kraUserHistory.current.addUserId(builder, userId);
  kraUserHistory.current.addStartDate(builder, startDateOffset);
  kraUserHistory.current.addEndDate(builder, endDateOffset);
  kraUserHistory.current.addUpdatedUserId(builder, updatedUserId);
  kraUserHistory.current.addCreatedUserId(builder, createdUserId);
  kraUserHistory.current.addStatus(builder, status);
  kraUserHistory.current.addAppraiserId(builder, appraiserIdOffset);
  kraUserHistory.current.addAppraiserName(builder, appraiserNameOffset);
  kraUserHistory.current.addAppraisalYear(builder, appraisalYearOffset);
  return kraUserHistory.current.endcurrent(builder);
}

/**
 * @constructor
 */
kraUserHistory.history = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {kraUserHistory.history}
 */
kraUserHistory.history.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {kraUserHistory.history=} obj
 * @returns {kraUserHistory.history}
 */
kraUserHistory.history.getRootAshistory = function(bb, obj) {
  return (obj || new kraUserHistory.history).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.history.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.history.prototype.userId = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.history.prototype.startDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.history.prototype.endDate = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.history.prototype.updatedUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
kraUserHistory.history.prototype.createdUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 14);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {boolean}
 */
kraUserHistory.history.prototype.status = function() {
  var offset = this.bb.__offset(this.bb_pos, 16);
  return offset ? !!this.bb.readInt8(this.bb_pos + offset) : false;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.history.prototype.appraiserId = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 18);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.history.prototype.appraiserName = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 20);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
kraUserHistory.history.prototype.appraisalYear = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 22);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Builder} builder
 */
kraUserHistory.history.starthistory = function(builder) {
  builder.startObject(10);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
kraUserHistory.history.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} userId
 */
kraUserHistory.history.addUserId = function(builder, userId) {
  builder.addFieldInt64(1, userId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} startDateOffset
 */
kraUserHistory.history.addStartDate = function(builder, startDateOffset) {
  builder.addFieldOffset(2, startDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} endDateOffset
 */
kraUserHistory.history.addEndDate = function(builder, endDateOffset) {
  builder.addFieldOffset(3, endDateOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} updatedUserId
 */
kraUserHistory.history.addUpdatedUserId = function(builder, updatedUserId) {
  builder.addFieldInt64(4, updatedUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} createdUserId
 */
kraUserHistory.history.addCreatedUserId = function(builder, createdUserId) {
  builder.addFieldInt64(5, createdUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {boolean} status
 */
kraUserHistory.history.addStatus = function(builder, status) {
  builder.addFieldInt8(6, +status, +false);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraiserIdOffset
 */
kraUserHistory.history.addAppraiserId = function(builder, appraiserIdOffset) {
  builder.addFieldOffset(7, appraiserIdOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraiserNameOffset
 */
kraUserHistory.history.addAppraiserName = function(builder, appraiserNameOffset) {
  builder.addFieldOffset(8, appraiserNameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} appraisalYearOffset
 */
kraUserHistory.history.addAppraisalYear = function(builder, appraisalYearOffset) {
  builder.addFieldOffset(9, appraisalYearOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.history.endhistory = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Long} userId
 * @param {flatbuffers.Offset} startDateOffset
 * @param {flatbuffers.Offset} endDateOffset
 * @param {flatbuffers.Long} updatedUserId
 * @param {flatbuffers.Long} createdUserId
 * @param {boolean} status
 * @param {flatbuffers.Offset} appraiserIdOffset
 * @param {flatbuffers.Offset} appraiserNameOffset
 * @param {flatbuffers.Offset} appraisalYearOffset
 * @returns {flatbuffers.Offset}
 */
kraUserHistory.history.createhistory = function(builder, id, userId, startDateOffset, endDateOffset, updatedUserId, createdUserId, status, appraiserIdOffset, appraiserNameOffset, appraisalYearOffset) {
  kraUserHistory.history.starthistory(builder);
  kraUserHistory.history.addId(builder, id);
  kraUserHistory.history.addUserId(builder, userId);
  kraUserHistory.history.addStartDate(builder, startDateOffset);
  kraUserHistory.history.addEndDate(builder, endDateOffset);
  kraUserHistory.history.addUpdatedUserId(builder, updatedUserId);
  kraUserHistory.history.addCreatedUserId(builder, createdUserId);
  kraUserHistory.history.addStatus(builder, status);
  kraUserHistory.history.addAppraiserId(builder, appraiserIdOffset);
  kraUserHistory.history.addAppraiserName(builder, appraiserNameOffset);
  kraUserHistory.history.addAppraisalYear(builder, appraisalYearOffset);
  return kraUserHistory.history.endhistory(builder);
}

// Exports for Node.js and RequireJS
this.kraUserHistory = kraUserHistory;
