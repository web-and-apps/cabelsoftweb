/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import { Reducer } from 'redux';
import DesiredSkillSetState from '../state/DesiredSkillSetState';
import DesiredSkillSetActionTypeKeys from '../actions/DesiredSkillSetActionTypeKeys';
import DesiredSkillSetActionTypes from '../actions/DesiredSkillSetActionTypes';


const initialState: DesiredSkillSetState = {
    skillSetList:[],
    searchedSkill:[],
    showLoader: false
};

const DesiredSkillSetReducers: Reducer<DesiredSkillSetState, DesiredSkillSetActionTypes> = (state = initialState, action) => {
    switch (action.type) {

        case DesiredSkillSetActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoading

            };
        }
       
        case DesiredSkillSetActionTypeKeys.ADD_SKILLSET: {
            return {
                ...state,
                skillSetList:action.value,
                text: action.text
            };
        }
        case DesiredSkillSetActionTypeKeys.SEARCH_SKILL_SET: {
            return {
                ...state,
                searchedSkill:action.value,
                text: action.text
            };
        }
        
       
        default:
            return state;
    }
};

export default DesiredSkillSetReducers;