/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

 
import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import DesiredSkillSetActionTypeKeys from './DesiredSkillSetActionTypeKeys';
import DesiredSkillSetActionTypes from './DesiredSkillSetActionTypes';
import DesiredSkillSetState from '../state/DesiredSkillSetState';
import { APIUserManagement } from '../../../api/APIUsersManagement';


export const addSkill = (data:any): DesiredSkillSetActionTypes => {
    return {
        type: DesiredSkillSetActionTypeKeys.ADD_SKILLSET,
        value:data, 
        text: "Success"
    };
}
export const showLoader = (data:Boolean): DesiredSkillSetActionTypes => {
    return {
      type: DesiredSkillSetActionTypeKeys.SHOW_LOADER,
      showLoading: data
    }
  }
export const searchSkill = (data:any): DesiredSkillSetActionTypes => {
    return {
        type: DesiredSkillSetActionTypeKeys.SEARCH_SKILL_SET,
        value:data, 
        text: "Success"
    };
}




export const deleteSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    DesiredSkillSetState,
    null,
    DesiredSkillSetActionTypes
    >
> = (id: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.deleteDesiredSkillSet(id).then((response) => {
            getSkillSetlist()
            .then((response) => {
                dispatch(showLoader(false));
                console.log(response)
                dispatch(addSkill(response));
            })
            .catch((error) => {
                dispatch(showLoader(false));
                console.log(error)
            })
            
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
export const addSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    DesiredSkillSetState,
    null,
    DesiredSkillSetActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./desiredSkillSet').desiredskillset;
        var addProfileData = addProfile.desiredskillset
        var fbb = new flatbuffers.Builder(0);
        var refId = fbb.createLong("");
        var id = fbb.createLong(param.id);
        var createdUserId = fbb.createLong(param.createdUserId);
        addProfileData.startdesiredskillset(fbb)
        addProfileData.addId(fbb, refId)
        addProfileData.addSkillsetId(fbb, id)
        addProfileData.addCreatedUserId(fbb, createdUserId)
        var ints = addProfileData.enddesiredskillset(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.addDesiredSkillSet(value).then((response) => {
            getSkillSetlist()
            .then((response) => {
                dispatch(showLoader(false));
                console.log(response)
                dispatch(addSkill(response));
            })
            .catch((error) => {
                dispatch(showLoader(false));
                console.log(error)
            })
            
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
const getSkillSetlist = (): Promise<any> => {
    return new Promise((resolve, reject) => {
        const service = new APIUserManagement();
        return service.getDesiredSkillSetList("").then((response) => {
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./DesiredSkillSetList').com.vthink.skillset.table;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                skilllist.push(obj)
            }
            console.log("finalArray",skilllist)
            resolve(skilllist);
        }).catch((error) => {
            reject(error)
        });
    })
}

export const searchSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    DesiredSkillSetState,
    null,
    DesiredSkillSetActionTypes
    >
> = (searchtext:string,start:String,limit:String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getSkillSetList(searchtext,"","").then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./DesiredSkillSetList').com.vthink.skillset.table;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                skilllist.push(obj)
            }
            console.log("finalArray",skilllist)
            dispatch(searchSkill(skilllist));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}

export const getDesiredSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    DesiredSkillSetState,
    null,
    DesiredSkillSetActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getDesiredSkillSetList(searchtext).then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./DesiredSkillSetList').com.vthink.skillset.table;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                skilllist.push(obj)
            }
            console.log("finalArray",skilllist)
            dispatch(addSkill(skilllist));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}