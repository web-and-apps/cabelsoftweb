// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var com = com || {};

/**
 * @const
 * @namespace
 */
com.vthink = com.vthink || {};

/**
 * @const
 * @namespace
 */
com.vthink.skillset = com.vthink.skillset || {};

/**
 * @const
 * @namespace
 */
com.vthink.skillset.table = com.vthink.skillset.table || {};

/**
 * @constructor
 */
com.vthink.skillset.table.skillsetlist = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {com.vthink.skillset.table.skillsetlist}
 */
com.vthink.skillset.table.skillsetlist.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {com.vthink.skillset.table.skillsetlist=} obj
 * @returns {com.vthink.skillset.table.skillsetlist}
 */
com.vthink.skillset.table.skillsetlist.getRootAsskillsetlist = function(bb, obj) {
  return (obj || new com.vthink.skillset.table.skillsetlist).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {number} index
 * @param {com.vthink.skillset.table.skillset=} obj
 * @returns {com.vthink.skillset.table.skillset}
 */
com.vthink.skillset.table.skillsetlist.prototype.skillset = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? (obj || new com.vthink.skillset.table.skillset).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
com.vthink.skillset.table.skillsetlist.prototype.skillsetLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
com.vthink.skillset.table.skillsetlist.startskillsetlist = function(builder) {
  builder.startObject(1);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillsetOffset
 */
com.vthink.skillset.table.skillsetlist.addSkillset = function(builder, skillsetOffset) {
  builder.addFieldOffset(0, skillsetOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
com.vthink.skillset.table.skillsetlist.createSkillsetVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
com.vthink.skillset.table.skillsetlist.startSkillsetVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
com.vthink.skillset.table.skillsetlist.endskillsetlist = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} offset
 */
com.vthink.skillset.table.skillsetlist.finishskillsetlistBuffer = function(builder, offset) {
  builder.finish(offset);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillsetOffset
 * @returns {flatbuffers.Offset}
 */
com.vthink.skillset.table.skillsetlist.createskillsetlist = function(builder, skillsetOffset) {
  com.vthink.skillset.table.skillsetlist.startskillsetlist(builder);
  com.vthink.skillset.table.skillsetlist.addSkillset(builder, skillsetOffset);
  return com.vthink.skillset.table.skillsetlist.endskillsetlist(builder);
}

/**
 * @constructor
 */
com.vthink.skillset.table.skillset = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {com.vthink.skillset.table.skillset}
 */
com.vthink.skillset.table.skillset.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {com.vthink.skillset.table.skillset=} obj
 * @returns {com.vthink.skillset.table.skillset}
 */
com.vthink.skillset.table.skillset.getRootAsskillset = function(bb, obj) {
  return (obj || new com.vthink.skillset.table.skillset).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
com.vthink.skillset.table.skillset.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
com.vthink.skillset.table.skillset.prototype.skillName = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
com.vthink.skillset.table.skillset.prototype.createdUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
com.vthink.skillset.table.skillset.prototype.updatedUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 */
com.vthink.skillset.table.skillset.startskillset = function(builder) {
  builder.startObject(4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
com.vthink.skillset.table.skillset.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillNameOffset
 */
com.vthink.skillset.table.skillset.addSkillName = function(builder, skillNameOffset) {
  builder.addFieldOffset(1, skillNameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} createdUserId
 */
com.vthink.skillset.table.skillset.addCreatedUserId = function(builder, createdUserId) {
  builder.addFieldInt64(2, createdUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} updatedUserId
 */
com.vthink.skillset.table.skillset.addUpdatedUserId = function(builder, updatedUserId) {
  builder.addFieldInt64(3, updatedUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
com.vthink.skillset.table.skillset.endskillset = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Offset} skillNameOffset
 * @param {flatbuffers.Long} createdUserId
 * @param {flatbuffers.Long} updatedUserId
 * @returns {flatbuffers.Offset}
 */
com.vthink.skillset.table.skillset.createskillset = function(builder, id, skillNameOffset, createdUserId, updatedUserId) {
  com.vthink.skillset.table.skillset.startskillset(builder);
  com.vthink.skillset.table.skillset.addId(builder, id);
  com.vthink.skillset.table.skillset.addSkillName(builder, skillNameOffset);
  com.vthink.skillset.table.skillset.addCreatedUserId(builder, createdUserId);
  com.vthink.skillset.table.skillset.addUpdatedUserId(builder, updatedUserId);
  return com.vthink.skillset.table.skillset.endskillset(builder);
}

// Exports for Node.js and RequireJS
this.com = com;
