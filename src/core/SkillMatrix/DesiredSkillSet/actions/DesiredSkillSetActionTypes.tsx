/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import {
    IAddSkillSet,
    ISearchSkillSet,
    IShowLoader
} from "./DesiredSkillSetActionInterface";


type DesiredSkillSetActionTypes =
     |IAddSkillSet
     |ISearchSkillSet
     |IShowLoader
   
export default DesiredSkillSetActionTypes;