/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.43.115:8080/sakthivel)
 */


enum DesiredSkillSetActionTypeKeys {
    ADD_SKILLSET = "ADD_SKILLSET",
    SEARCH_SKILL_SET = "SEARCH_SKILL_SET",
    SHOW_LOADER = "SHOW_LOADER"
}

export default DesiredSkillSetActionTypeKeys;