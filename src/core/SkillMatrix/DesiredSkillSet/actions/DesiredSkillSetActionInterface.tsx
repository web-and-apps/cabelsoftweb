/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import keys from "./DesiredSkillSetActionTypeKeys";


export interface IAddSkillSet {
    readonly type: keys.ADD_SKILLSET,
    value:any,
    text: any
}
export interface ISearchSkillSet {
    readonly type: keys.SEARCH_SKILL_SET,
    value:any,
    text: any
}
export interface IShowLoader {
    type: keys.SHOW_LOADER,
    showLoading: Boolean,
}


