/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import FilterResourceManagementState from '../state/FilterResourceManagementState';
import FilterResourceManagementActionTypes from './FilterResourceManagementActionTypes';
import FilterResourceManagementActionTypeKeys from './FilterResourceManagementActionTypeKeys';
import { APIUserManagement } from '../../../api/APIUsersManagement';
import { TagModel } from '../Models/TagModel';

export const showPopup = (data:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SHOW_POPUP,
        showPopup: data,
        text: "Success"
    };
}
export const showSkillSetDropDown = (data:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SHOW_SKILLSET_DROPDOWN,
        showPopup: data,
    };
}
export const showEmployeeListDropDown = (data:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SHOW_EMPLOYEELIST_DROPDOWN,
        showPopup: data,
    };
}
export const showFilterResult = (data:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SHOW_FILTER_RESULT,
        showPopup: data
    };
}
export const searchEmployee = (data:any,render:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SEARCH_EMPLOYEE_LIST,
        value:data,
        render:render
    };
}

export const showLoader = (data:Boolean): FilterResourceManagementActionTypes => {
    return {
      type: FilterResourceManagementActionTypeKeys.SHOW_LOADER,
      showLoading: data
    }
  }
  export const searchSkill = (data:any,render:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.SEARCH_SKILL_SET,
        value:data,
        render:render
    };
}
export const clearData = (empdata:any,skillData:any,render:Boolean): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.CLEAR_ALL,
        empdata:empdata,
        SkillData:skillData,
        render:render
    };
}
export const empDetails = (yeardata:any,skillData:any): FilterResourceManagementActionTypes => {
    return {
        type: FilterResourceManagementActionTypeKeys.CLICK_CARD,
        yearDetails:yeardata,
        tableDetails:skillData
    };
}
export const actionShowPopup: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopup(!data));
        }).catch((error) => {
            dispatch(showPopup(data));
        });
    }
}

export const actionemployeeDropDown: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showEmployeeListDropDown(!data));
        }).catch((error) => {
            dispatch(showEmployeeListDropDown(data));
        });
    }
}

export const actionSkillSetDropDown: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showSkillSetDropDown(!data));
        }).catch((error) => {
            dispatch(showSkillSetDropDown(data));
        });
    }
}
export const actionShowPopFilterResultPopUp: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showFilterResult(!data));
        }).catch((error) => {
            dispatch(showFilterResult(data));
        });
    }
}
export const searchUser: ActionCreator<
    ThunkAction<
    Promise<any>,
    FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.searchUserList(searchtext).then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('../../EmployeeSkillSetDetailsContainer/actions/userList').users;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.users.getRootAsusers(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.usersLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.users(i).id().toFloat64();
                obj.title = Value.users(i).title();
                obj.first_name = Value.users(i).firstName();
                obj.last_name = Value.users(i).lastName();
                obj.emp_id = Value.users(i).empId();
                obj.emp_photo_url = Value.users(i).empPhotoUrl();
                obj.status = Value.users(i).status();
                obj.email_id = Value.users(i).emailId();
                obj.appraiser_id = Value.users(i).appraiserId().toFloat64();
                obj.created_user_id = Value.users(i).createdUserId().toFloat64();
                obj.updated_user_id = Value.users(i).updatedUserId().toFloat64();
                obj.role_id = Value.users(i).roleId();
                obj.empLevel = Value.users(i).empLevel();
                obj.text = Value.users(i).empId()+" - "+Value.users(i).firstName()+" "+Value.users(i).lastName()
                obj.isActive = false
                empList.push(obj)
            }
            if (searchtext === "") {
            localStorage.setItem("empList", JSON.stringify(empList));
            }
            dispatch(searchEmployee(empList,false));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}
export const searchSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    FilterResourceManagementState,
    null,
    FilterResourceManagementActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getSkillSetList(searchtext,"","").then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('../../DesiredSkillSet/actions/DesiredSkillSetList').com.vthink.skillset.table;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                obj.text = Value.skillset(i).skillName().toString();
                obj.isActive = false
                skilllist.push(obj)
            }
            if (searchtext === "") {
            localStorage.setItem("skilllist", JSON.stringify(skilllist));
            }
            dispatch(searchSkill(skilllist,false));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}
export const getFilterResults: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('../../SkillSetDetailContainer/actions/SkillSetDetailsRequest').report;
        var addProfileData = addProfile.report;
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createString(data.userId);
        var addPreviousYearExperience = fbb.createLong(data.prevExp);
        var addVthinkExperience = fbb.createLong(data.vthinkExp);
        var addTotalExperience = fbb.createLong(data.totalExp);
        var addSkillMatrixId = fbb.createString(data.skillId);
        addProfileData.startreport(fbb);
        addProfileData.addUserId(fbb, id);
        addProfileData.addPreviousYearExperience(fbb, addPreviousYearExperience);
        addProfileData.addVthinkExperience(fbb, addVthinkExperience);
        addProfileData.addTotalExperience(fbb, addTotalExperience);
        addProfileData.addSkillMatrixId(fbb, addSkillMatrixId);
        var ints = addProfileData.endreport(fbb);
        fbb.finish(ints);
        var value = fbb.asUint8Array();
        const service = new APIUserManagement();
        return service.getFilterResults(value).then((response) => {
            dispatch(showLoader(false));
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
export const getCardDetails: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (data: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('../../SkillSetDetailContainer/actions/SkillSetDetailsRequest').report;
        var addProfileData = addProfile.report;
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createString(data.userId);
        var addPreviousYearExperience = fbb.createLong(data.prevExp);
        var addVthinkExperience = fbb.createLong(data.vthinkExp);
        var addTotalExperience = fbb.createLong(data.totalExp);
        var addSkillMatrixId = fbb.createString(data.skillId);
        addProfileData.startreport(fbb);
        addProfileData.addUserId(fbb, id);
        addProfileData.addPreviousYearExperience(fbb, addPreviousYearExperience);
        addProfileData.addVthinkExperience(fbb, addVthinkExperience);
        addProfileData.addTotalExperience(fbb, addTotalExperience);
        addProfileData.addSkillMatrixId(fbb, addSkillMatrixId);
        var ints = addProfileData.endreport(fbb);
        fbb.finish(ints);
        var value = fbb.asUint8Array();
        const service = new APIUserManagement();
        return service.getSkillSetDetails(value).then((response) => {
            dispatch(showLoader(false));
            console.log(response.value);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('../../SkillSetDetailContainer/actions/SkillSetDetails').detailres;
            var responseArray = new Uint8Array(response.value);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.detailres.getRootAsdetailres(byteBuffer);
            let Skillobj: any = {}
            Skillobj.id = Value.id().toFloat64()
            Skillobj.userName = Value.userName()
            Skillobj.previousYearExperience = Value.previousYearExperience().toFloat64()
            Skillobj.vthinkExperience = Value.vthinkExperience().toFloat64()
            Skillobj.totalExperience = Value.totalExperience().toFloat64()
            var skillLength = Value.skillDetailsLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillDetails(i).id().toFloat64();
                obj.skillname = Value.skillDetails(i).skillname();
                obj.exp = Value.skillDetails(i).exp().toFloat64();
                obj.status = Value.skillDetails(i).status();
                obj.lastUpdated = Value.skillDetails(i).lastUpdated();
                if (obj.status === "Approved"){
                obj.Approved = true
                }else{
                obj.Approved = false   
                }
                empList.push(obj)
            }
            console.log("finalArray",Skillobj)
            dispatch(empDetails(Skillobj,empList));
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}

export const selectUnSelectInLocalDataEmployeeList: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (singleSelectionData:any,profiledata:any,render) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            var employeeArray: any
    if (localStorage.getItem('empList') !== "" || localStorage.getItem('empList') !== null) {
        employeeArray = JSON.parse(localStorage.getItem('empList')!!)
    }
    if (employeeArray.length > 0) {   
            for (let i = 0; i < employeeArray.length; i++) {
                if (singleSelectionData.emp_id === employeeArray[i].emp_id){
                   employeeArray[i].isActive = !singleSelectionData.isActive
                }
            } 
            for (let i = 0; i < profiledata.length; i++) {
                if (singleSelectionData.emp_id === profiledata[i].emp_id){
                    profiledata[i].isActive = !singleSelectionData.isActive
                }
            }
        localStorage.setItem("empList", JSON.stringify(employeeArray));
    } 
            dispatch(searchEmployee(profiledata,!render));
        }).catch((error) => {
            dispatch(searchEmployee(profiledata,render));
        });
    }
}
export const selectUnSelectInLocalDataSkillList: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (singleSelectionData:any,skillData:any,render:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
           // Change the value in local Array
    var employeeArray: any
    if (localStorage.getItem('skilllist') !== "" || localStorage.getItem('skilllist') !== null) {
        employeeArray = JSON.parse(localStorage.getItem('skilllist')!!)
    }
    if (employeeArray.length > 0) {   
            for (let i = 0; i < employeeArray.length; i++) {
                if (singleSelectionData.id === employeeArray[i].id){
                   employeeArray[i].isActive = !singleSelectionData.isActive
                }
            } 
            for (let i = 0; i < skillData.length; i++) {
                if (singleSelectionData.id === skillData[i].id){
                    skillData[i].isActive = !singleSelectionData.isActive
                }
            }
        localStorage.setItem("skilllist", JSON.stringify(employeeArray));
    } 
            dispatch(searchSkill(skillData,!render));
        }).catch((error) => {
            dispatch(searchSkill(skillData,render));
        });
    }
}

export const clearAll: ActionCreator<
    ThunkAction<
        Promise<any>,
        FilterResourceManagementState,
        null,
        FilterResourceManagementActionTypes
    >
> = (emplData: any, skillData: any, render: Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            // Change the value in local Array
            var employeeArray: any
            if (localStorage.getItem('empList') !== "" || localStorage.getItem('empList') !== null) {
                employeeArray = JSON.parse(localStorage.getItem('empList')!!)
            }
            var skillArray: any
            if (localStorage.getItem('skilllist') !== "" || localStorage.getItem('skilllist') !== null) {
                skillArray = JSON.parse(localStorage.getItem('skilllist')!!)
            }

            for (let i = 0; i < emplData.length; i++) {
                emplData[i].isActive = false
            }
            for (let i = 0; i < employeeArray.length; i++) {
                employeeArray[i].isActive = false
            }
            for (let i = 0; i < skillData.length; i++) {
                skillData[i].isActive = false
            }
            for (let i = 0; i < skillArray.length; i++) {
                skillArray[i].isActive = false
            }
            localStorage.setItem("empList", JSON.stringify(employeeArray));
            localStorage.setItem("skilllist", JSON.stringify(employeeArray));
            dispatch(clearData(emplData,skillData, !render));
        }).catch((error) => {
            dispatch(searchSkill(skillData, render));
        });
    }
}