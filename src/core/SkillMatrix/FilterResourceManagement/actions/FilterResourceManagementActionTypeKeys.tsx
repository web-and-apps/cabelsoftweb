/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */


export enum FilterResourceManagementActionTypeStates {
    SHOW = "_SHOW",
    HIDE = "_HIDE"
}

enum FilterResourceManagementActionTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    SHOW_FILTER_RESULT = "SHOW_FILTER_RESULT",
    SHOW_SKILLSET_DROPDOWN = "SHOW_SKILLSET_DROPDOWN",
    SHOW_EMPLOYEELIST_DROPDOWN = "SHOW_EMPLOYEELIST_DROPDOWN",
    SEARCH_EMPLOYEE_LIST = "SEARCH_EMPLOYEE_LIST",
    SHOW_LOADER = "SHOW_LOADER",
    SEARCH_SKILL_SET = "SEARCH_SKILL_SET",
    CLEAR_ALL = "CLEAR_ALL",
    CLICK_CARD = "CLICK_CARD",
}

export default FilterResourceManagementActionTypeKeys;