/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import {
    IShowAction,
    IShowFilterPopup,
    IShowEmployeeListDropDown,
    IShowSkillsetDropDown,
    IEmployeeList,
    IShowLoader,
    ISearchSkillSet,
    IClearAll,
    IClickCard

} from "./FilterResourceManagementInterface";


type FilterResourceManagementActionTypes =  
    | IShowAction
    | IShowFilterPopup
    | IShowEmployeeListDropDown
    | IShowSkillsetDropDown
    | IEmployeeList
    | IShowLoader
    | ISearchSkillSet
    | IClearAll
    | IClickCard


export default FilterResourceManagementActionTypes;