/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import keys from "./FilterResourceManagementActionTypeKeys";

export interface IShowAction {
    readonly type: keys.SHOW_POPUP,
    showPopup: Boolean,
    text: any
}
export interface IShowFilterPopup {
    readonly type: keys.SHOW_FILTER_RESULT,
    showPopup: Boolean,
}
export interface IShowSkillsetDropDown {
    readonly type: keys.SHOW_SKILLSET_DROPDOWN,
    showPopup: Boolean,
}
export interface IShowEmployeeListDropDown {
    readonly type: keys.SHOW_EMPLOYEELIST_DROPDOWN,
    showPopup: Boolean,
}
export interface IEmployeeList {
    readonly type: keys.SEARCH_EMPLOYEE_LIST,
    value: any,
    render:Boolean
}
export interface IShowLoader {
    type: keys.SHOW_LOADER,
    showLoading: Boolean,
}
export interface ISearchSkillSet {
    readonly type: keys.SEARCH_SKILL_SET,
    value:any,
    render:Boolean
}
export interface IClearAll {
    readonly type: keys.CLEAR_ALL,
    empdata:any,
    SkillData:any,
    render:Boolean
}
export interface IClickCard {
    readonly type: keys.CLICK_CARD,
    yearDetails:any,
    tableDetails:any
}
