/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import FilterResourceManagementState from '../state/FilterResourceManagementState';
import FilterResourceManagementActionTypes from '../actions/FilterResourceManagementActionTypes';
import FilterResourceManagementActionTypeKeys from '../actions/FilterResourceManagementActionTypeKeys';

const initialState: FilterResourceManagementState = {
    showPopup: false,
    showFilterResult:false,
    showSkillSet: false,
    showEmployeeListDropDown:false,
    employeeList:[],
    showLoader: false,
    searchedSkill:[],
    rerender:false,
    yearData:"",
    tableData:[]
};

const FilterResourceManagementReducers: Reducer<FilterResourceManagementState, FilterResourceManagementActionTypes> = (state = initialState, action) => {
    switch (action.type) {
        case FilterResourceManagementActionTypeKeys.SHOW_POPUP: {
            return {
                ...state,
                showPopup: action.showPopup,
                text: action.text,
                showTagList: false,
                clearSearch:false
            };
        }
        case FilterResourceManagementActionTypeKeys.SHOW_FILTER_RESULT: {
            return {
                ...state,
                showFilterResult: action.showPopup,
            };
        }
        case FilterResourceManagementActionTypeKeys.SHOW_EMPLOYEELIST_DROPDOWN: {
            return {
                ...state,
                showEmployeeListDropDown: action.showPopup,
            };
        }
        case FilterResourceManagementActionTypeKeys.SHOW_SKILLSET_DROPDOWN: {
            return {
                ...state,
                showSkillSet: action.showPopup,
            };
        }
        case FilterResourceManagementActionTypeKeys.SEARCH_EMPLOYEE_LIST: {
            return {
                ...state,
                employeeList: action.value,
                rerender: action.render
            };
        }
        case FilterResourceManagementActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoading

            };
        }
        case FilterResourceManagementActionTypeKeys.SEARCH_SKILL_SET: {
            return {
                ...state,
                searchedSkill:action.value,
                rerender: action.render
            };
        }
        case FilterResourceManagementActionTypeKeys.CLEAR_ALL: {
            return {
                ...state,
                searchedSkill:action.SkillData,
                employeeList:action.empdata,
                showSkillSet:false,
                showEmployeeListDropDown:false,
                rerender: action.render
            };
        }
        case FilterResourceManagementActionTypeKeys.CLICK_CARD: {
            return {
                ...state,
                yearData:action.yearDetails,
                tableData:action.tableDetails,
                showFilterResult:true
            };
        }
        
       
        default:
            return state;
    }
};

export default FilterResourceManagementReducers;