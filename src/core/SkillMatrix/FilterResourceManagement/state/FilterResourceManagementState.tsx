/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */
import { KRAGoalModels } from "../../../../components/KRAGoal/KRAGoalComponents";
export default interface FilterResourceManagementState {
    showPopup: Boolean;
    showFilterResult:Boolean;
    showSkillSet: Boolean;
    employeeList:any,
    showEmployeeListDropDown:Boolean;
    showLoader:Boolean;
    searchedSkill:any;
    rerender:Boolean;
    yearData:any,
    tableData:[]
}