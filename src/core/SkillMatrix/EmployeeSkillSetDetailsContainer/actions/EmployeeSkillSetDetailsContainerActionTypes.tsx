/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import {
    ISearchEmployee,
    IShowLoader
} from "./EmployeeSkillSetDetailsContainerActionInterface";


type EmployeeSkillSetDetailsContainerActionActionTypes =
     |ISearchEmployee
     |IShowLoader
   
export default EmployeeSkillSetDetailsContainerActionActionTypes;