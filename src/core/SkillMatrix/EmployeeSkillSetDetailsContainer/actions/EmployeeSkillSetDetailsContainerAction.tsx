/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

 
import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import EmployeeSkillSetDetailsContainerActionTypeKeys from './EmployeeSkillSetDetailsContainerActionTypeKeys';
import EmployeeSkillSetDetailsContainerActionTypes from './EmployeeSkillSetDetailsContainerActionTypes';
import EmployeeSkillSetDetailsContainerState from '../state/EmployeeSkillSetDetailsContainerState';
import { APIUserManagement } from '../../../api/APIUsersManagement';

export const showLoader = (data:Boolean): EmployeeSkillSetDetailsContainerActionTypes => {
    return {
      type: EmployeeSkillSetDetailsContainerActionTypeKeys.SHOW_LOADER,
      showLoading: data
    }
  }
export const searchEmployee = (data:any): EmployeeSkillSetDetailsContainerActionTypes => {
    return {
        type: EmployeeSkillSetDetailsContainerActionTypeKeys.SEARCH_EMPLOYEE_LIST,
        value:data, 
        text: "Success"
    };
}

export const searchSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    EmployeeSkillSetDetailsContainerState,
    null,
    EmployeeSkillSetDetailsContainerActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getuserList().then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./userList').users;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.users.getRootAsusers(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.usersLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.users(i).id().toFloat64();
                obj.title = Value.users(i).title();
                obj.first_name = Value.users(i).firstName();
                obj.last_name = Value.users(i).lastName();
                obj.emp_id = Value.users(i).empId();
                obj.emp_photo_url = Value.users(i).empPhotoUrl();
                obj.status = Value.users(i).status();
                obj.email_id = Value.users(i).emailId();
                obj.appraiser_id = Value.users(i).appraiserId().toFloat64();
                obj.created_user_id = Value.users(i).createdUserId().toFloat64();
                obj.updated_user_id = Value.users(i).updatedUserId().toFloat64();
                obj.role_id = Value.users(i).roleId();
                obj.empLevel = Value.users(i).empLevel();
                obj.isSelected = false;
                empList.push(obj)
            }
            console.log("finalArray",empList)
            dispatch(searchEmployee(empList));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}


export const searchUser: ActionCreator<
    ThunkAction<
    Promise<any>,
    EmployeeSkillSetDetailsContainerState,
    null,
    EmployeeSkillSetDetailsContainerActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.searchUserList(searchtext).then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./userList').users;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.users.getRootAsusers(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.usersLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.users(i).id().toFloat64();
                obj.title = Value.users(i).title();
                obj.first_name = Value.users(i).firstName();
                obj.last_name = Value.users(i).lastName();
                obj.emp_id = Value.users(i).empId();
                obj.emp_photo_url = Value.users(i).empPhotoUrl();
                obj.status = Value.users(i).status();
                obj.email_id = Value.users(i).emailId();
                obj.appraiser_id = Value.users(i).appraiserId().toFloat64();
                obj.created_user_id = Value.users(i).createdUserId().toFloat64();
                obj.updated_user_id = Value.users(i).updatedUserId().toFloat64();
                obj.role_id = Value.users(i).roleId();
                obj.empLevel = Value.users(i).empLevel();
                obj.isSelected = false;
                empList.push(obj)
            }
            console.log("finalArray",empList)
            dispatch(searchEmployee(empList));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}