/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import { Reducer } from 'redux';
import EmployeeSkillSetDetailsContainerState from '../state/EmployeeSkillSetDetailsContainerState';
import EmployeeSkillSetDetailsContainerActionTypeKeys from '../actions/EmployeeSkillSetDetailsContainerActionTypeKeys';
import EmployeeSkillSetDetailsContainerActionTypes from '../actions/EmployeeSkillSetDetailsContainerActionTypes';


const initialState: EmployeeSkillSetDetailsContainerState = {
    employeeList:[],
    showLoader: false
};

const EmployeeSkillSetDetailsContainerReducers: Reducer<EmployeeSkillSetDetailsContainerState, EmployeeSkillSetDetailsContainerActionTypes> = (state = initialState, action) => {
    switch (action.type) {

        case EmployeeSkillSetDetailsContainerActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoading

            };
        }
       
        case EmployeeSkillSetDetailsContainerActionTypeKeys.SEARCH_EMPLOYEE_LIST: {
            return {
                ...state,
                employeeList:action.value,
                text: action.text
            };
        }
        
       
        default:
            return state;
    }
};

export default EmployeeSkillSetDetailsContainerReducers;