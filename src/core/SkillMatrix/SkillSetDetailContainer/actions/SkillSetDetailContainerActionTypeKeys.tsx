/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.43.115:8080/sakthivel)
 */


enum DesiredSkillSetActionTypeKeys {
    SKILL_SET_DETAILS = "SKILL_SET_DETAILS",
    SHOW_LOADER = "SHOW_LOADER"
}

export default DesiredSkillSetActionTypeKeys;