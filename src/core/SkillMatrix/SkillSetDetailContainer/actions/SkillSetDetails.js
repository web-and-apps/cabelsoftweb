// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var detailres = detailres || {};

/**
 * @constructor
 */
detailres.detailres = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {detailres.detailres}
 */
detailres.detailres.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {detailres.detailres=} obj
 * @returns {detailres.detailres}
 */
detailres.detailres.getRootAsdetailres = function(bb, obj) {
  return (obj || new detailres.detailres).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.detailres.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
detailres.detailres.prototype.userName = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.detailres.prototype.previousYearExperience = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.detailres.prototype.vthinkExperience = function() {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.detailres.prototype.totalExperience = function() {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {number} index
 * @param {detailres.skilldetailsres=} obj
 * @returns {detailres.skilldetailsres}
 */
detailres.detailres.prototype.skillDetails = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 14);
  return offset ? (obj || new detailres.skilldetailsres).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
detailres.detailres.prototype.skillDetailsLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 14);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @param {flatbuffers.Builder} builder
 */
detailres.detailres.startdetailres = function(builder) {
  builder.startObject(6);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
detailres.detailres.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} userNameOffset
 */
detailres.detailres.addUserName = function(builder, userNameOffset) {
  builder.addFieldOffset(1, userNameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} previousYearExperience
 */
detailres.detailres.addPreviousYearExperience = function(builder, previousYearExperience) {
  builder.addFieldInt64(2, previousYearExperience, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} vthinkExperience
 */
detailres.detailres.addVthinkExperience = function(builder, vthinkExperience) {
  builder.addFieldInt64(3, vthinkExperience, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} totalExperience
 */
detailres.detailres.addTotalExperience = function(builder, totalExperience) {
  builder.addFieldInt64(4, totalExperience, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillDetailsOffset
 */
detailres.detailres.addSkillDetails = function(builder, skillDetailsOffset) {
  builder.addFieldOffset(5, skillDetailsOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
detailres.detailres.createSkillDetailsVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
detailres.detailres.startSkillDetailsVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
detailres.detailres.enddetailres = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} offset
 */
detailres.detailres.finishdetailresBuffer = function(builder, offset) {
  builder.finish(offset);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Offset} userNameOffset
 * @param {flatbuffers.Long} previousYearExperience
 * @param {flatbuffers.Long} vthinkExperience
 * @param {flatbuffers.Long} totalExperience
 * @param {flatbuffers.Offset} skillDetailsOffset
 * @returns {flatbuffers.Offset}
 */
detailres.detailres.createdetailres = function(builder, id, userNameOffset, previousYearExperience, vthinkExperience, totalExperience, skillDetailsOffset) {
  detailres.detailres.startdetailres(builder);
  detailres.detailres.addId(builder, id);
  detailres.detailres.addUserName(builder, userNameOffset);
  detailres.detailres.addPreviousYearExperience(builder, previousYearExperience);
  detailres.detailres.addVthinkExperience(builder, vthinkExperience);
  detailres.detailres.addTotalExperience(builder, totalExperience);
  detailres.detailres.addSkillDetails(builder, skillDetailsOffset);
  return detailres.detailres.enddetailres(builder);
}

/**
 * @constructor
 */
detailres.skilldetailsres = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {detailres.skilldetailsres}
 */
detailres.skilldetailsres.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {detailres.skilldetailsres=} obj
 * @returns {detailres.skilldetailsres}
 */
detailres.skilldetailsres.getRootAsskilldetailsres = function(bb, obj) {
  return (obj || new detailres.skilldetailsres).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.skilldetailsres.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
detailres.skilldetailsres.prototype.skillname = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
detailres.skilldetailsres.prototype.exp = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
detailres.skilldetailsres.prototype.status = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
detailres.skilldetailsres.prototype.lastUpdated = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 12);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @param {flatbuffers.Builder} builder
 */
detailres.skilldetailsres.startskilldetailsres = function(builder) {
  builder.startObject(5);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
detailres.skilldetailsres.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillnameOffset
 */
detailres.skilldetailsres.addSkillname = function(builder, skillnameOffset) {
  builder.addFieldOffset(1, skillnameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} exp
 */
detailres.skilldetailsres.addExp = function(builder, exp) {
  builder.addFieldInt64(2, exp, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} statusOffset
 */
detailres.skilldetailsres.addStatus = function(builder, statusOffset) {
  builder.addFieldOffset(3, statusOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} lastUpdatedOffset
 */
detailres.skilldetailsres.addLastUpdated = function(builder, lastUpdatedOffset) {
  builder.addFieldOffset(4, lastUpdatedOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
detailres.skilldetailsres.endskilldetailsres = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Offset} skillnameOffset
 * @param {flatbuffers.Long} exp
 * @param {flatbuffers.Offset} statusOffset
 * @param {flatbuffers.Offset} lastUpdatedOffset
 * @returns {flatbuffers.Offset}
 */
detailres.skilldetailsres.createskilldetailsres = function(builder, id, skillnameOffset, exp, statusOffset, lastUpdatedOffset) {
  detailres.skilldetailsres.startskilldetailsres(builder);
  detailres.skilldetailsres.addId(builder, id);
  detailres.skilldetailsres.addSkillname(builder, skillnameOffset);
  detailres.skilldetailsres.addExp(builder, exp);
  detailres.skilldetailsres.addStatus(builder, statusOffset);
  detailres.skilldetailsres.addLastUpdated(builder, lastUpdatedOffset);
  return detailres.skilldetailsres.endskilldetailsres(builder);
}

// Exports for Node.js and RequireJS
this.detailres = detailres;