/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

 
import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import SkillSetDetailContainerActionTypeKeys from './SkillSetDetailContainerActionTypeKeys';
import SkillSetDetailContainerActionTypes from './SkillSetDetailContainerActionTypes';
import SkillSetDetailContainerState from '../state/SkillSetDetailContainerState';
import { APIUserManagement } from '../../../api/APIUsersManagement';


export const skillSetDetails = (data:any,skill:any): SkillSetDetailContainerActionTypes => {
    return {
        type: SkillSetDetailContainerActionTypeKeys.SKILL_SET_DETAILS,
        value:data,
        skill: skill,
        text: "Success"
    };
}
export const showLoader = (data:Boolean): SkillSetDetailContainerActionTypes => {
    return {
      type: SkillSetDetailContainerActionTypeKeys.SHOW_LOADER,
      showLoading: data
    }
  }


export const skillSetdetails: ActionCreator<
    ThunkAction<
    Promise<any>,
    SkillSetDetailContainerState,
    null,
    SkillSetDetailContainerActionTypes
    >
> = (user_id: String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./SkillSetDetailsRequest').report;
        var addProfileData = addProfile.report;
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createString(user_id);
        var addPreviousYearExperience = fbb.createLong("");
        var addVthinkExperience = fbb.createLong("");
        var addTotalExperience = fbb.createLong("");
        var addSkillMatrixId = fbb.createString("");
        addProfileData.startreport(fbb);
        addProfileData.addUserId(fbb, id);
        addProfileData.addPreviousYearExperience(fbb, addPreviousYearExperience);
        addProfileData.addVthinkExperience(fbb, addVthinkExperience);
        addProfileData.addTotalExperience(fbb, addTotalExperience);
        addProfileData.addSkillMatrixId(fbb, addSkillMatrixId);
        var ints = addProfileData.endreport(fbb);
        fbb.finish(ints);
        var value = fbb.asUint8Array();
        const service = new APIUserManagement();
        return service.getSkillSetDetails(value).then((response) => {
            dispatch(showLoader(false));
            console.log(response.value);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./SkillSetDetails').detailres;
            var responseArray = new Uint8Array(response.value);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.detailres.getRootAsdetailres(byteBuffer);
            let Skillobj: any = {}
            Skillobj.id = Value.id().toFloat64()
            Skillobj.userName = Value.userName()
            Skillobj.previousYearExperience = Value.previousYearExperience().toFloat64()
            Skillobj.vthinkExperience = Value.vthinkExperience().toFloat64()
            Skillobj.totalExperience = Value.totalExperience().toFloat64()
            var skillLength = Value.skillDetailsLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillDetails(i).id().toFloat64();
                obj.skillname = Value.skillDetails(i).skillname();
                obj.exp = Value.skillDetails(i).exp().toFloat64();
                obj.status = Value.skillDetails(i).status();
                obj.lastUpdated = Value.skillDetails(i).lastUpdated();
                if (obj.status === "Approved"){
                obj.Approved = true
                }else{
                obj.Approved = false   
                }
                empList.push(obj)
            }
            console.log("finalArray",Skillobj)
            dispatch(skillSetDetails(Skillobj,empList));
            
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}

