/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import { Reducer } from 'redux';
import SkillSetDetailContainerState from '../state/SkillSetDetailContainerState';
import SkillSetDetailContainerActionTypeKeys from '../actions/SkillSetDetailContainerActionTypeKeys';
import SkillSetDetailContainerActionTypes from '../actions/SkillSetDetailContainerActionTypes';
import SkillSetDetails from "../../../../containers/SkillMatrix/SkillSetDetails/SkillSetDetailsContainer"


const initialState: SkillSetDetailContainerState = {
    skillSetList:"",
    skillDetails:[],
    showLoader: false
};

const SkillSetDetailContainerReducers: Reducer<SkillSetDetailContainerState, SkillSetDetailContainerActionTypes> = (state = initialState, action) => {
    switch (action.type) {

        case SkillSetDetailContainerActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoading

            };
        }
       
        case SkillSetDetailContainerActionTypeKeys.SKILL_SET_DETAILS: {
            return {
                ...state,
                skillSetList:action.value,
                skillDetails:action.skill,
                text: action.text
            };
        }
        
       
        default:
            return state;
    }
};

export default SkillSetDetailContainerReducers;