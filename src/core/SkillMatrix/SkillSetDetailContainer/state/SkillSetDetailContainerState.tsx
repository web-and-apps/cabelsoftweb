/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.43.115:8080/sakthivel)
 */
import{SkillSetList} from "../../../../containers/SkillMatrix/MasterSkillset/MasterSkillsetContainer"

export default interface SkillSetDetailContainerState {
    skillSetList:any,
    skillDetails:any,
    showLoader:Boolean

}