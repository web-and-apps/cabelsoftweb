/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [Kumaresan Periyasamy](http://192.168.1.22:8080/Kumaresan)
 */

import { Reducer } from 'redux';
import MasterSkillsetState from '../state/MasterSkillsetState';
import MasterSkillsetActionTypeKeys from '../actions/MasterSkillsetActionTypeKeys';
import MasterSkillsetActionTypes from '../actions/MasterSkillsetActionTypes';


const initialState: MasterSkillsetState = {
    skillSetList:[],
    showPopup: false,
    showLoader:false

};

const MasterSkillsetReducers: Reducer<MasterSkillsetState, MasterSkillsetActionTypes> = (state = initialState, action) => {
    switch (action.type) {
       
        case MasterSkillsetActionTypeKeys.SHOW_POPUP: {
            return {
                ...state,
                showPopup: action.showPopup,
                text: action.text
            };
        }
        case MasterSkillsetActionTypeKeys.ADD_SKILLSET: {
            return {
                ...state,
                showPopup: action.showPopup,
                skillSetList:action.value,
                text: action.text
            };
        }
        case MasterSkillsetActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoader,
            };
        }
       
        default:
            return state;
    }
};

export default MasterSkillsetReducers;