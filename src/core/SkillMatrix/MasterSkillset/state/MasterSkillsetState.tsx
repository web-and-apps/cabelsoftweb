/**
 * Login Container State Informations
 *
 * @version 1.0.0
 * @author [Parthiban](http://192.168.43.115:8080/parthiban)
 */
import{SkillSetList} from "../../../../containers/SkillMatrix/MasterSkillset/MasterSkillsetContainer"

export default interface MasterSkillsetState {
    skillSetList:SkillSetList[],
    showPopup: Boolean;
    showLoader:Boolean;

}