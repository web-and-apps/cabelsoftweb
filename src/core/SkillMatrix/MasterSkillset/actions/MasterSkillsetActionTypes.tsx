/**
 * Login Action Types
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import {
    IHideAcion,
    IShowAction,
    IAddSkillSet,
    IShowLoader
} from "./MasterSkillsetActionInterface";


type MasterSkillsetActionTypes =
    | IHideAcion
    | IShowAction
    | IAddSkillSet
    |IShowLoader
   
export default MasterSkillsetActionTypes;