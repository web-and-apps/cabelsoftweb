/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

 
import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import MasterSkillsetActionTypeKeys from './MasterSkillsetActionTypeKeys';
import MasterSkillsetActionTypes from './MasterSkillsetActionTypes';
import MasterSkillsetState from '../state/MasterSkillsetState';
import { APIUserManagement } from '../../../api/APIUsersManagement';
export const showPopup = (data:Boolean): MasterSkillsetActionTypes => {
    return {
        type: MasterSkillsetActionTypeKeys.SHOW_POPUP,
        showPopup: data,
        text: "Success"
    };
}

export const hide = (): MasterSkillsetActionTypes => {
    return {
        type: MasterSkillsetActionTypeKeys.SHOW_POPUP,
        showPopup: false,
        text: "Success"
    };
}
export const showLoader = (data:Boolean): MasterSkillsetActionTypes => {
    return {
        type: MasterSkillsetActionTypeKeys.SHOW_LOADER,
        showLoader: data
    };
}
export const addSkill = (data:any): MasterSkillsetActionTypes => {
    return {
        type: MasterSkillsetActionTypeKeys.ADD_SKILLSET,
        showPopup: false,
        value:data, 
        text: "Success"
    };
}

export const hidePopup = (error: Error): MasterSkillsetActionTypes => {
    return {
        type: MasterSkillsetActionTypeKeys.HIDEPOPUP
    };
}

export const actionShowPopup: ActionCreator<
    ThunkAction<
        Promise<any>,
        MasterSkillsetState,
        null,
        MasterSkillsetActionTypes
    >
> = (data:Boolean) => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(showPopup(!data));
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
export const hidePopUp: ActionCreator<
    ThunkAction<
        Promise<any>,
        MasterSkillsetState,
        null,
        MasterSkillsetActionTypes
    >
> = () => {
    return (dispatch: Dispatch) => {
        const service = new APIUserManagement();
        return service.showPopup().then((response) => {
            dispatch(hide());
        }).catch((error) => {
            dispatch(hidePopup(error));
        });
    }
}
export const editSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    MasterSkillsetState,
    null,
    MasterSkillsetActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./addSkillSet').com;
        var addProfileData = addProfile.vthink.skillset.tables.skillset
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var title = fbb.createString(param.skillName);
        var first_name = fbb.createLong(param.createdUserId);
        var last_name = fbb.createLong(param.updatedUserId);
        addProfileData.startskillset(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addSkillName(fbb, title)
        addProfileData.addCreatedUserId(fbb, first_name)
        addProfileData.addUpdatedUserId(fbb, last_name)
        var ints = addProfileData.endskillset(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.editSkillSet(value,param.id).then((response) => {
            getSkillSetlist()
            .then((response) => {
                dispatch(showLoader(false));
                console.log(response)
                dispatch(addSkill(response));
            })
            .catch((error) => {
                dispatch(showLoader(false));
                console.log(error)
            })
            
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
export const addSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    MasterSkillsetState,
    null,
    MasterSkillsetActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./addSkillSet').com;
        var addProfileData = addProfile.vthink.skillset.tables.skillset
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var title = fbb.createString(param.skillName);
        var first_name = fbb.createLong(param.createdUserId);
        var last_name = fbb.createLong(param.updatedUserId);
        addProfileData.startskillset(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addSkillName(fbb, title)
        addProfileData.addCreatedUserId(fbb, first_name)
        addProfileData.addUpdatedUserId(fbb, last_name)
        var ints = addProfileData.endskillset(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.addSkillSet(value).then((response) => {
            getSkillSetlist()
            .then((response) => {
                dispatch(showLoader(false));
                console.log(response)
                dispatch(addSkill(response));
            })
            .catch((error) => {
                dispatch(showLoader(false));
                console.log(error)
            })
            
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
const getSkillSetlist = (): Promise<any> => {
    return new Promise((resolve, reject) => {
        const service = new APIUserManagement();
        return service.getSkillSetList("","0","10").then((response) => {
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./SkillSetList').skillsetlist;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            var totalCount = Value.totalSkillsetCount().toFloat64();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                skilllist.push(obj)
            }
            console.log("finalArray",skilllist)
            resolve(skilllist);
        }).catch((error) => {
            reject(error)
        });
    })
}

export const searchSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    MasterSkillsetState,
    null,
    MasterSkillsetActionTypes
    >
> = (searchtext:string,start:String,limit:String) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getSkillSetList(searchtext,start,limit).then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./SkillSetList').skillsetlist;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.skillsetlist.getRootAsskillsetlist(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.skillsetLength();
            var totalCount = Value.totalSkillsetCount().toFloat64();
            const skilllist = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.skillset(i).id().toFloat64();
                obj.skillName = Value.skillset(i).skillName().toString();
                obj.createdUserId = Value.skillset(i).createdUserId().toFloat64();
                obj.updatedUserId = Value.skillset(i).updatedUserId().toFloat64();
                skilllist.push(obj)
            }
            console.log("finalArray",skilllist)
            dispatch(addSkill(skilllist));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}