/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sundar Essakimuthu](http://192.168.1.22:8080/Sundar)
 */

import keys from "./MasterSkillsetActionTypeKeys";

export interface IShowAction {
    readonly type: keys.SHOW_POPUP,
    showPopup: Boolean,
    text: any
}
export interface IShowLoader {
    readonly type: keys.SHOW_LOADER,
    showLoader: Boolean,
}
export interface IAddSkillSet {
    readonly type: keys.ADD_SKILLSET,
    showPopup: Boolean,
    value:any,
    text: any
}

export interface IHideAcion {
    readonly type: keys.HIDEPOPUP;
}

