/**
 * Login Action Keys for identify the action type
 *
 * @version 1.0.0
 * @author [Parthiban](http://192.168.43.115:8080/parthiban)
 */


enum MasterSkillsetActionTypeKeys {
    SHOW_POPUP = "SHOW_POPUP",
    HIDEPOPUP = "HIDE_POPUP",
    ADD_SKILLSET = "ADD_SKILLSET",
    SHOW_LOADER = "SHOW_LOADER",
}

export default MasterSkillsetActionTypeKeys;