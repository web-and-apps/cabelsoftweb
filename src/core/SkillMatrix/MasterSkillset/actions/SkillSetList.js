// automatically generated by the FlatBuffers compiler, do not modify

/**
 * @const
 * @namespace
 */
var skillsetlist = skillsetlist || {};

/**
 * @constructor
 */
skillsetlist.skillsetlist = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {skillsetlist.skillsetlist}
 */
skillsetlist.skillsetlist.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {skillsetlist.skillsetlist=} obj
 * @returns {skillsetlist.skillsetlist}
 */
skillsetlist.skillsetlist.getRootAsskillsetlist = function(bb, obj) {
  return (obj || new skillsetlist.skillsetlist).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @param {number} index
 * @param {skillsetlist.skillset=} obj
 * @returns {skillsetlist.skillset}
 */
skillsetlist.skillsetlist.prototype.skillset = function(index, obj) {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? (obj || new skillsetlist.skillset).__init(this.bb.__indirect(this.bb.__vector(this.bb_pos + offset) + index * 4), this.bb) : null;
};

/**
 * @returns {number}
 */
skillsetlist.skillsetlist.prototype.skillsetLength = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.__vector_len(this.bb_pos + offset) : 0;
};

/**
 * @returns {flatbuffers.Long}
 */
skillsetlist.skillsetlist.prototype.totalSkillsetCount = function() {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 */
skillsetlist.skillsetlist.startskillsetlist = function(builder) {
  builder.startObject(2);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillsetOffset
 */
skillsetlist.skillsetlist.addSkillset = function(builder, skillsetOffset) {
  builder.addFieldOffset(0, skillsetOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {Array.<flatbuffers.Offset>} data
 * @returns {flatbuffers.Offset}
 */
skillsetlist.skillsetlist.createSkillsetVector = function(builder, data) {
  builder.startVector(4, data.length, 4);
  for (var i = data.length - 1; i >= 0; i--) {
    builder.addOffset(data[i]);
  }
  return builder.endVector();
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {number} numElems
 */
skillsetlist.skillsetlist.startSkillsetVector = function(builder, numElems) {
  builder.startVector(4, numElems, 4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} totalSkillsetCount
 */
skillsetlist.skillsetlist.addTotalSkillsetCount = function(builder, totalSkillsetCount) {
  builder.addFieldInt64(1, totalSkillsetCount, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
skillsetlist.skillsetlist.endskillsetlist = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} offset
 */
skillsetlist.skillsetlist.finishskillsetlistBuffer = function(builder, offset) {
  builder.finish(offset);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillsetOffset
 * @param {flatbuffers.Long} totalSkillsetCount
 * @returns {flatbuffers.Offset}
 */
skillsetlist.skillsetlist.createskillsetlist = function(builder, skillsetOffset, totalSkillsetCount) {
  skillsetlist.skillsetlist.startskillsetlist(builder);
  skillsetlist.skillsetlist.addSkillset(builder, skillsetOffset);
  skillsetlist.skillsetlist.addTotalSkillsetCount(builder, totalSkillsetCount);
  return skillsetlist.skillsetlist.endskillsetlist(builder);
}

/**
 * @constructor
 */
skillsetlist.skillset = function() {
  /**
   * @type {flatbuffers.ByteBuffer}
   */
  this.bb = null;

  /**
   * @type {number}
   */
  this.bb_pos = 0;
};

/**
 * @param {number} i
 * @param {flatbuffers.ByteBuffer} bb
 * @returns {skillsetlist.skillset}
 */
skillsetlist.skillset.prototype.__init = function(i, bb) {
  this.bb_pos = i;
  this.bb = bb;
  return this;
};

/**
 * @param {flatbuffers.ByteBuffer} bb
 * @param {skillsetlist.skillset=} obj
 * @returns {skillsetlist.skillset}
 */
skillsetlist.skillset.getRootAsskillset = function(bb, obj) {
  return (obj || new skillsetlist.skillset).__init(bb.readInt32(bb.position()) + bb.position(), bb);
};

/**
 * @returns {flatbuffers.Long}
 */
skillsetlist.skillset.prototype.id = function() {
  var offset = this.bb.__offset(this.bb_pos, 4);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Encoding=} optionalEncoding
 * @returns {string|Uint8Array|null}
 */
skillsetlist.skillset.prototype.skillName = function(optionalEncoding) {
  var offset = this.bb.__offset(this.bb_pos, 6);
  return offset ? this.bb.__string(this.bb_pos + offset, optionalEncoding) : null;
};

/**
 * @returns {flatbuffers.Long}
 */
skillsetlist.skillset.prototype.createdUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 8);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @returns {flatbuffers.Long}
 */
skillsetlist.skillset.prototype.updatedUserId = function() {
  var offset = this.bb.__offset(this.bb_pos, 10);
  return offset ? this.bb.readInt64(this.bb_pos + offset) : this.bb.createLong(0, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 */
skillsetlist.skillset.startskillset = function(builder) {
  builder.startObject(4);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 */
skillsetlist.skillset.addId = function(builder, id) {
  builder.addFieldInt64(0, id, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Offset} skillNameOffset
 */
skillsetlist.skillset.addSkillName = function(builder, skillNameOffset) {
  builder.addFieldOffset(1, skillNameOffset, 0);
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} createdUserId
 */
skillsetlist.skillset.addCreatedUserId = function(builder, createdUserId) {
  builder.addFieldInt64(2, createdUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} updatedUserId
 */
skillsetlist.skillset.addUpdatedUserId = function(builder, updatedUserId) {
  builder.addFieldInt64(3, updatedUserId, builder.createLong(0, 0));
};

/**
 * @param {flatbuffers.Builder} builder
 * @returns {flatbuffers.Offset}
 */
skillsetlist.skillset.endskillset = function(builder) {
  var offset = builder.endObject();
  return offset;
};

/**
 * @param {flatbuffers.Builder} builder
 * @param {flatbuffers.Long} id
 * @param {flatbuffers.Offset} skillNameOffset
 * @param {flatbuffers.Long} createdUserId
 * @param {flatbuffers.Long} updatedUserId
 * @returns {flatbuffers.Offset}
 */
skillsetlist.skillset.createskillset = function(builder, id, skillNameOffset, createdUserId, updatedUserId) {
  skillsetlist.skillset.startskillset(builder);
  skillsetlist.skillset.addId(builder, id);
  skillsetlist.skillset.addSkillName(builder, skillNameOffset);
  skillsetlist.skillset.addCreatedUserId(builder, createdUserId);
  skillsetlist.skillset.addUpdatedUserId(builder, updatedUserId);
  return skillsetlist.skillset.endskillset(builder);
}

// Exports for Node.js and RequireJS
this.skillsetlist = skillsetlist;