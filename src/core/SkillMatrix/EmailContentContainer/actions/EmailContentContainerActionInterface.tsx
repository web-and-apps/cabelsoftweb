/**
 * Login Action Interface
 *
 * @version 1.0.0
 * @author [Sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import keys from "./EmailContentContainerActionTypeKeys";



export interface ISearchEmployee {
    readonly type: keys.SEARCH_EMPLOYEE_LIST,
    value:any,
    text: any
}
export interface IShowLoader {
    type: keys.SHOW_LOADER,
    showLoading: Boolean,
}


