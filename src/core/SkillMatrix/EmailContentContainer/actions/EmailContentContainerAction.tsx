/**
 * Login Container Action
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

 
import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import EmailContentContainerActionTypeKeys from './EmailContentContainerActionTypeKeys';
import EmailContentContainerActionTypes from './EmailContentContainerActionTypes';
import EmailContentContainerState from '../state/EmailContentContainerState';
import { APIUserManagement } from '../../../api/APIUsersManagement';

export const showLoader = (data:Boolean): EmailContentContainerActionTypes => {
    return {
      type: EmailContentContainerActionTypeKeys.SHOW_LOADER,
      showLoading: data
    }
  }
export const searchEmployee = (data:any): EmailContentContainerActionTypes => {
    return {
        type: EmailContentContainerActionTypeKeys.SEARCH_EMPLOYEE_LIST,
        value:data, 
        text: "Success"
    };
}

export const searchSkillSet: ActionCreator<
    ThunkAction<
    Promise<any>,
    EmailContentContainerState,
    null,
    EmailContentContainerActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.getuserList().then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./saveEmailContent').users;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.users.getRootAsusers(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.usersLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.users(i).id().toFloat64();
                obj.title = Value.users(i).title();
                obj.first_name = Value.users(i).firstName();
                obj.last_name = Value.users(i).lastName();
                obj.emp_id = Value.users(i).empId();
                obj.emp_photo_url = Value.users(i).empPhotoUrl();
                obj.status = Value.users(i).status();
                obj.email_id = Value.users(i).emailId();
                obj.appraiser_id = Value.users(i).appraiserId().toFloat64();
                obj.created_user_id = Value.users(i).createdUserId().toFloat64();
                obj.updated_user_id = Value.users(i).updatedUserId().toFloat64();
                obj.role_id = Value.users(i).roleId();
                obj.empLevel = Value.users(i).empLevel();
                obj.isSelected = false;
                empList.push(obj)
            }
            console.log("finalArray",empList)
            dispatch(searchEmployee(empList));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}


export const getEmailTemplate: ActionCreator<
    ThunkAction<
    Promise<any>,
    EmailContentContainerState,
    null,
    EmailContentContainerActionTypes
    >
> = (searchtext:string) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        return service.searchUserList(searchtext).then((response) => {
            dispatch(showLoader(false));
            console.log(response.data);
            var flatbuffers = require('flatbuffers').flatbuffers;
            var Data = require('./saveEmailContent').users;
            var responseArray = new Uint8Array(response.data);
            var byteBuffer = new flatbuffers.ByteBuffer(responseArray);
            var Value = Data.users.getRootAsusers(byteBuffer);
            // var feedBackValue = kraData.kraList.feedbackListLength(byteBuffer);
            var skillLength = Value.usersLength();
            const empList = [];
            for (let i = 0; i <= skillLength - 1; i++) {
                let obj: any = {}
                obj.id = Value.users(i).id().toFloat64();
                obj.title = Value.users(i).title();
                obj.first_name = Value.users(i).firstName();
                obj.last_name = Value.users(i).lastName();
                obj.emp_id = Value.users(i).empId();
                obj.emp_photo_url = Value.users(i).empPhotoUrl();
                obj.status = Value.users(i).status();
                obj.email_id = Value.users(i).emailId();
                obj.appraiser_id = Value.users(i).appraiserId().toFloat64();
                obj.created_user_id = Value.users(i).createdUserId().toFloat64();
                obj.updated_user_id = Value.users(i).updatedUserId().toFloat64();
                obj.role_id = Value.users(i).roleId();
                obj.empLevel = Value.users(i).empLevel();
                obj.isSelected = false;
                empList.push(obj)
            }
            console.log("finalArray",empList)
            dispatch(searchEmployee(empList));
        }).catch((error) => {
            dispatch(showLoader(false));
            console.log(error)
        });
    }
}
export const saveEmailTemplate: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmailContentContainerState,
        null,
        EmailContentContainerActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./saveEmailContent').emailtemplatecontent;
        var addProfileData = addProfile.emailtemplatecontent
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong("");
        var email_template_id = fbb.createLong("");
        var sendTo = fbb.createString(param.sendTo);
        var subject = fbb.createString(param.subject);
        var messageBody = fbb.createString(param.messageBody);
        var created_user_id = fbb.createLong("1");
        addProfileData.startemailtemplatecontent(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addEmailTemplateId(fbb, email_template_id)
        addProfileData.addSubject(fbb, subject)
        addProfileData.addMessage(fbb, messageBody)
        addProfileData.addTemplateName(fbb, sendTo)
        addProfileData.addCreatedUserId(fbb, created_user_id)
        var ints = addProfileData.endemailtemplatecontent(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.saveEmailTemplate(value).then((response) => {
            window.alert("EmailTemplate Added SuccessFully")
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
export const updateEmailTemplate: ActionCreator<
    ThunkAction<
        Promise<any>,
        EmailContentContainerState,
        null,
        EmailContentContainerActionTypes
    >
> = (param: any) => {
    return (dispatch: Dispatch) => {
        dispatch(showLoader(true));
        const service = new APIUserManagement();
        var flatbuffers = require('flatbuffers').flatbuffers;
        var addProfile = require('./saveEmailContent').com;
        var addProfileData = addProfile.vthink.skillset.tables.skillset
        var fbb = new flatbuffers.Builder(0);
        var id = fbb.createLong(param.id);
        var title = fbb.createString(param.skillName.data);
        var first_name = fbb.createLong(param.createdUserId);
        var last_name = fbb.createLong(param.updatedUserId);
        addProfileData.startskillset(fbb)
        addProfileData.addId(fbb, id)
        addProfileData.addSkillName(fbb, title)
        addProfileData.addCreatedUserId(fbb, first_name)
        addProfileData.addUpdatedUserId(fbb, last_name)
        var ints = addProfileData.endskillset(fbb)
        fbb.finish(ints)
        var value = fbb.asUint8Array();
        return service.addSkillSet(value).then((response) => {
            window.alert("EmailTemplate Updated SuccessFully")   
        }).catch((error: any) => {
            dispatch(showLoader(false));
        });
    }
}
