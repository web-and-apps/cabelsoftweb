/**
 * Login Container Reducer 
 *
 * @version 1.0.0
 * @author [sakthivel](http://192.168.1.22:8080/sakthivel)
 */

import { Reducer } from 'redux';
import EmailContentContainerState from '../state/EmailContentContainerState';
import EmailContentContainerActionTypeKeys from '../actions/EmailContentContainerActionTypeKeys';
import EmailContentContainerActionTypes from '../actions/EmailContentContainerActionTypes';


const initialState: EmailContentContainerState = {
    employeeList:[],
    showLoader: false
};

const EmailContentContainerReducers: Reducer<EmailContentContainerState, EmailContentContainerActionTypes> = (state = initialState, action) => {
    switch (action.type) {

        case EmailContentContainerActionTypeKeys.SHOW_LOADER: {
            return {
                ...state,
                showLoader: action.showLoading

            };
        }
       
        case EmailContentContainerActionTypeKeys.SEARCH_EMPLOYEE_LIST: {
            return {
                ...state,
                employeeList:action.value,
                text: action.text
            };
        }
        
       
        default:
            return state;
    }
};

export default EmailContentContainerReducers;