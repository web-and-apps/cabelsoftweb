import React, { Suspense, lazy } from 'react';
import './App.scss';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import "./App.scss";
import { Provider } from 'react-redux';
import { Store } from 'redux';
import AppraiserManagementContainer from './containers/KRA/AppraiserManagement/AppraiserManagementContainer';
import EmailNotificationConainer from './containers/SkillMatrix/EmailNotification/EmailNotificationConainer';

const LoginPage = (
  lazy(() => (
    import('./containers/LoginContainer/LoginContainer')
  ))
);

const EmployeeManagementPage = (
  lazy(() => (
    import('./containers/KRA/EmployeeManagement/EmployeeManagementContainer')
  ))
);
const GoalManagementPage = (
  lazy(() => (
    import('./containers/KRA/GoalManagement/GoalManagementContainer')
  ))
);
const EmployeeKRADetailsContainer = (
  lazy(() => (
    import('./containers/KRA/EmployeeKRADetails/EmployeeKRADetailsContainer')
  ))
);
const AssignKRAGoalsContainer = (
  lazy(() => (
    import('./containers/KRA/AssignKRAGoals/AssignKRAGoalsContainer')
  ))
);
const UserGoalDetailsPage = (
  lazy(() => (
    import('./containers/KRA/UserGoalDetails/UserGoalDetailsContainer')
  ))
);

const MasterSkillsetContainer = (
  lazy(() => (
    import('./containers/SkillMatrix/MasterSkillset/MasterSkillsetContainer')
  ))
);
const DesiredSkillSet = (
  lazy(() => (
    import('./containers/SkillMatrix/DesiredSkillSet/DesiredSkillSetContainer')
  ))
);
const SkillSetDetails = (
  lazy(() => (
    import('./containers/SkillMatrix/SkillSetDetails/SkillSetDetailsContainer')
  ))
);
const FilterresourceContainer = (
  lazy(() => (
    import('./containers/SkillMatrix/FilterResources/FilterResourcesContainer')
  ))
);
const employeeSkillSetDetailsContainer = (
  lazy(() => (
    import('./containers/SkillMatrix/EmployeeSkilSetDetails/EmployeeSkilSetDetailsContainer')
  ))
);
// const AppraiserManagement = (
//   lazy(() => (
//     import('./containers/KRA/AppraiserManagement/AppraiserManagementContainer')
//   ))
// );
const LoadingMessage = () => (
  <div>loading...</div>
);

// Create interface for Props
interface IProps {
  store: Store;
}

const App: React.SFC<IProps> = props => {
  return (
    <Provider store={props.store}>
    {/* <LoaderComponent></LoaderComponent> */}
    <Router>
    <div>
        <Suspense fallback={<LoadingMessage />}>
          <Switch>
            <Route exact path="/" component={LoginPage} />
            <Route path="/EmployeeManagement" component={EmployeeManagementPage} />
            <Route path="/AppraiserManagement" component={AppraiserManagementContainer} />
            <Route path="/GoalManagement" component={GoalManagementPage} />
            <Route path="/KRADetails" component={EmployeeKRADetailsContainer} />
            <Route path="/AssignKRA" component={AssignKRAGoalsContainer} />
            <Route path="/userKRA" component={UserGoalDetailsPage} />
            <Route path="/history" component={EmployeeKRADetailsContainer} />
            <Route path="/MasterSkillset" component={MasterSkillsetContainer} />
            <Route path="/EmailNotification" component={EmailNotificationConainer} />
            <Route path="/DesiredSkillSet" component={DesiredSkillSet} />
            <Route path="/SkillSetDetails" component={SkillSetDetails} />
            <Route path="/FilterResources" component={FilterresourceContainer} />
            <Route path="/EmployeeSkilSetDetails" component={employeeSkillSetDetailsContainer} />
          </Switch>
        </Suspense>
      </div>
    </Router></Provider>
  );
};


export default App;
