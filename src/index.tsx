import React from 'react';
import ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';
import './App.scss';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './core/index';
import App from './App';

let store = createStore(rootReducer, undefined, applyMiddleware(thunk));

// export default AppProviders;
const rootElement = document.getElementById("root");
ReactDOM.render(<App store={store} />, rootElement);
