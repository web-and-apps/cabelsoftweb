const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
    app.use(
        '/api/users',
        createProxyMiddleware({
            //   target: 'https://vthink.vthinkdeveloper.com',
            target: 'http://192.168.43.115:8086',
            changeOrigin: true,
            pathRewrite: {
                '^/api/users': '/api/users'
            },
            // secure: false
        })
    );

    app.use(
        '/api/kra',
        createProxyMiddleware({
            //   target: 'https://vthink.vthinkdeveloper.com',
            target: 'http://192.168.43.115:8087',
            changeOrigin: true,
            pathRewrite: {
                '^/api/kra': '/api/kra'
            },
            // secure: false
        })
    );
    app.use(
        '/api/goal',
        createProxyMiddleware({
            //   target: 'https://vthink.vthinkdeveloper.com',
            target: 'http://192.168.43.115:8087',
            changeOrigin: true,
            pathRewrite: {
                '^/api/goal': '/api/goal'
            },
            // secure: false
        })
    );


    app.use(
        '/api/storage',
        createProxyMiddleware({
            //   target: 'https://vthink.vthinkdeveloper.com',
            target: 'http://192.168.43.115:8088',
            changeOrigin: true,
            pathRewrite: {
                '^/api/storage': '/api/storage'
            },
            // secure: false
        })
    );

    app.use(
        '/api/login',
        createProxyMiddleware({
            //   target: 'https://vthink.vthinkdeveloper.com',
            target: 'http://192.168.43.115:8086',
            changeOrigin: true,
            pathRewrite: {
                '^/api/login': '/api/login'
            },
            // secure: false
        })
    );
};