import React from 'react'
import { connect } from 'react-redux';
import CenteredView from '../../components/CenteredView/CenteredView';
import '../../components/Background/Background.scss';
import {APIUserManagement} from '../../core/api/APIUsersManagement' 
import { actionDoLogin, actionGetRoles, loginSuccess } from '../../core/Login/actions/LoginActions'
import AppStore from '../../core/AppStore';
// function LoginPage() {

// }
// export default LoginPage

interface IProps {
    // doLogin: any,
    selectedCategory: any,
    getEmpDetail: any,
    isFetching: Boolean,
    tokenValue: any,
    loginSuccess: any,
    selectedFile: any,
    showLoading: Boolean
}
export class LoginContainer extends React.Component<IProps> {
    state: IProps
    selectCategory: Boolean = false;
    constructor(props: any) {
        super(props)
        this.state = {
            selectedCategory: "",
            getEmpDetail: "",
            isFetching: false,
            selectedFile: "",
            loginSuccess: "",
            tokenValue: "",
            showLoading: false
        }
    }
    componentDidMount() {
        // this.setState({showLoading: false})
    }
    public componentWillMount() {
        this.checkLocalStoreValue();
        this.props.selectedCategory();
        console.log("getEmpDetail", this.props.getEmpDetail)
    }
    checkLocalStoreValue() {
        const Accesskey = localStorage.getItem('accessKey');
        this.selectCategory = Accesskey ? true : false;
        console.log(this.selectCategory)
        console.log("loginSuccessCall",this.props.loginSuccess)
        console.log("this.props.isFetching",this.props.isFetching)
        // window.location.href;
    }
    onFileChangeHandler(e: any){
        e.preventDefault();
        const formData = new FormData();
        for(let i = 0; i< e.target.files.length; i++) {
            formData.append('file', e.target.files[i])
        }
        console.log("formData",formData)
        const service = new APIUserManagement();
        service.upload(formData).then(res => {
            console.log(res.data);
            alert("File uploaded successfully.")
        })
    }
    public render() {
        console.log("this.props.showLoading", this.props.showLoading)
        if (this.props.isFetching) {
            // this.setState({showLoading: true})
            localStorage.setItem('accessKey', this.props.tokenValue);
            this.props.selectedCategory();
            setTimeout(() => {
                this.selectCategory = true
            }, 100);
        }
        // this.setState({showLoading : !this.props.showLoading ? false : ''});
        // this.props.loginSuccess
        // const {
        //     // doLogin,

        //     isFetching,
        //     selectedCategory,
        //     loginSuccess,
        //     getEmpDetail
        // } = this.props;
        // selectedCategory();
        return (
            <>
           {/*  {this.props.showLoading ?  <LoaderComponent /> : ''} */}
            <div>
                <div className='background-image'>
                </div>
                <CenteredView/>
            </div>
            </>

        )
    }
}

// Make data available on props
const mapStateToProps = (store: AppStore) => {
    return {
        isFetching: store.loginState.isFetching,
        getEmpDetail: store.loginState.getEmpDetail,
        tokenValue: store.loginState.tokenValue,
        isSuccess: store.loginState.isSuccess,
        showLoading: store.loginState.showLoading
    };
};

// Make functions available on props
const mapDispatchToProps = (dispatch: any) => {
    return {
        // doLogin: () => dispatch(actionDoLogin()),
        selectedCategory: () => dispatch(actionGetRoles()),
        loginSuccess: () => dispatch(loginSuccess())
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
