/**
 * Centered Login view
 *
 * @version 1.0.0
 * @author [Sakthi vel](http://192.168.1.22:8080/sakthivel_dev)
 */
import React from 'react'
// import { ReactComponent } from '*.svg';
import './CenteredView.scss'
import vthink from '../../assets/images/login/vThink(color).png'; 
import GoogleLogin from 'react-google-login';
import { actionDoLogin } from '../../core/Login/actions/LoginActions';
import AppStore from '../../core/AppStore';
import { connect } from "react-redux";
import { AppConstants } from '../../core/constants/Constant';
interface LoginProp {
    login: any;
    tokenValue: any;
    loginSuccess:Boolean;
};
interface LoginContainerState { value: any };

 class CenteredView extends React.Component<LoginProp, LoginContainerState> {
    state: LoginContainerState;
    constructor(props: any) {
        super(props);
        const getLocalStorageValue = localStorage.getItem('accessKey');
        this.state = { value: getLocalStorageValue };
    }
    
    render() {
        // const { loginSuccess } = this.props;
        const responseGoogle = (response: any) => {
            if (response && response.tokenId) {
                this.props.login(response.tokenId)
            }
        }
        // if (this.props.loginSuccess){
        //     debugger
        //     localStorage.setItem('accessKey', this.props.tokenValue);
        //     }
        return <div className='main-block'>
            <div className='vthink-image'>
                <img src={vthink} alt="vThink" />
            </div>
            <div className='Sign-in-to-continue'>
                <p>Sign in to continue</p>
            </div>
            <div className='To-sign-in-with-Goog'>
                <p>To sign in with Google, Open your browser</p>
            </div>
            <GoogleLogin
                clientId={AppConstants.clientId}
                render={renderProps => (
                    <button className='button' onClick={renderProps.onClick} disabled={renderProps.disabled}>SIGN IN</button>
                )}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
            />

        </div>
    }
}
// Make data available on props
const mapStateToProps = (store: AppStore) => {
    return {
        loginSuccess: store.loginState.isFetching,
        tokenValue: store.loginState.tokenValue,
    };
};

// Make functions available on props
const mapDispatchToProps = (dispatch: any) => {
    return {
        login: (data: any) => {
            // const reqValue = {token: data}
            dispatch(actionDoLogin(data))
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(CenteredView);