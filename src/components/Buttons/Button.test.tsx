import React from 'react';
import ButtonComponent from './ButtonComponent'
import { mount } from '../../setupTests';

describe('ButtonComponent', () => {
    it('button click should hide component', () => {
        const component = mount(<ButtonComponent/>);
        expect(component.find('#primaryBtn')).toHaveLength(1);
        // Testing Initial State
        expect(component.state("onHover")).toBe(true);
        component.simulate("mouseleave");

        // Testing state after mouseleave
        expect(component.state("onHover")).toBe(false);

        // Testing state after mouseover
        component.simulate("mouseover");
        expect(component.state("onHover")).toBe(true);
    });
})
