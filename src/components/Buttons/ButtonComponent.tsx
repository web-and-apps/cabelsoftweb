/**
 * Button component for entire Screens
 *
 * @version 1.0.0
 * @author [Sakthi Murugan](http://192.168.1.22:8080/sakthimurugan)
 */
import React from 'react';
import './ButtonComponent.scss';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import btnPlus from '../../assets/images/btn_plus_icon.svg'

interface ButtonClassProps {
    btnData: ButtonClassComponentModel
}

export class ButtonComponent extends React.Component<ButtonClassProps> {
    state = { onHover: true };
    hoverButton = (value: any) => {
        this.setState({ onHover: value });
    };
    render() {
        const { btnData } = this.props;
        return (
            // <button id="primaryBtn" className="custom-primary-btn-theme filled-btn" onMouseOver={() => this.hoverButton(true)} onMouseLeave={() => this.hoverButton(false)}>
            //     <FontAwesomeIcon icon={faPlus} />&nbsp;&nbsp; Add Employee
            // </button>
            <button id="primaryBtn" style={btnData.style} className={btnData.isBorder ? "custom-bordered-btn" : "custom-filled-btn"}
                onMouseOver={() => this.hoverButton(true)}
                onMouseLeave={() => this.hoverButton(false)}>
               {btnData.image ? <img src={btnPlus} alt="Plus ICon"/> : ''} {btnData.btnText}
            </button>
        )
    }
}

export class ButtonClassComponentModel {
    isBorder: Boolean;
    btnText: String;
    icon: String;
    style: Object;
    image:Boolean;
    constructor(isBorder: Boolean, btnText: String, icon: String, style: object, image: Boolean) {
        this.isBorder = isBorder;
        this.btnText = btnText;
        this.icon = icon;
        this.style = style;
        this.image = image;
    }

}